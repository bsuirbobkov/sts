﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Sustainability.TodoResults;

namespace STS.Domain.Configurations
{
    public class MetricTodoResultConfiguration : EntityTypeConfiguration<MetricTodoResultEntity>
    {
        public MetricTodoResultConfiguration()
        {
            ToTable("MetricTodoResults");
        }
    }
}