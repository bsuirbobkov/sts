﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Sustainability.TodoResults;

namespace STS.Domain.Configurations
{
    public class SurveyTodoResultConfiguration : EntityTypeConfiguration<SurveyTodoResultEntity>
    {
        public SurveyTodoResultConfiguration()
        {
            ToTable("SurveyTodoResults");
        }
    }
}