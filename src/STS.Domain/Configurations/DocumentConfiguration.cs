﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Document;

namespace STS.Domain.Configurations
{
    public class DocumentConfiguration : EntityTypeConfiguration<DocumentEntity>
    {
        public DocumentConfiguration()
        {
            ToTable("Documents");
        }
    }
}