﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Sustainability.Todo;

namespace STS.Domain.Configurations
{
    public class DocumentTodoConfiguration : EntityTypeConfiguration<DocumentTodoEntity>
    {
        public DocumentTodoConfiguration()
        {
            ToTable("DocumentTodos");
            HasMany(x => x.DocumentTodoItems);
        }
    }
}