﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities;

namespace STS.Domain.Configurations
{
    public class QuestionConfiguration : EntityTypeConfiguration<QuestionEntity>
    {
        public QuestionConfiguration()
        {
            ToTable("Questions");
        }
    }
}