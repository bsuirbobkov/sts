﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Sustainability;

namespace STS.Domain.Configurations
{
    public class CategoryConfiguration : EntityTypeConfiguration<SustainabilityCategoryEntity>
    {
        public CategoryConfiguration()
        {
            ToTable("Categories");
            HasMany(x => x.Todos);
        }
    }
}