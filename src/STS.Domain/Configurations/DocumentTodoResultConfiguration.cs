﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Sustainability.TodoResults;

namespace STS.Domain.Configurations
{
    public class DocumentTodoResultConfiguration : EntityTypeConfiguration<DocumentTodoResultEntity>
    {
        public DocumentTodoResultConfiguration()
        {
            ToTable("DocumentTodoResults");
        }
    }
}