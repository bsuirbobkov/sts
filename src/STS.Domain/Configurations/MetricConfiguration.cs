﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities;

namespace STS.Domain.Configurations
{
    public class MetricConfiguration : EntityTypeConfiguration<MetricEntity>
    {
        public MetricConfiguration()
        {
            ToTable("Metrics");
        }
    }
}