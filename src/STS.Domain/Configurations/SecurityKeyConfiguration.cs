﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities;

namespace STS.Domain.Configurations
{
    public class SecurityKeyConfiguration : EntityTypeConfiguration<SecurityKeyEntity>
    {
        public SecurityKeyConfiguration()
        {
            ToTable("SecurityKeys");
        }
    }
}