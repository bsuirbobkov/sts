﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Sustainability.TodoItems;

namespace STS.Domain.Configurations
{
    public class MetricTodoItemConfiguration : EntityTypeConfiguration<MetricTodoItemEntity>
    {
        public MetricTodoItemConfiguration()
        {
            ToTable("MetricTodoItems");
        }
    }
}