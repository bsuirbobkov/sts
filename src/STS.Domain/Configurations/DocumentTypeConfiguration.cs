﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Document;

namespace STS.Domain.Configurations
{
    public class DocumentTypeConfiguration : EntityTypeConfiguration<DocumentTypeEntity>
    {
        public DocumentTypeConfiguration()
        {
            ToTable("DocumentTypes");
        }
    }
}