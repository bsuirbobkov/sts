﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Sustainability.TodoItems;

namespace STS.Domain.Configurations
{
    public class SurveyTodoItemConfiguration : EntityTypeConfiguration<SurveyTodoItemEntity>
    {
        public SurveyTodoItemConfiguration()
        {
            ToTable("SurveyTodoItems");
        }
    }
}