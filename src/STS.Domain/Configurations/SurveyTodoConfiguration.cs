﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Sustainability.Todo;

namespace STS.Domain.Configurations
{
    public class SurveyTodoConfiguration : EntityTypeConfiguration<SurveyTodoEntity>
    {
        public SurveyTodoConfiguration()
        {
            ToTable("SurveyTodos");
            HasMany(x => x.SurveyTodoItems);
        }
    }
}