﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities;

namespace STS.Domain.Configurations
{
    public class CompanyConfiguration : EntityTypeConfiguration<CompanyEntity>
    {
        public CompanyConfiguration()
        {
            ToTable("Companies");
            HasMany(x => x.Users);
            HasMany(x => x.TodoResults).WithRequired(x => x.Company);
            HasOptional(x => x.SustainabilityCategory);
        }
    }
}
