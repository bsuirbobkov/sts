﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Users;

namespace STS.Domain.Configurations
{
    public class RoleConfiguration : EntityTypeConfiguration<RoleEntity>
    {
        public RoleConfiguration()
        {
            ToTable("Roles");
        }
    }
}