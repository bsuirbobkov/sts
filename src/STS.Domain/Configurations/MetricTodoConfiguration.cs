﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Sustainability.Todo;

namespace STS.Domain.Configurations
{
    public class MetricTodoConfiguration : EntityTypeConfiguration<MetricTodoEntity>
    {
        public MetricTodoConfiguration()
        {
            ToTable("MetricTodos");
            HasMany(x => x.MetricTodoItems);
        }
    }
}