﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities;

namespace STS.Domain.Configurations
{
    public class AnswerConfiguration : EntityTypeConfiguration<AnswerEntity>
    {
        public AnswerConfiguration()
        {
            ToTable("Answers");
        }
    }
}