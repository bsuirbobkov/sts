﻿using System.Data.Entity.ModelConfiguration;
using STS.Domain.Entities.Users;

namespace STS.Domain.Configurations
{
    public class UserConfiguration : EntityTypeConfiguration<UserEntity>
    {
        public UserConfiguration()
        {
            ToTable("Users");
            HasOptional(x => x.Company).WithMany(x => x.Users);
            HasMany(x => x.Roles).WithMany(x => x.Users).Map(x => x.ToTable("UsersToRoles"));
        }
    }
}