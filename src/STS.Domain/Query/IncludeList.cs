﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using STS.Domain.Entities;

namespace STS.Domain.Query
{
    public class IncludeList<TEntity> where TEntity : EntityBase
    {
        public IncludeList()
        {
            includes = new List<Expression<Func<TEntity, object>>>();
        }

        public IncludeList(params Expression<Func<TEntity, object>>[] includes)
        {
            this.includes = new List<Expression<Func<TEntity, object>>>(includes);
        }

        private readonly List<Expression<Func<TEntity, object>>> includes;

        public IEnumerable<Expression<Func<TEntity, object>>> Includes => includes.AsEnumerable();

        public IncludeList<TEntity> Add(Expression<Func<TEntity, object>> include)
        {
            if (include == null)
            {
                return this;
            }
            includes.Add(include);
            return this;
        }
    }
}