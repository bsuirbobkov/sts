namespace STS.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        YesAnswer = c.Boolean(nullable: false),
                        QuestionId = c.Int(nullable: false),
                        SurveyTodoResultEntity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.QuestionId)
                .ForeignKey("dbo.SurveyTodoResults", t => t.SurveyTodoResultEntity_Id)
                .Index(t => t.QuestionId)
                .Index(t => t.SurveyTodoResultEntity_Id);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        YesAnswerPoints = c.Int(nullable: false),
                        NoAnswerPoints = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        AddressSecond = c.String(),
                        City = c.String(),
                        Site = c.String(),
                        Country = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        Zip = c.String(),
                        TaxId = c.String(),
                        AnnualProcurementBudget = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SustainabilityCategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.SustainabilityCategoryId)
                .Index(t => t.SustainabilityCategoryId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        OrderWeight = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BaseTodoEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Enabled = c.Boolean(nullable: false),
                        Name = c.String(),
                        SustainabilityCategoryEntity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.SustainabilityCategoryEntity_Id)
                .Index(t => t.SustainabilityCategoryEntity_Id);
            
            CreateTable(
                "dbo.DocumentTodoItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConsiderationTime = c.Int(nullable: false),
                        Hyperlink = c.String(),
                        Quantity = c.Int(nullable: false),
                        DocumentTypeId = c.Int(),
                        ConsiderationTimeInMonths = c.Int(nullable: false),
                        DocumentTodoEntity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DocumentTypes", t => t.DocumentTypeId)
                .ForeignKey("dbo.DocumentTodos", t => t.DocumentTodoEntity_Id)
                .Index(t => t.DocumentTypeId)
                .Index(t => t.DocumentTodoEntity_Id);
            
            CreateTable(
                "dbo.DocumentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.MetricTodoItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ConsiderationTime = c.Int(nullable: false),
                        Increase = c.Boolean(nullable: false),
                        MetricTrend = c.Int(nullable: false),
                        RangeInPercents = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ConsiderationTimeInMonths = c.Int(nullable: false),
                        MetricTodoEntity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MetricTodos", t => t.MetricTodoEntity_Id)
                .Index(t => t.MetricTodoEntity_Id);
            
            CreateTable(
                "dbo.SurveyTodoItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TotalScores = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ConsiderationTimeInMonths = c.Int(nullable: false),
                        SurveyTodoEntity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SurveyTodos", t => t.SurveyTodoEntity_Id)
                .Index(t => t.SurveyTodoEntity_Id);
            
            CreateTable(
                "dbo.BaseTodoResultEntities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        FileName = c.String(),
                        OriginalFileName = c.String(),
                        UploadDate = c.DateTime(nullable: false),
                        FromDate = c.DateTime(nullable: false),
                        ToDate = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        DocumentTypeId = c.Int(nullable: false),
                        DocumentTodoResultEntity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DocumentTypes", t => t.DocumentTypeId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.DocumentTodoResults", t => t.DocumentTodoResultEntity_Id)
                .Index(t => t.UserId)
                .Index(t => t.DocumentTypeId)
                .Index(t => t.DocumentTodoResultEntity_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        UserName = c.String(),
                        NormalizedUserName = c.String(),
                        PasswordHash = c.String(),
                        CreatedOn = c.DateTime(nullable: false),
                        LastSignedIn = c.DateTime(),
                        UserStatus = c.Int(nullable: false),
                        LockoutEndDate = c.DateTimeOffset(precision: 7),
                        AccessFailedCount = c.Int(nullable: false),
                        LockoutEnabled = c.Boolean(nullable: false),
                        CompanyId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Metrics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MetricDate = c.DateTime(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        Revenue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Merged = c.Boolean(nullable: false),
                        Details = c.String(),
                        Scope1 = c.Decimal(precision: 18, scale: 2),
                        Scope2 = c.Decimal(precision: 18, scale: 2),
                        Scope3 = c.Decimal(precision: 18, scale: 2),
                        Electrical = c.Decimal(precision: 18, scale: 2),
                        NaturalGas = c.Decimal(precision: 18, scale: 2),
                        Gasoline = c.Decimal(precision: 18, scale: 2),
                        Diesel = c.Decimal(precision: 18, scale: 2),
                        AviationFuel = c.Decimal(precision: 18, scale: 2),
                        Propane = c.Decimal(precision: 18, scale: 2),
                        PotableGallons = c.Decimal(precision: 18, scale: 2),
                        NonPotableGallons = c.Decimal(precision: 18, scale: 2),
                        AmountSentToLandfills = c.Decimal(precision: 18, scale: 2),
                        AmountRecycled = c.Decimal(precision: 18, scale: 2),
                        AmountComposted = c.Decimal(precision: 18, scale: 2),
                        MetricTodoResultEntity_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MetricTodoResults", t => t.MetricTodoResultEntity_Id)
                .Index(t => t.MetricTodoResultEntity_Id);
            
            CreateTable(
                "dbo.SecurityKeys",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExpireDate = c.DateTime(nullable: false),
                        IsVisited = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UsersToRoles",
                c => new
                    {
                        UserEntity_Id = c.Int(nullable: false),
                        RoleEntity_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserEntity_Id, t.RoleEntity_Id })
                .ForeignKey("dbo.Users", t => t.UserEntity_Id)
                .ForeignKey("dbo.Roles", t => t.RoleEntity_Id)
                .Index(t => t.UserEntity_Id)
                .Index(t => t.RoleEntity_Id);
            
            CreateTable(
                "dbo.DocumentTodoResults",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseTodoResultEntities", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.DocumentTodos",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseTodoEntities", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.MetricTodoResults",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseTodoResultEntities", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.MetricTodos",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseTodoEntities", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.SurveyTodoResults",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseTodoResultEntities", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.SurveyTodos",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BaseTodoEntities", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SurveyTodos", "Id", "dbo.BaseTodoEntities");
            DropForeignKey("dbo.SurveyTodoResults", "Id", "dbo.BaseTodoResultEntities");
            DropForeignKey("dbo.MetricTodos", "Id", "dbo.BaseTodoEntities");
            DropForeignKey("dbo.MetricTodoResults", "Id", "dbo.BaseTodoResultEntities");
            DropForeignKey("dbo.DocumentTodos", "Id", "dbo.BaseTodoEntities");
            DropForeignKey("dbo.DocumentTodoResults", "Id", "dbo.BaseTodoResultEntities");
            DropForeignKey("dbo.BaseTodoResultEntities", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Answers", "SurveyTodoResultEntity_Id", "dbo.SurveyTodoResults");
            DropForeignKey("dbo.Metrics", "MetricTodoResultEntity_Id", "dbo.MetricTodoResults");
            DropForeignKey("dbo.Documents", "DocumentTodoResultEntity_Id", "dbo.DocumentTodoResults");
            DropForeignKey("dbo.Documents", "UserId", "dbo.Users");
            DropForeignKey("dbo.UsersToRoles", "RoleEntity_Id", "dbo.Roles");
            DropForeignKey("dbo.UsersToRoles", "UserEntity_Id", "dbo.Users");
            DropForeignKey("dbo.Users", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Documents", "DocumentTypeId", "dbo.DocumentTypes");
            DropForeignKey("dbo.Companies", "SustainabilityCategoryId", "dbo.Categories");
            DropForeignKey("dbo.BaseTodoEntities", "SustainabilityCategoryEntity_Id", "dbo.Categories");
            DropForeignKey("dbo.SurveyTodoItems", "SurveyTodoEntity_Id", "dbo.SurveyTodos");
            DropForeignKey("dbo.MetricTodoItems", "MetricTodoEntity_Id", "dbo.MetricTodos");
            DropForeignKey("dbo.DocumentTodoItems", "DocumentTodoEntity_Id", "dbo.DocumentTodos");
            DropForeignKey("dbo.DocumentTodoItems", "DocumentTypeId", "dbo.DocumentTypes");
            DropForeignKey("dbo.Answers", "QuestionId", "dbo.Questions");
            DropIndex("dbo.SurveyTodos", new[] { "Id" });
            DropIndex("dbo.SurveyTodoResults", new[] { "Id" });
            DropIndex("dbo.MetricTodos", new[] { "Id" });
            DropIndex("dbo.MetricTodoResults", new[] { "Id" });
            DropIndex("dbo.DocumentTodos", new[] { "Id" });
            DropIndex("dbo.DocumentTodoResults", new[] { "Id" });
            DropIndex("dbo.UsersToRoles", new[] { "RoleEntity_Id" });
            DropIndex("dbo.UsersToRoles", new[] { "UserEntity_Id" });
            DropIndex("dbo.Metrics", new[] { "MetricTodoResultEntity_Id" });
            DropIndex("dbo.Users", new[] { "CompanyId" });
            DropIndex("dbo.Documents", new[] { "DocumentTodoResultEntity_Id" });
            DropIndex("dbo.Documents", new[] { "DocumentTypeId" });
            DropIndex("dbo.Documents", new[] { "UserId" });
            DropIndex("dbo.BaseTodoResultEntities", new[] { "CompanyId" });
            DropIndex("dbo.SurveyTodoItems", new[] { "SurveyTodoEntity_Id" });
            DropIndex("dbo.MetricTodoItems", new[] { "MetricTodoEntity_Id" });
            DropIndex("dbo.DocumentTodoItems", new[] { "DocumentTodoEntity_Id" });
            DropIndex("dbo.DocumentTodoItems", new[] { "DocumentTypeId" });
            DropIndex("dbo.BaseTodoEntities", new[] { "SustainabilityCategoryEntity_Id" });
            DropIndex("dbo.Companies", new[] { "SustainabilityCategoryId" });
            DropIndex("dbo.Answers", new[] { "SurveyTodoResultEntity_Id" });
            DropIndex("dbo.Answers", new[] { "QuestionId" });
            DropTable("dbo.SurveyTodos");
            DropTable("dbo.SurveyTodoResults");
            DropTable("dbo.MetricTodos");
            DropTable("dbo.MetricTodoResults");
            DropTable("dbo.DocumentTodos");
            DropTable("dbo.DocumentTodoResults");
            DropTable("dbo.UsersToRoles");
            DropTable("dbo.SecurityKeys");
            DropTable("dbo.Metrics");
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
            DropTable("dbo.Documents");
            DropTable("dbo.BaseTodoResultEntities");
            DropTable("dbo.SurveyTodoItems");
            DropTable("dbo.MetricTodoItems");
            DropTable("dbo.DocumentTypes");
            DropTable("dbo.DocumentTodoItems");
            DropTable("dbo.BaseTodoEntities");
            DropTable("dbo.Categories");
            DropTable("dbo.Companies");
            DropTable("dbo.Questions");
            DropTable("dbo.Answers");
        }
    }
}
