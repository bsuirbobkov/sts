namespace STS.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SurveyTodoItems", "TotalScores", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SurveyTodoItems", "TotalScores", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
