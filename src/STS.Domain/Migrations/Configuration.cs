using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Document;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Entities.Sustainability.Todo;
using STS.Domain.Entities.Users;

namespace STS.Domain.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<StsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(StsContext context)
        {
            context.Roles.AddOrUpdate(r => r.Name,
                Enum.GetValues(typeof(UserRole)).Cast<UserRole>()
                    .Select(x => new RoleEntity { Name = x.ToString() })
                    .ToArray());
            context.SaveChanges();

            context.DocumentTypes.AddOrUpdate(x => x.Name, DocumentTypes());
            context.SustainabilityCategories.AddOrUpdate(x => x.Id, GetSustainabilityCategories());
            context.Users.AddOrUpdate(x => x.UserName, GetSuperadmin(context));
            context.SaveChanges();

            UpdateTodos(context);

            context.SaveChanges();
        }

        private static DocumentTypeEntity[] DocumentTypes()
        {
            var types = new[]
            {
                new DocumentTypeEntity {Id = 1, Name = "Sustainability Report"},
                new DocumentTypeEntity {Id = 2, Name = "Carbon Footprint Report"},
                new DocumentTypeEntity {Id = 3, Name = "Sustainability Plan"},
                new DocumentTypeEntity {Id = 4, Name = "Facility Improvement Project Report"},
                new DocumentTypeEntity {Id = 5, Name = "Employee Engagement Project Report"},
                new DocumentTypeEntity {Id = 6, Name = "Supplier Engagement Project Report"}
            };
            return types;
        }

        private static SustainabilityCategoryEntity[] GetSustainabilityCategories()
        {
            var categories = new[]
            {
                new SustainabilityCategoryEntity {Id = 1, Name = "A��ess", OrderWeight = 1},
                new SustainabilityCategoryEntity {Id = 2, Name = "Plan", OrderWeight = 2},
                new SustainabilityCategoryEntity {Id = 3, Name = "Implement", OrderWeight = 3},
                new SustainabilityCategoryEntity {Id = 4, Name = "Report", OrderWeight = 4},
                new SustainabilityCategoryEntity {Id = 5, Name = "Reduce", OrderWeight = 5}
            };
            return categories;
        }

        private static void UpdateTodos(StsContext context)
        {
            var categories = context.SustainabilityCategories.ToArray();
            foreach (var category in categories)
            {
                if (category.Todos == null || !category.Todos.Any())
                {
                    category.Todos = GetTodos();
                }
            }

            context.SustainabilityCategories.AddOrUpdate(x => x.Id, categories);
        }

        private static BaseTodoEntity[] GetTodos()
        {
            return new BaseTodoEntity[]
            {
                new DocumentTodoEntity
                {
                    Enabled = false,
                    Name = "Document"
                },
                new SurveyTodoEntity
                {
                    Enabled = false,
                    Name = "Survey"
                },
                new MetricTodoEntity
                {
                    Enabled = false,
                    Name = "Metric"
                }
            };
        }

        private static UserEntity GetSuperadmin(StsContext context)
        {
            return new UserEntity
            {
                UserName = "Supervisor",
                NormalizedUserName = "SUPERVISOR",

                Email = "Supervisor@test.test",
                CreatedOn = DateTime.Now,

                // password: 123456
                PasswordHash = "AQAAAAEAACcQAAAAEJrBdCcOw1RjVtZfx5FN66/MhX7SyFP1IW+XJ7EKAYuxqffYFow7noRrB4+57amDBg==",
                FirstName = "John",
                LastName = "Doe",
                UserStatus = UserStatus.Active,
                Roles = new List<RoleEntity>
                {
                    context.Roles.FirstOrDefault(x => x.Name == UserRole.SuperAdmin.ToString())
                },
            };
        }
    }
}
