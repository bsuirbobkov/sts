namespace STS.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Answers", "Answer", c => c.Int(nullable: false));
            DropColumn("dbo.Answers", "YesAnswer");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Answers", "YesAnswer", c => c.Boolean(nullable: false));
            DropColumn("dbo.Answers", "Answer");
        }
    }
}
