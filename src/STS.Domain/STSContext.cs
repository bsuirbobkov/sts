﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Reflection;
using STS.Domain.Configurations;
using STS.Domain.Entities;
using STS.Domain.Entities.Document;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Entities.Sustainability.Todo;
using STS.Domain.Entities.Sustainability.TodoItems;
using STS.Domain.Entities.Sustainability.TodoResults;
using STS.Domain.Entities.Users;

namespace STS.Domain
{
    [DbConfigurationType(typeof(CodeConfig))]
    public class StsContext : DbContext
    {
        public StsContext() : this("StsContext")
        {
        }

        public StsContext(string connectionString) : base(connectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(UserConfiguration)));

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<UserEntity> Users { get; set; }
        public virtual DbSet<RoleEntity> Roles { get; set; }
        public virtual DbSet<SecurityKeyEntity> SecurityKeys { get; set; }
        public virtual DbSet<CompanyEntity> Companies { get; set; }
        public virtual DbSet<SurveyTodoResultEntity> SurveyTodoResults { get; set; }
        public virtual DbSet<DocumentEntity> Documents { get; set; }
        public virtual DbSet<DocumentTypeEntity> DocumentTypes { get; set; }
        public virtual DbSet<DocumentTodoResultEntity> DocumentTodoResults { get; set; }
        public virtual DbSet<MetricTodoResultEntity> MetricTodoResults { get; set; }
        public virtual DbSet<MetricEntity> Metrics { get; set; }
        public virtual DbSet<SustainabilityCategoryEntity> SustainabilityCategories { get; set; }
        public virtual DbSet<DocumentTodoEntity> DocumentTodos { get; set; }
        public virtual DbSet<SurveyTodoEntity> SurveyTodos { get; set; }
        public virtual DbSet<MetricTodoEntity> MetricTodos { get; set; }
        public virtual DbSet<DocumentTodoItemEntity> DocumentTodoItems { get; set; }
        public virtual DbSet<SurveyTodoItemEntity> SurveyTodoItems { get; set; }
        public virtual DbSet<MetricTodoItemEntity> MetricTodoItems { get; set; }
        public virtual DbSet<QuestionEntity> Questions { get; set; }
        public virtual DbSet<AnswerEntity> Answers { get; set; }
    }
}