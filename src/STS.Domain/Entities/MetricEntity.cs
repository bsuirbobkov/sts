﻿using System;

namespace STS.Domain.Entities
{
    public class MetricEntity : EntityBase
    {
        public DateTime MetricDate { get; set; }
        public DateTime CreateDate { get; set; }
        public decimal Revenue { get; set; }
        public bool Merged { get; set; }

        public string Details { get; set; }

        public decimal? Scope1 { get; set; }
        public decimal? Scope2 { get; set; }
        public decimal? Scope3 { get; set; }

        public decimal? Electrical { get; set; }
        public decimal? NaturalGas { get; set; }
        public decimal? Gasoline { get; set; }
        public decimal? Diesel { get; set; }
        public decimal? AviationFuel { get; set; }
        public decimal? Propane { get; set; }

        public decimal? PotableGallons { get; set; }
        public decimal? NonPotableGallons { get; set; }

        public decimal? AmountSentToLandfills { get; set; }
        public decimal? AmountRecycled { get; set; }
        public decimal? AmountComposted { get; set; }
    }
}