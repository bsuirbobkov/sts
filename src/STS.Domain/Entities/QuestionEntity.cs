﻿namespace STS.Domain.Entities
{
    public class QuestionEntity : EntityBase
    {
        public string Text { get; set; }
        public int YesAnswerPoints { get; set; }
        public int NoAnswerPoints { get; set; }
    }
}