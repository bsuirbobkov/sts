﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace STS.Domain.Entities.Users
{
    public class RoleEntity : EntityBase
    {
        [StringLength(100)]
        public string Name { get; set; }

        public virtual ICollection<UserEntity> Users { get; set; }
    }
}