﻿using System;
using System.Collections.Generic;
using STS.CrossCutting.Enums;

namespace STS.Domain.Entities.Users
{
    public class UserEntity : EntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string PasswordHash { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? LastSignedIn { get; set; }
        public UserStatus UserStatus { get; set; }
        public DateTimeOffset? LockoutEndDate { get; set; }
        public int AccessFailedCount { get; set; }
        public bool LockoutEnabled { get; set; }

        public int? CompanyId { get; set; }

        public virtual CompanyEntity Company { get; set; }

        public virtual ICollection<RoleEntity> Roles { get; set; }
    }
}