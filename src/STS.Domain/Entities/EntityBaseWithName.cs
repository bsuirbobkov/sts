﻿namespace STS.Domain.Entities
{
    public class EntityBaseWithName : EntityBase
    {
        public string Name { get; set; }
    }
}
