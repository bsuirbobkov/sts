﻿namespace STS.Domain.Entities.Document
{
    public class DocumentTypeEntity : EntityBase
    {
        public string Name { get; set; }
    }
}