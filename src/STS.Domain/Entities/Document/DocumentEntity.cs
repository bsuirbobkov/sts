﻿using System;
using STS.Domain.Entities.Users;

namespace STS.Domain.Entities.Document
{
    public class DocumentEntity : EntityBase
    {
        public string Description { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public DateTime UploadDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public int UserId { get; set; }
        public int DocumentTypeId { get; set; }

        public virtual UserEntity User { get; set; }
        public virtual DocumentTypeEntity DocumentType { get; set; }
    }
}