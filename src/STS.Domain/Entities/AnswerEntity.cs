﻿using STS.CrossCutting.Enums;

namespace STS.Domain.Entities
{
    public class AnswerEntity : EntityBase
    {
        public AnswerType Answer { get; set; }

        public int QuestionId { get; set; }
        public virtual QuestionEntity Question { get; set; }
    }
}