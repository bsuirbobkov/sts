using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Entities.Users;

namespace STS.Domain.Entities
{
    public class CompanyEntity: EntityBase
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public string AddressSecond { get; set; }

        public string City { get; set; }

        public string Site { get; set; }

        public string Country { get; set; }

        public DateTime CreatedOn { get; set; }

        public string Zip { get; set; }

        public string TaxId { get; set; }

        public decimal AnnualProcurementBudget { get; set; }

        public int? SustainabilityCategoryId { get; set; }

        public virtual SustainabilityCategoryEntity SustainabilityCategory { get; set; }

        public virtual ICollection<UserEntity> Users { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<BaseTodoResultEntity> TodoResults { get; set; }
    }
}
