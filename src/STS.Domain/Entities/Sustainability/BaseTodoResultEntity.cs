﻿namespace STS.Domain.Entities.Sustainability
{
    public abstract class BaseTodoResultEntity: EntityBase
    {
        public int CompanyId { get; set; }
        public virtual CompanyEntity Company { get; set; }
    }
}