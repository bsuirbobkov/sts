﻿using System.Collections.Generic;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;

namespace STS.Domain.Entities.Sustainability.TodoResults
{
    [Todo(TodoType.SurveyTodo)]
    public class SurveyTodoResultEntity : BaseTodoResultEntity
    {
        public virtual ICollection<AnswerEntity> Answers { get; set; }
    }
}