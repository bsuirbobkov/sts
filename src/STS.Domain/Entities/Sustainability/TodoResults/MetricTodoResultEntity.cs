﻿using System.Collections.Generic;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;

namespace STS.Domain.Entities.Sustainability.TodoResults
{
    [Todo(TodoType.MetricTodo)]
    public class MetricTodoResultEntity : BaseTodoResultEntity
    {
        public virtual ICollection<MetricEntity> Metrics { get; set; }
    }
}