﻿using System.Collections.Generic;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Document;

namespace STS.Domain.Entities.Sustainability.TodoResults
{
    [Todo(TodoType.DocumentTodo)]
    public class DocumentTodoResultEntity : BaseTodoResultEntity
    {
        public virtual ICollection<DocumentEntity> Documents { get; set; }
    }
}