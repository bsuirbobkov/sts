﻿using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;

namespace STS.Domain.Entities.Sustainability.TodoItems
{
    [Todo(TodoType.MetricTodo)]
    public class MetricTodoItemEntity : BaseTodoItemEntity
    {
        public MetricConsideration ConsiderationTime { get; set; }
        public bool Increase { get; set; }
        public MetricTrend MetricTrend { get; set; }
        public decimal RangeInPercents { get; set; }
    }
}