﻿using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;

namespace STS.Domain.Entities.Sustainability.TodoItems
{
    [Todo(TodoType.SurveyTodo)]
    public class SurveyTodoItemEntity : BaseTodoItemEntity
    {
        public int TotalScores { get; set; }
    }
}