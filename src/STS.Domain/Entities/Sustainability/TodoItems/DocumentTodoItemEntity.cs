﻿using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Document;

namespace STS.Domain.Entities.Sustainability.TodoItems
{
    [Todo(TodoType.DocumentTodo)]
    public class DocumentTodoItemEntity : BaseTodoItemEntity
    {
        public DocumentConsideration ConsiderationTime { get; set; }
        public string Hyperlink { get; set; }
        public int Quantity { get; set; }
        public int? DocumentTypeId { get; set; }
        public virtual DocumentTypeEntity DocumentType { get; set; }
    }
}