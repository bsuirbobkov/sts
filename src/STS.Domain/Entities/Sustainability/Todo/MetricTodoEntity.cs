﻿using System.Collections.Generic;
using System.Linq;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Sustainability.TodoItems;

namespace STS.Domain.Entities.Sustainability.Todo
{
    [Todo(TodoType.MetricTodo)]
    public class MetricTodoEntity : BaseTodoEntity
    {
        public virtual ICollection<MetricTodoItemEntity> MetricTodoItems { get; set; }

        public override IEnumerable<BaseTodoItemEntity> GetItems()
        {
            return MetricTodoItems;
        }

        public override void SetItems(IEnumerable<BaseTodoItemEntity> items)
        {
            items.ToList().ForEach(item => MetricTodoItems.Add(item as MetricTodoItemEntity));
        }
    }
}