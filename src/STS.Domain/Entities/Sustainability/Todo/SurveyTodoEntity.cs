﻿using System.Collections.Generic;
using System.Linq;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Sustainability.TodoItems;

namespace STS.Domain.Entities.Sustainability.Todo
{
    [Todo(TodoType.SurveyTodo)]
    public class SurveyTodoEntity : BaseTodoEntity
    {
        public virtual ICollection<SurveyTodoItemEntity> SurveyTodoItems { get; set; }

        public override IEnumerable<BaseTodoItemEntity> GetItems()
        {
            return SurveyTodoItems;
        }

        public override void SetItems(IEnumerable<BaseTodoItemEntity> items)
        {
            items.ToList().ForEach(item => SurveyTodoItems.Add(item as SurveyTodoItemEntity));
        }
    }
}