﻿using System.Collections.Generic;
using System.Linq;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Sustainability.TodoItems;

namespace STS.Domain.Entities.Sustainability.Todo
{
    [Todo(TodoType.DocumentTodo)]
    public class DocumentTodoEntity : BaseTodoEntity
    {
        public virtual ICollection<DocumentTodoItemEntity> DocumentTodoItems { get; set; }

        public override IEnumerable<BaseTodoItemEntity> GetItems()
        {
            return DocumentTodoItems;
        }

        public override void SetItems(IEnumerable<BaseTodoItemEntity> items)
        {
            items.ToList().ForEach(item => DocumentTodoItems.Add(item as DocumentTodoItemEntity));
        }
    }
}