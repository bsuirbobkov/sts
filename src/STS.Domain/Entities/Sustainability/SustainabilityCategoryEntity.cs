﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace STS.Domain.Entities.Sustainability
{
    public class SustainabilityCategoryEntity : EntityBase
    {
        public string Name { get; set; }
        public int OrderWeight { get; set; }

        [ScaffoldColumn(false)]
        public virtual ICollection<BaseTodoEntity> Todos { get; set; }
    }
}