﻿using System.Collections.Generic;

namespace STS.Domain.Entities.Sustainability
{
    public abstract class BaseTodoEntity : EntityBase
    {
        public bool Enabled { get; set; }
        public string Name { get; set; }

        public abstract IEnumerable<BaseTodoItemEntity> GetItems();
        public abstract void SetItems(IEnumerable<BaseTodoItemEntity> items);
    }
}