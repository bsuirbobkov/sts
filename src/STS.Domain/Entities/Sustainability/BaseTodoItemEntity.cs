﻿namespace STS.Domain.Entities.Sustainability
{
    public abstract class BaseTodoItemEntity : EntityBase
    {
        public int ConsiderationTimeInMonths { get; set; }
    }
}