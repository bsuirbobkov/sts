﻿using System;

namespace STS.Domain.Entities
{
    public class SecurityKeyEntity : EntityBase
    {
        public DateTime ExpireDate { get; set; }
        public bool IsVisited { get; set; }
    }
}
