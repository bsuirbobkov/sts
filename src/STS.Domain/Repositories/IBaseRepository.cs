﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using STS.Domain.Entities;
using STS.Domain.Query;

namespace STS.Domain.Repositories
{
    public interface IBaseRepository<TEntity> where TEntity : EntityBase
    {
        IEnumerable<TEntity> GetAll(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, IncludeList<TEntity> includes = null);
        IEnumerable<TEntity> GetPage(Expression<Func<TEntity, bool>> filter, int startRow, int pageLength, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, IncludeList<TEntity> includes = null);
        IEnumerable<TEntity> GetPage(Expression<Func<TEntity, bool>> filter, int startRow, int pageLength, bool orderByAscending, IList<string> columnsToSort);
        TEntity Get(int id, IncludeList<TEntity> includes = null);
        IList<TEntity> GetByIds(IEnumerable<int> ids);
        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        void Remove(TEntity entity);
        int Count(Expression<Func<TEntity, bool>> filter = null);
        void SaveChanges();
    }
}