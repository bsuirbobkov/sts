﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using STS.Domain.Entities;
using STS.Domain.Repositories.Interfaces;

namespace STS.Domain.Repositories.Implementations
{
    public class CompanyRepository : GenericEntityBaseRepository<CompanyEntity>, ICompanyRepository
    {
        private readonly DbSet<CompanyEntity> dbSet;

        public CompanyRepository(StsContext context) : base(context)
        {
            dbSet = Context.Companies;
        }

        public async Task<CompanyEntity> GetFirstOrDefaultAsync()
        {
            return await dbSet.FirstOrDefaultAsync();
        }

        public async Task<CompanyEntity> GetCompanyByUserIdAsync(int userId)
        {
            return await dbSet.FirstOrDefaultAsync(x => x.Users.Any(c => c.Id == userId));
        }

        public CompanyEntity GetCompanyByUserId(int userId)
        {
            return dbSet.FirstOrDefault(x => x.Users.Any(c => c.Id == userId));
        }

        public bool CompanyNameIsUnique(string value, int id)
        {
            return dbSet.Any(c => c.Name.Equals(value) && c.Id != id);
        }

        public bool AddressIsUnique(string value, int id)
        {
            return dbSet.Any(c => c.Address.Equals(value) && c.Id != id);
        }

        public bool SiteIsUnique(string value, int id)
        {
            return dbSet.Any(c => c.Site.Equals(value) && c.Id != id);
        }

        public bool TaxIdIsUnique(string value, int id)
        {
            return dbSet.Any(c => c.TaxId == value && c.Id != id);
        }

        public CompanyEntity GetFirstCompany()
        {
            return dbSet.FirstOrDefault();
        }
    }
}
