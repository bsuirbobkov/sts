﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using STS.Domain.Entities.Users;
using STS.Domain.Repositories.Interfaces;

namespace STS.Domain.Repositories.Implementations
{
    public class UserRepository : GenericEntityBaseRepository<UserEntity>, IUserRepository
    {
        private readonly DbSet<UserEntity> dbSet;

        public UserRepository(StsContext context) : base(context)
        {
            dbSet = Context.Users;
        }

        public UserEntity GetByUserName(string userName)
        {
            return dbSet.FirstOrDefault(x => x.UserName == userName);
        }

        public UserEntity GetByNormalizeUserName(string normalizedUserName)
        {
            return dbSet.FirstOrDefault(x => x.NormalizedUserName == normalizedUserName);
        }

        public UserEntity GetByEmail(string email)
        {
            return dbSet.FirstOrDefault(x => x.Email == email);
        }

        public bool IsUserNameExists(string userName)
        {
            return dbSet.Any(x => x.UserName == userName);
        }

        public bool IsEmailExists(string email)
        {
            return dbSet.Any(x => x.Email == email);
        }

        public IList<UserEntity> GetAllSystemAdmins()
        {
            return dbSet.Where(x => x.Roles.Any(r => r.Name == nameof(CrossCutting.Enums.UserRole.SuperAdmin))).ToList();
        }
    }
}
