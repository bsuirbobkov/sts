﻿using STS.Domain.Entities.Document;
using STS.Domain.Repositories.Interfaces;

namespace STS.Domain.Repositories.Implementations
{
    public class DocumentRepository : GenericEntityBaseRepository<DocumentEntity>, IDocumentRepository
    {
        public DocumentRepository(StsContext context) : base(context)
        {
        }
    }
}