﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Repositories.Interfaces;

namespace STS.Domain.Repositories.Implementations
{
    public class SustainabilityCategoryRepository : GenericEntityBaseRepository<SustainabilityCategoryEntity>, ISustainabilityCategoryRepository
    {
        public SustainabilityCategoryRepository(StsContext context) : base(context)
        {
        }

        public void UpdateTodoItems(SustainabilityCategoryEntity entity, IList<BaseTodoEntity> todos)
        {
            foreach (var todo in entity.Todos)
            {
                RemoveTodoItems(todo.GetItems());
                var itemsToCreate = todos.First(x => x.Id == todo.Id).GetItems().ToList();
                UpdateTodoItems(todo, itemsToCreate);
            }
        }

        private void UpdateTodoItems(BaseTodoEntity todoEntity, IList<BaseTodoItemEntity> items)
        {
            foreach (var item in items)
            {
                var todoItemSet = Context.Set(item.GetType());
                todoItemSet.Add(item);
            }
            todoEntity.SetItems(items);
            SaveChanges();
        }

        private void RemoveTodoItems(IEnumerable<BaseTodoItemEntity> items)
        {
            foreach (var item in items.ToList())
            {
                var entry = Context.Entry(item);
                entry.State = EntityState.Deleted;
            }
            SaveChanges();
        }
    }
}
