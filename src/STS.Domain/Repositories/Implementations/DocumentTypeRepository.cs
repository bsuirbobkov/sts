﻿using STS.Domain.Entities.Document;
using STS.Domain.Repositories.Interfaces;

namespace STS.Domain.Repositories.Implementations
{
    public class DocumentTypeRepository : GenericEntityBaseRepository<DocumentTypeEntity>, IDocumentTypeRepository
    {
        public DocumentTypeRepository(StsContext context) : base(context)
        {
        }
    }
}