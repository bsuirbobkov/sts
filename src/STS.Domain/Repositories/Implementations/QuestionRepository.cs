﻿using STS.Domain.Entities;
using STS.Domain.Repositories.Interfaces;

namespace STS.Domain.Repositories.Implementations
{
    public class QuestionRepository : GenericEntityBaseRepository<QuestionEntity>, IQuestionRepository
    {
        public QuestionRepository(StsContext context) : base(context)
        {
        }
    }
}