﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using STS.Domain.Entities.Sustainability.TodoResults;
using STS.Domain.Repositories.Interfaces;

namespace STS.Domain.Repositories.Implementations
{
    public class SurveyTodoResultRepository : GenericEntityBaseRepository<SurveyTodoResultEntity>, ISurveyTodoResultRepository
    {
        private readonly DbSet<SurveyTodoResultEntity> dbSet;

        public SurveyTodoResultRepository(StsContext context) : base(context)
        {
            dbSet = Context.SurveyTodoResults;
        }

        public async Task<SurveyTodoResultEntity> GetByCompanyIdAsync(int companyId)
        {
            var result = await dbSet.FirstOrDefaultAsync(x => x.Company.Id == companyId);
            return result;
        }

        public SurveyTodoResultEntity GetByCompanyId(int companyId)
        {
            var result = dbSet.FirstOrDefault(x => x.Company.Id == companyId);
            return result;
        }

        public IEnumerable<SurveyTodoResultEntity> GetAllByCompanyId(int companyId)
        {
            var items = dbSet.Where(x => x.Company.Id == companyId);
            return items.ToList();
        }
    }
}