﻿using STS.Domain.Entities;
using STS.Domain.Repositories.Interfaces;

namespace STS.Domain.Repositories.Implementations
{
    public class MetricRepository : GenericEntityBaseRepository<MetricEntity>, IMetricRepository
    {
        public MetricRepository(StsContext context) : base(context)
        {
        }
    }
}