﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using STS.Domain.Entities.Users;
using STS.Domain.Repositories.Interfaces;

namespace STS.Domain.Repositories.Implementations
{
    public class RoleRepository : GenericEntityBaseRepository<RoleEntity>, IRoleRepository
    {
        private readonly DbSet<RoleEntity> dbSet;

        public RoleRepository(StsContext context) : base(context)
        {
            dbSet = Context.Roles;
        }

        public async Task<RoleEntity> GetByNameAsync(string roleName)
        {
            return await dbSet.FirstOrDefaultAsync(role => role.Name == roleName);
        }

        public RoleEntity GetByName(string roleName)
        {
            return dbSet.FirstOrDefault(role => role.Name == roleName);
        }

        public IList<UserEntity> GetUsersInRole(string roleName)
        {
            return dbSet.Where(x => x.Name == roleName).SelectMany(x => x.Users).ToList();
        }
    }
}
