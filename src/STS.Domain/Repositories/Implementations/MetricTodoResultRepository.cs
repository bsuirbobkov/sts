﻿using System.Linq;
using System.Data.Entity;
using STS.Domain.Repositories.Interfaces;
using STS.Domain.Entities.Sustainability.TodoResults;

namespace STS.Domain.Repositories.Implementations
{
    public class MetricTodoResultRepository :
        GenericEntityBaseRepository<MetricTodoResultEntity>, IMetricTodoResultRepository
    {
        private readonly DbSet<MetricTodoResultEntity> dbSet;

        public MetricTodoResultRepository(StsContext context) : base(context)
        {
            dbSet = Context.MetricTodoResults;
        }

        public MetricTodoResultEntity GetByCompanyId(int companyId)
        {
            return dbSet.FirstOrDefault(x => x.Company.Id == companyId);
        }
    }
}