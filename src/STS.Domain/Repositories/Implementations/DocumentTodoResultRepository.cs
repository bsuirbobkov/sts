﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using STS.Domain.Entities.Sustainability.TodoResults;
using STS.Domain.Repositories.Interfaces;

namespace STS.Domain.Repositories.Implementations
{
    public class DocumentTodoResultRepository : GenericEntityBaseRepository<DocumentTodoResultEntity>, IDocumentTodoResultRepository
    {
        private readonly DbSet<DocumentTodoResultEntity> dbSet;

        public DocumentTodoResultRepository(StsContext context) : base(context)
        {
            dbSet = Context.DocumentTodoResults;
        }

        public DocumentTodoResultEntity GetByCompanyId(int companyId)
        {
            return dbSet.FirstOrDefault(x => x.Company.Id == companyId);
        }

        public IEnumerable<DocumentTodoResultEntity> GetAllByCompanyId(int companyId)
        {
            var items = dbSet.Where(x => x.Company.Id == companyId);
            return items.ToList();
        }
    }
}