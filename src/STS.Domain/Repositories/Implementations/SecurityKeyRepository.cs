﻿using STS.Domain.Entities;
using STS.Domain.Repositories.Interfaces;

namespace STS.Domain.Repositories.Implementations
{
    public class SecurityKeyRepository : GenericEntityBaseRepository<SecurityKeyEntity>, ISecurityKeyRepository
    {
        public SecurityKeyRepository(StsContext context) : base(context)
        {
        }
    }
}