﻿using STS.Domain.Entities;

namespace STS.Domain.Repositories
{
    public class GenericEntityBaseRepository<TEntity> :
        EntityBaseRepositoryBase<StsContext, TEntity> where TEntity : EntityBase, new()
    {
        public GenericEntityBaseRepository(StsContext context) : base(context)
        {
        }
    }
}
