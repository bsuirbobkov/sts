﻿using STS.Domain.Entities;

namespace STS.Domain.Repositories.Interfaces
{
    public interface ISecurityKeyRepository : IBaseRepository<SecurityKeyEntity>
    {
    }
}