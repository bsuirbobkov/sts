﻿using System.Collections.Generic;
using STS.Domain.Entities.Users;

namespace STS.Domain.Repositories.Interfaces
{
    public interface IUserRepository : IBaseRepository<UserEntity>
    {
        UserEntity GetByUserName(string userName);
        UserEntity GetByNormalizeUserName(string normalizedUserName);
        UserEntity GetByEmail(string email);

        bool IsUserNameExists(string userName);
        bool IsEmailExists(string email);
        IList<UserEntity> GetAllSystemAdmins();
    }
}
