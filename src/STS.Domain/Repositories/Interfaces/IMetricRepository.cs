﻿using STS.Domain.Entities;

namespace STS.Domain.Repositories.Interfaces
{
    public interface IMetricRepository : IBaseRepository<MetricEntity>
    {
    }
}