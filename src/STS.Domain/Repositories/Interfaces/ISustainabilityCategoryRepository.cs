﻿using System.Collections.Generic;
using STS.Domain.Entities.Sustainability;

namespace STS.Domain.Repositories.Interfaces
{
    public interface ISustainabilityCategoryRepository : IBaseRepository<SustainabilityCategoryEntity>
    {
        void UpdateTodoItems(SustainabilityCategoryEntity entity, IList<BaseTodoEntity> todos);
    }
}
