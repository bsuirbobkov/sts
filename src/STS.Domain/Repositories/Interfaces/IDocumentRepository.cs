﻿using STS.Domain.Entities.Document;

namespace STS.Domain.Repositories.Interfaces
{
    public interface IDocumentRepository : IBaseRepository<DocumentEntity>
    {
    }
}