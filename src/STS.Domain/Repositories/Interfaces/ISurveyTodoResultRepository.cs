﻿using System.Collections.Generic;
using System.Threading.Tasks;
using STS.Domain.Entities.Sustainability.TodoResults;

namespace STS.Domain.Repositories.Interfaces
{
    public interface ISurveyTodoResultRepository : IBaseRepository<SurveyTodoResultEntity>
    {
        Task<SurveyTodoResultEntity> GetByCompanyIdAsync(int companyId);
        SurveyTodoResultEntity GetByCompanyId(int companyId);
        IEnumerable<SurveyTodoResultEntity> GetAllByCompanyId(int companyId);
    }
}