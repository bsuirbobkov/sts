﻿using STS.Domain.Entities.Sustainability.TodoResults;

namespace STS.Domain.Repositories.Interfaces
{
    public interface IMetricTodoResultRepository : IBaseRepository<MetricTodoResultEntity>
    {
        MetricTodoResultEntity GetByCompanyId(int companyId);
    }
}