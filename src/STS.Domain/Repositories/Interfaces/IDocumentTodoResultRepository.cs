﻿using System.Collections.Generic;
using STS.Domain.Entities.Sustainability.TodoResults;

namespace STS.Domain.Repositories.Interfaces
{
    public interface IDocumentTodoResultRepository : IBaseRepository<DocumentTodoResultEntity>
    {
        DocumentTodoResultEntity GetByCompanyId(int companyId);
        IEnumerable<DocumentTodoResultEntity> GetAllByCompanyId(int companyId);
    }
}