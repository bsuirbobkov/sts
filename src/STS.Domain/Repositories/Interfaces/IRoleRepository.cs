﻿using System.Collections.Generic;
using System.Threading.Tasks;
using STS.Domain.Entities.Users;

namespace STS.Domain.Repositories.Interfaces
{
    public interface IRoleRepository : IBaseRepository<RoleEntity>
    {
        Task<RoleEntity> GetByNameAsync(string roleName);
        RoleEntity GetByName(string roleName);
        IList<UserEntity> GetUsersInRole(string roleName);
    }
}
