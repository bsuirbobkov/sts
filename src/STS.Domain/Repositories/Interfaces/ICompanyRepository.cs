﻿using System.Threading.Tasks;
using STS.Domain.Entities;

namespace STS.Domain.Repositories.Interfaces
{
    public interface ICompanyRepository : IBaseRepository<CompanyEntity>
    {
        Task<CompanyEntity> GetFirstOrDefaultAsync();
        Task<CompanyEntity> GetCompanyByUserIdAsync(int userId);
        CompanyEntity GetCompanyByUserId(int userId);
        bool CompanyNameIsUnique(string value, int id);
        bool AddressIsUnique(string value, int id);
        bool SiteIsUnique(string value, int id);
        bool TaxIdIsUnique(string value, int id);
        CompanyEntity GetFirstCompany();
    }
}
