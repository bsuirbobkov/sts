﻿using STS.Domain.Entities.Document;

namespace STS.Domain.Repositories.Interfaces
{
    public interface IDocumentTypeRepository: IBaseRepository<DocumentTypeEntity>
    {
        
    }
}