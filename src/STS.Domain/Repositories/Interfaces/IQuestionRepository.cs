﻿using STS.Domain.Entities;

namespace STS.Domain.Repositories.Interfaces
{
    public interface IQuestionRepository : IBaseRepository<QuestionEntity>
    {
    }
}