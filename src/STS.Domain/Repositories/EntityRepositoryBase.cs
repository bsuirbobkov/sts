﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using STS.Domain.Entities;
using STS.Domain.Query;
using STS.Domain.Extensions;

namespace STS.Domain.Repositories
{
    public abstract class EntityBaseRepositoryBase<TContext, TEntity> :
        RepositoryBase<TContext>,
        IBaseRepository<TEntity> where TContext : StsContext where TEntity : EntityBase, new()
    {
        private readonly OrderBy<TEntity> defaultOrderBy = new OrderBy<TEntity>(qry => qry.OrderBy(e => e.Id));

        protected EntityBaseRepositoryBase(TContext context) : base(context)
        { }

        public virtual IEnumerable<TEntity> GetAll(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, IncludeList<TEntity> includes = null)
        {
            var result = QueryDb(null, orderBy, includes);
            return result.ToList();
        }

        public virtual IEnumerable<TEntity> GetPage(Expression<Func<TEntity, bool>> filter, int startRow, int pageLength, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, IncludeList<TEntity> includes = null)
        {
            if (orderBy == null) orderBy = defaultOrderBy.Expression;

            var result = QueryDb(filter, orderBy, includes);
            return result.Skip(startRow).Take(pageLength).ToList();
        }

        public IEnumerable<TEntity> GetPage(Expression<Func<TEntity, bool>> filter, int startRow, int pageLength, bool orderByAscending, IList<string> columnsToSort)
        {
            if (columnsToSort == null || !columnsToSort.Any())
            {
                return GetPage(filter, startRow, pageLength, defaultOrderBy.Expression);
            }

            var result = GetOrderedEntities(filter, orderByAscending, columnsToSort);
            return result.Skip(startRow).Take(pageLength).ToList();
        }

        private IOrderedQueryable<TEntity> GetOrderedEntities(Expression<Func<TEntity, bool>> filter, bool orderByAscending, IList<string> columnsToSort)
        {
            var primaryOrderProperty = columnsToSort[0];
            var result = orderByAscending
                ? Context.Set<TEntity>().Where(filter).OrderBy(primaryOrderProperty)
                : Context.Set<TEntity>().Where(filter).OrderByDescending(primaryOrderProperty);

            return columnsToSort.Count <= 1
                ? result
                : columnsToSort.Aggregate(result, (current, column) => orderByAscending ? current.ThenBy(column) : current.ThenByDescending(column));
        }

        public virtual TEntity Get(int id, IncludeList<TEntity> includes = null)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();

            if (includes != null)
            {
                query = AddIncludes(query, includes);
            }

            return query.SingleOrDefault(x => x.Id == id);
        }

        public IList<TEntity> GetByIds(IEnumerable<int> ids)
        {
            return ids == null ? null : Context.Set<TEntity>().Where(x => ids.Contains(x.Id)).ToList();
        }

        public virtual TEntity Add(TEntity entity)
        {
            var newEntity = Context.Set<TEntity>().Add(entity);
            Context.SaveChanges();
            return newEntity;
        }

        public virtual TEntity Update(TEntity entity)
        {
            if (entity == null)
            {
                return null;
            }

            var existing = Get(entity.Id);

            if (existing == null)
            {
                return null;
            }
            Context.Entry(existing).CurrentValues.SetValues(entity);
            SaveChanges();

            return existing;
        }

        public virtual void Remove(TEntity entity)
        {
            Context.Set<TEntity>().Attach(entity);
            Context.Entry(entity).State = EntityState.Deleted;
            Context.Set<TEntity>().Remove(entity);
        }

        public virtual int Count(Expression<Func<TEntity, bool>> filter = null)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            return query.Count();
        }

        protected IQueryable<TEntity> QueryDb(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy, IncludeList<TEntity> includes)
        {
            IQueryable<TEntity> query = Context.Set<TEntity>();
            query = AddFilter(filter, query);

            if (includes != null)
            {
                query = AddIncludes(query, includes);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            return query;
        }

        private static IQueryable<TEntity> AddFilter(Expression<Func<TEntity, bool>> filter, IQueryable<TEntity> query)
        {
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query;
        }

        protected IQueryable<TEntity> AddIncludes(IQueryable<TEntity> query, IncludeList<TEntity> includes)
        {
            if (includes.Includes.Any())
            {
                foreach (var includeProperty in includes.Includes)
                {
                    query = query.Include(includeProperty);
                }
            }
            return query;
        }

        public virtual void SaveChanges() => Context.SaveChanges();
    }
}