﻿namespace STS.Domain.Repositories
{
    public abstract class RepositoryBase<TContext>  where TContext : StsContext
    {
        protected RepositoryBase(TContext context)
        {
            Context = context;
        }

        protected TContext Context { get; private set; }
    }
}