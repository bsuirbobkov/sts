﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace STS.Domain.Extensions
{
    public static class LinqExtensions
    {
        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "OrderBy");
        }

        public static IOrderedQueryable<T> OrderByDescending<T>(this IQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "OrderByDescending");
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "ThenBy");
        }

        public static IOrderedQueryable<T> ThenByDescending<T>(this IOrderedQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "ThenByDescending");
        }

        private static IOrderedQueryable<T> ApplyOrder<T>(IQueryable<T> source, string property, string methodName)
        {
            var properties = property.Split('.');
            var propertyType = typeof(T);
            var expressionArgument = Expression.Parameter(propertyType, "x");
            Expression expression = expressionArgument;

            foreach (var prop in properties)
            {
                var info = propertyType.GetProperty(prop);
                expression = Expression.Property(expression, info);
                propertyType = info.PropertyType;
            }

            var delegateType = typeof(Func<,>).MakeGenericType(typeof(T), propertyType);
            var lambda = Expression.Lambda(delegateType, expression, expressionArgument);

            var result = typeof(Queryable).GetMethods().Single(
                    method => method.Name == methodName
                            && method.IsGenericMethodDefinition
                            && method.GetGenericArguments().Length == 2
                            && method.GetParameters().Length == 2)
                    .MakeGenericMethod(typeof(T), propertyType)
                    .Invoke(null, new object[] { source, lambda });

            return (IOrderedQueryable<T>)result;
        }
    }
}