﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Hangfire;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using STS.Container.Modules.Hangfire;
using STS.Domain;

namespace STS.Container
{
    public class HangfireAutofacConfiguration
    {
        public static ContainerBuilder Register(IServiceCollection services, IConfigurationRoot configuration, Action<ContainerBuilder> registerWebModules)
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<DomainServiceAutofacModule>();
            builder.RegisterModule<ApplicationServiceAutofacModule>();
            builder.RegisterModule<CrossCuttingsServiceAutofacModule>();

            registerWebModules(builder);

            builder.RegisterType<StsContext>()
                .As<StsContext>()
                .WithParameter("connectionString", configuration.GetConnectionString("StsContext"))
                .InstancePerBackgroundJob().InstancePerLifetimeScope();

            builder.Populate(services);

            return builder;
        }
    }
}