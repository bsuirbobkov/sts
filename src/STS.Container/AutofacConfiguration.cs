﻿using System;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Autofac.Integration.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using STS.Container.Modules;
using STS.Domain;
using TwentyTwenty.Storage;
using TwentyTwenty.Storage.Local;

namespace STS.Container
{
    public class AutofacConfiguration
    {
        public static ContainerBuilder Register(IServiceCollection services, IConfigurationRoot configuration, Action<ContainerBuilder> registerWebModules)
        {
            services.AddScoped(_ => new StsContext(configuration.GetConnectionString("StsContext")));
            var builder = new ContainerBuilder();

            builder.RegisterModule<CrossCuttingServiceAutofacModule>();
            builder.RegisterModule<DomainServiceAutofacModule>();
            builder.RegisterModule<ApplicationServiceAutofacModule>();

            registerWebModules(builder);

            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();

            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterFilterProvider();

            builder.Populate(services);

            if (configuration.GetSection("FileManagementSettings")["Type"] == "Local")
            {
                builder.RegisterType<LocalStorageProvider>()
                    .As<IStorageProvider>()
                    .WithParameter(new TypedParameter(typeof(string), configuration.GetSection("FileManagementSettings")["Path"]));
            }

            return builder;
        }
    }
}
