﻿using Autofac;
using STS.Domain;
using STS.Domain.Repositories;

namespace STS.Container.Modules
{
    public class DomainServiceAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IBaseRepository<>).Assembly)
                .AsClosedTypesOf(typeof(IBaseRepository<>))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterType<StsContext>()
                .As<StsContext>().InstancePerDependency();

            base.Load(builder);
        }
    }
}