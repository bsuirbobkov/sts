﻿using Autofac;
using STS.Application.MetricTrends;
using STS.Application.Services;
using STS.Application.Utils;

namespace STS.Container.Modules
{
    public class ApplicationServiceAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IBaseApplicationService).Assembly)
                .Where(t => typeof(IBaseApplicationService).IsAssignableFrom(t))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterAssemblyTypes(typeof(IBaseUtils).Assembly)
                .Where(t => typeof(IBaseUtils).IsAssignableFrom(t))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterType<MetricTrendCalculator>()
                .As<MetricTrendCalculator>()
                .InstancePerDependency();

            base.Load(builder);
        }
    }
}