﻿using Autofac;
using Hangfire;
using STS.CrossCutting.Constans;
using STS.CrossCutting.Email;
using STS.CrossCutting.Models.Configuration;
using STS.CrossCutting.Models.Cryptography;

namespace STS.Container.Modules.Hangfire
{
    public class CrossCuttingsServiceAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EmailManager>().As<EmailManager>().InstancePerBackgroundJob().InstancePerDependency();
            builder.RegisterType<PathConstant>().As<PathConstant>().InstancePerBackgroundJob().InstancePerDependency();
            builder.RegisterType<AuthConstant>().As<AuthConstant>().InstancePerBackgroundJob().InstancePerDependency();
            builder.RegisterType<LinkConstant>().As<LinkConstant>().InstancePerBackgroundJob().InstancePerDependency();
            builder.RegisterType<EmailSettings>().As<EmailSettings>().InstancePerBackgroundJob().InstancePerDependency();
            builder.RegisterType<Constants>().As<Constants>().InstancePerBackgroundJob().InstancePerDependency();
            builder.RegisterType<AesCrypter>().As<ICrypter>().InstancePerBackgroundJob().InstancePerDependency();
            base.Load(builder);
        }
    }
}