﻿using Autofac;
using Hangfire;
using STS.Domain.Repositories;

namespace STS.Container.Modules.Hangfire
{
    public class DomainServiceAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IBaseRepository<>).Assembly)
                .AsClosedTypesOf(typeof(IBaseRepository<>))
                .AsImplementedInterfaces()
                .InstancePerBackgroundJob()
                .InstancePerDependency();

            base.Load(builder);
        }
    }
}