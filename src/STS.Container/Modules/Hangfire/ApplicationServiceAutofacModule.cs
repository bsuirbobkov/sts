﻿using Autofac;
using Hangfire;
using Microsoft.Extensions.Logging;
using STS.Application.MetricTrends;
using STS.Application.Services;
using STS.Application.Utils;

namespace STS.Container.Modules.Hangfire
{
    public class ApplicationServiceAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(typeof(IBaseApplicationService).Assembly)
                .Where(t => typeof(IBaseApplicationService).IsAssignableFrom(t))
                .AsImplementedInterfaces()
                .InstancePerBackgroundJob()
                .InstancePerDependency();

            builder.RegisterAssemblyTypes(typeof(IBaseUtils).Assembly)
                .Where(t => typeof(IBaseUtils).IsAssignableFrom(t))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            builder.RegisterType<MetricTrendCalculator>()
                .As<MetricTrendCalculator>()
                .InstancePerBackgroundJob()
                .InstancePerDependency();

            builder.RegisterType<LoggerFactory>()
                .As<ILoggerFactory>()
                .InstancePerBackgroundJob()
                .InstancePerDependency();

            builder.RegisterGeneric(typeof(Logger<>))
                .As(typeof(ILogger<>))
                .InstancePerBackgroundJob()
                .InstancePerDependency();

            base.Load(builder);
        }
    }
}