﻿using Autofac;
using STS.CrossCutting.Constans;
using STS.CrossCutting.Email;
using STS.CrossCutting.Models.Cryptography;

namespace STS.Container.Modules
{
    public class CrossCuttingServiceAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EmailManager>().As<EmailManager>().InstancePerDependency();
            builder.RegisterType<PathConstant>().As<PathConstant>().InstancePerDependency();
            builder.RegisterType<AuthConstant>().As<AuthConstant>().InstancePerDependency();
            builder.RegisterType<LinkConstant>().As<LinkConstant>().InstancePerDependency();
            builder.RegisterType<Constants>().As<Constants>().InstancePerDependency();
            builder.RegisterType<AesCrypter>().As<ICrypter>().InstancePerDependency();
            base.Load(builder);
        }
    }
}