﻿using System.Collections.Generic;
using STS.Application.Models.Sustainability.AchievementItems;
using STS.Domain.Entities.Sustainability;

namespace STS.Application.Processors
{
    public interface IBaseTodoProcessor
    {
        bool Verify(BaseTodoEntity todo, BaseTodoResultEntity todoResult);
        IList<BaseAchievementItemViewModel> Validate(IList<BaseTodoItemEntity> todoItems, BaseTodoResultEntity todoResult);
    }
}