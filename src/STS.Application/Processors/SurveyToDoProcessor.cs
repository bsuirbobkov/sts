﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using STS.Application.Models.Sustainability.AchievementItems;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;
using STS.Domain.Entities;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Entities.Sustainability.Todo;
using STS.Domain.Entities.Sustainability.TodoItems;
using STS.Domain.Entities.Sustainability.TodoResults;

namespace STS.Application.Processors
{
    [Todo(TodoType.SurveyTodo)]
    public class SurveyTodoProcessor : IBaseTodoProcessor
    {
        private readonly FuncEqualityComparer<SurveyAchievementItemViewModel> distinctComparer;

        public SurveyTodoProcessor()
        {
            distinctComparer = new FuncEqualityComparer<SurveyAchievementItemViewModel>
            {
                EqualsFn = (x, y) =>
                {
                    var isEqual = x.RequiredScores == y.RequiredScores;
                    return isEqual;
                },
                HashCodeFn = x =>
                {
                    var hash = x.RequiredScores;
                    return hash;
                }
            };
        }

        public bool Verify(BaseTodoEntity todo, BaseTodoResultEntity todoResult)
        {
            var surveyTodo = todo as SurveyTodoEntity;
            var surveyResult = todoResult as SurveyTodoResultEntity;
            var rule = surveyTodo?.SurveyTodoItems?.FirstOrDefault();

            if (surveyTodo == null || !surveyTodo.Enabled || rule == null)
            {
                return true;
            }

            if (surveyResult == null)
            {
                return false;
            }

            return GetVerifyResult(rule, surveyResult);
        }

        public IList<BaseAchievementItemViewModel> Validate(IList<BaseTodoItemEntity> todoItems, BaseTodoResultEntity todoResult)
        {
            var surveyTodoItems = todoItems.Cast<SurveyTodoItemEntity>().ToList();
            var surveyTodoResult = todoResult as SurveyTodoResultEntity;

            if (!surveyTodoItems.Any())
            {
                return new List<BaseAchievementItemViewModel>();
            }

            if (surveyTodoResult == null)
            {
                return Mapper.Map<List<SurveyAchievementItemViewModel>>(todoItems)
                    .Cast<BaseAchievementItemViewModel>()
                    .ToList();
            }

            var items = new List<BaseAchievementItemViewModel>();
            foreach (var todoItem in surveyTodoItems)
            {
                var achievementItem = Mapper.Map<SurveyAchievementItemViewModel>(todoItem);
                achievementItem.Achieved = GetVerifyResult(todoItem, surveyTodoResult);
                achievementItem.RequiredScores = todoItem.TotalScores;
                items.Add(achievementItem);
            }

            var result = items
                .Cast<SurveyAchievementItemViewModel>()
                .Distinct(distinctComparer)
                .Cast<BaseAchievementItemViewModel>()
                .ToList();

            return result;
        }

        private static bool GetVerifyResult(SurveyTodoItemEntity rule, SurveyTodoResultEntity todoResult)
        {
            var totalScores = todoResult.Answers.Sum(GetScores);
            var result = rule.TotalScores <= totalScores;
            return result;
        }

        private static int GetScores(AnswerEntity answer)
        {
            switch (answer.Answer)
            {
                case AnswerType.Yes:
                    return answer.Question.YesAnswerPoints;
                case AnswerType.No:
                    return answer.Question.NoAnswerPoints;
                case AnswerType.None:
                    return 0;
                default:
                    return 0;
            }
        }
    }
}