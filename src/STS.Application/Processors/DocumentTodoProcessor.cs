﻿using System;
using System.Collections.Generic;
using System.Linq;
using STS.CrossCutting.Attributes;
using AutoMapper;
using STS.Application.Models.Sustainability.AchievementItems;
using STS.Application.Models.Sustainability.AchievementItems.SuitableItems;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Entities.Sustainability.Todo;
using STS.Domain.Entities.Sustainability.TodoItems;
using STS.Domain.Entities.Sustainability.TodoResults;

namespace STS.Application.Processors
{
    [Todo(TodoType.DocumentTodo)]
    public class DocumentTodoProcessor : IBaseTodoProcessor
    {
        private readonly FuncEqualityComparer<DocumentAchievementItemViewModel> distinctComparer;

        public DocumentTodoProcessor()
        {
            distinctComparer = new FuncEqualityComparer<DocumentAchievementItemViewModel>
            {
                EqualsFn = (x, y) =>
                {
                    var isEqual =
                        x.ConsiderationTime == y.ConsiderationTime &&
                        x.DocumentTypeName == y.DocumentTypeName &&
                        x.Quantity == y.Quantity;

                    return isEqual;
                },
                HashCodeFn = (x) =>
                {
                    var hash =
                        (int) x.ConsiderationTime ^
                        x.Quantity ^
                        x.DocumentTypeName.GetHashCode();

                    return hash;
                }
            };
        }

        public bool Verify(BaseTodoEntity todo, BaseTodoResultEntity todoResult)
        {
            var documentTodo = todo as DocumentTodoEntity;
            var documentTodoResult = todoResult as DocumentTodoResultEntity;

            if (documentTodo == null ||!documentTodo.Enabled || documentTodo.DocumentTodoItems == null || documentTodo.DocumentTodoItems.Count == 0)
            {
                return true;
            }

            if (documentTodoResult == null)
            {
                return false;
            }

            foreach (var documentTodoItem in documentTodo.DocumentTodoItems)
            {
                if (!IsCompletedTodoItem(documentTodoItem, documentTodoResult))
                    return false;
            }

            return true;
        }

        private static bool IsInTimeConsiderationRange(int considerationTimeInMonths, DateTime documentDate)
        {
            var monthCount = (DateTime.Now.Year - documentDate.Year)*12 + (DateTime.Now.Month - documentDate.Month);
            return  monthCount < considerationTimeInMonths
                || monthCount == considerationTimeInMonths && DateTime.Now.Day < documentDate.Day;
        }

        private static bool IsCompletedTodoItem(DocumentTodoItemEntity documentTodo, DocumentTodoResultEntity documentTodoResult)
        {
            var documents = documentTodoResult.Documents.Where(document => document.DocumentType == documentTodo.DocumentType);

            if (!documents.Any())
            {
                return false;
            }

            var quantity = documentTodo.ConsiderationTime == DocumentConsideration.FromUploadingDate
                ? documents.Count(document => IsInTimeConsiderationRange(documentTodo.ConsiderationTimeInMonths, document.UploadDate))
                : documents.Count(document => IsInTimeConsiderationRange(documentTodo.ConsiderationTimeInMonths, document.ToDate));

            return quantity >= documentTodo.Quantity;
        }

        public IList<BaseAchievementItemViewModel> Validate(IList<BaseTodoItemEntity> todoItems, BaseTodoResultEntity todoResult)
        {
            var documentTodoItems = todoItems.Cast<DocumentTodoItemEntity>().ToList();
            var documentTodoResult = todoResult as DocumentTodoResultEntity;

            if (!documentTodoItems.Any())
            {
                return new List<BaseAchievementItemViewModel>();
            }

            if (documentTodoResult == null)
            {
                return Mapper.Map<List<DocumentAchievementItemViewModel>>(todoItems)
                    .Cast<BaseAchievementItemViewModel>()
                    .ToList();
            }

            var items = new List<BaseAchievementItemViewModel>();
            foreach (var documentTodoItem in documentTodoItems)
            {
                var achievement = Mapper.Map<DocumentAchievementItemViewModel>(documentTodoItem);
                achievement.Documents = new List<DocumentItemViewModel>();
                var documents = documentTodoResult.Documents
                    .Where(document => document.DocumentType == documentTodoItem.DocumentType)
                    .OrderBy(x => x.UploadDate);

                if (documents.Any())
                {
                    foreach (var document in documents)
                    {
                        bool isSuit;
                        DateTime expireDate;
                        if (documentTodoItem.ConsiderationTime == DocumentConsideration.FromUploadingDate)
                        {
                            isSuit = IsInTimeConsiderationRange(documentTodoItem.ConsiderationTimeInMonths, document.UploadDate);
                            expireDate = document.UploadDate.AddMonths(documentTodoItem.ConsiderationTimeInMonths);
                        }
                        else
                        {
                            isSuit = IsInTimeConsiderationRange(documentTodoItem.ConsiderationTimeInMonths, document.ToDate);
                            expireDate = document.ToDate.AddMonths(documentTodoItem.ConsiderationTimeInMonths);
                        }

                        if (isSuit)
                        {
                            var documentToInsert = new DocumentItemViewModel
                            {
                                Id = document.Id,
                                Name = document.OriginalFileName,
                                ExpireDate = expireDate
                            };

                            achievement.Documents.Add(documentToInsert);
                        }
                    }
                }

                achievement.Achieved = achievement.Documents.Count >= documentTodoItem.Quantity;

                items.Add(achievement);
            }

            var result = items
                .Cast<DocumentAchievementItemViewModel>()
                .Distinct(distinctComparer)
                .Cast<BaseAchievementItemViewModel>()
                .ToList();

            return result;
        }
    }
}
