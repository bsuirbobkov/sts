﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using System.Reflection;
using STS.Application.MetricTrends;
using STS.Application.Models.Sustainability.AchievementItems;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Entities.Sustainability.Todo;
using STS.Domain.Entities.Sustainability.TodoItems;
using STS.Domain.Entities.Sustainability.TodoResults;

namespace STS.Application.Processors
{
    [Todo(TodoType.MetricTodo)]
    public class MetricTodoProcessor : IBaseTodoProcessor
    {
        private readonly MetricTrendCalculator metricTrendCalculator;
        private readonly FuncEqualityComparer<MetricAchievementItemViewModel> distinctComparer;

        public MetricTodoProcessor(MetricTrendCalculator metricTrendCalculator)
        {
            this.metricTrendCalculator = metricTrendCalculator;
            distinctComparer = new FuncEqualityComparer<MetricAchievementItemViewModel>
            {
                EqualsFn = (x, y) =>
                {
                    var isEqual =
                        x.ExpireDate.Date == y.ExpireDate.Date &&
                        x.Increase == y.Increase &&
                        x.ConsiderationTime == y.ConsiderationTime &&
                        x.ThresholdCurrent == y.ThresholdCurrent &&
                        x.ThresholdByRule == y.ThresholdByRule &&
                        x.MetricTrend == y.MetricTrend;

                    return isEqual;
                },
                HashCodeFn = (x) =>
                {
                    var hash =
                        Convert.ToInt32(x.Increase) ^
                        (int)x.ConsiderationTime ^
                        (int)x.ThresholdCurrent ^
                        x.ExpireDate.Year ^
                        x.ExpireDate.Month ^
                        x.ExpireDate.Day ^
                        (int)x.MetricTrend;

                    return hash;
                }
            };
        }

        public bool Verify(BaseTodoEntity todo, BaseTodoResultEntity todoResult)
        {
            var metricTodo = todo as MetricTodoEntity;
            var metricTodoResult = todoResult as MetricTodoResultEntity;

            if (metricTodo == null || metricTodo.MetricTodoItems?.Any() != true || !todo.Enabled)
            {
                return true;
            }

            if (metricTodoResult?.Metrics == null || metricTodoResult.Metrics.Count <= 1)
            {
                return false;
            }

            var metricTrendInputModel = new MetricTrendInputModel
            {
                MetricBaseYear = metricTodoResult.Metrics.OrderBy(x => x.MetricDate).FirstOrDefault(),
                MetricRecentYear = metricTodoResult.Metrics.OrderBy(x => x.MetricDate).LastOrDefault()
            };

            var result = true;

            foreach (var metricTodoItem in metricTodo.MetricTodoItems)
            {
                var calculatorMetricTrend = CalculatorMetricTrend(metricTodoItem.MetricTrend, metricTrendInputModel);

                var intermediateResult = metricTodoItem.RangeInPercents <= calculatorMetricTrend * (metricTodoItem.Increase ? 1 : -1);

                var considerationTime = GetMetricValidDate(metricTodoItem, metricTrendInputModel) > DateTime.Now;

                result &= intermediateResult && considerationTime;
            }

            return result;
        }

        public IList<BaseAchievementItemViewModel> Validate(IList<BaseTodoItemEntity> todoItems, BaseTodoResultEntity todoResult)
        {
            var metricTodoItems = todoItems.Cast<MetricTodoItemEntity>().ToList();
            var metricTodoResult = todoResult as MetricTodoResultEntity;

            if (!metricTodoItems.Any())
            {
                return new List<BaseAchievementItemViewModel>();
            }

            if (metricTodoResult?.Metrics == null || metricTodoResult.Metrics.Count <= 1)
            {
                return Mapper.Map<List<MetricAchievementItemViewModel>>(todoItems)
                    .Cast<BaseAchievementItemViewModel>()
                    .ToList();
            }

            var metricTrendInputModel = new MetricTrendInputModel
            {
                MetricBaseYear = metricTodoResult.Metrics.OrderBy(x => x.MetricDate).FirstOrDefault(),
                MetricRecentYear = metricTodoResult.Metrics.OrderBy(x => x.MetricDate).LastOrDefault()
            };

            var items = new List<BaseAchievementItemViewModel>();
            foreach (var metricTodoItem in metricTodoItems)
            {
                var achievement = Mapper.Map<MetricAchievementItemViewModel>(metricTodoItem);

                if (!metricTodoResult.Metrics.Any())
                {
                    continue;
                }

                var calculatorMetricTrend = CalculatorMetricTrend(metricTodoItem.MetricTrend, metricTrendInputModel);
                var intermediateResult = metricTodoItem.RangeInPercents <= calculatorMetricTrend * (metricTodoItem.Increase ? 1 : -1);

                var validDate = GetMetricValidDate(metricTodoItem, metricTrendInputModel);
                var considerationTime = validDate > DateTime.Now;

                achievement.ExpireDate = validDate;
                achievement.Achieved = intermediateResult && considerationTime;
                achievement.ThresholdCurrent = calculatorMetricTrend ?? 0;

                items.Add(achievement);
            }

            var result = items
                .Cast<MetricAchievementItemViewModel>()
                .Distinct(distinctComparer)
                .Cast<BaseAchievementItemViewModel>()
                .ToList();

            return result;
        }

        private DateTime GetMetricValidDate(MetricTodoItemEntity metricTodoItem, MetricTrendInputModel metricTrendInputModel)
        {
            return metricTodoItem.ConsiderationTime == MetricConsideration.FromEnterDate
                    ? metricTrendInputModel.MetricRecentYear.CreateDate.AddMonths(metricTodoItem.ConsiderationTimeInMonths)
                    : metricTrendInputModel.MetricRecentYear.MetricDate.AddMonths(metricTodoItem.ConsiderationTimeInMonths);
        }

        private decimal? CalculatorMetricTrend(MetricTrend metricTrend, MetricTrendInputModel metricTrendInputModel)
        {
            var methodInfo = GetMetricTrendCalculatorMethodInfo(metricTrend);

            return (decimal?)methodInfo.Invoke(metricTrendCalculator, new object[] { metricTrendInputModel });
        }

        private static MethodInfo GetMetricTrendCalculatorMethodInfo(MetricTrend metricTrend)
        {
            return typeof(MetricTrendCalculator).GetMethods().First(x =>
                x.GetCustomAttributes(false).OfType<MetricTrendTypeAttribute>()
                    .FirstOrDefault()?.MetricTrend == metricTrend);
        }
    }
}