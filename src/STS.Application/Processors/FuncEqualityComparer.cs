﻿using System;
using System.Collections.Generic;

namespace STS.Application.Processors
{
    public class FuncEqualityComparer<TModel> : IEqualityComparer<TModel>
    {
        public Func<TModel, TModel, bool> EqualsFn { get; set; }
        public Func<TModel, int> HashCodeFn { get; set; }

        public FuncEqualityComparer(){}

        public FuncEqualityComparer(Func<TModel, TModel, bool> equalsFn, Func<TModel, int> hashCodeFn)
        {
            EqualsFn = equalsFn;
            HashCodeFn = hashCodeFn;
        }

        public bool Equals(TModel x, TModel y)
        {
            return EqualsFn(x, y);
        }

        public int GetHashCode(TModel obj)
        {
            return HashCodeFn(obj);
        }
    }
}