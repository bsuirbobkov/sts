﻿using AutoMapper;
using STS.Application.Models;
using STS.Application.Models.Admin;
using STS.Application.Models.Category;
using STS.Application.Models.Sustainability;
using STS.Application.Models.Todo;
using STS.Application.Models.Todo.Items;
using STS.CrossCutting.Constans;
using STS.CrossCutting.Enums;
using STS.Domain.Entities;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Entities.Sustainability.Todo;
using STS.Domain.Entities.Sustainability.TodoItems;

namespace STS.Application.MapperProfiles
{
    public class AdminAutomapperProfile : Profile
    {
        public AdminAutomapperProfile()
        {
            CommonMapping();
            TodoEntityToModel();
            TodoModelToEntity();
            CompanyEntityToLeadCompany();
        }

        private void CommonMapping()
        {
            CreateMap<CompanyEntity, BaseEnumModel>()
                .ForMember(x => x.Id, x => x.MapFrom(c => c.Id))
                .ForMember(x => x.Name, x => x.MapFrom(c => c.Name))
                .ReverseMap();

            CreateMap<CompanyViewModel, CompanyEntity>()
                .ForMember(x => x.Name, x => x.MapFrom(c => c.Name))
                .ForMember(x => x.Address, x => x.MapFrom(c => c.Address))
                .ForMember(x => x.AddressSecond, x => x.MapFrom(c => c.AddressSecond))
                .ForMember(x => x.Country, x => x.MapFrom(c => c.Country))
                .ForMember(x => x.City, x => x.MapFrom(c => c.City))
                .ForMember(x => x.Zip, x => x.MapFrom(c => c.Zip))
                .ForMember(x => x.Site, x => x.MapFrom(c => c.Site))
                .ForMember(x => x.AnnualProcurementBudget, x => x.MapFrom(c => c.AnnualProcurementBudget))
                .ReverseMap();

            CreateMap<QuestionEntity, QuestionEditViewModel>()
                .ForMember(x => x.Id, x => x.MapFrom(c => c.Id))
                .ForMember(x => x.Text, x => x.MapFrom(c => c.Text))
                .ForMember(x => x.YesAnswerPoints, x => x.MapFrom(c => c.YesAnswerPoints))
                .ForMember(x => x.NoAnswerPoints, x => x.MapFrom(c => c.NoAnswerPoints))
                .ReverseMap();

            CreateMap<QuestionEntity, QuestionItemViewModel>()
                .ForMember(x => x.Id, x => x.MapFrom(c => c.Id))
                .ForMember(x => x.Text, x => x.MapFrom(c => c.Text))
                .ForMember(x => x.YesAnswerPoints, x => x.MapFrom(c => c.YesAnswerPoints))
                .ForMember(x => x.NoAnswerPoints, x => x.MapFrom(c => c.NoAnswerPoints))
                .ReverseMap();
        }

        private void TodoEntityToModel()
        {
            CreateMap<SustainabilityCategoryEntity, CategoryModel>();

            CreateMap<SustainabilityCategoryEntity, CategoryItemViewModel>()
                .ForMember(x => x.Level, x => x.Ignore());

            CreateMap<BaseTodoEntity, BaseTodoModel>()
                .Include<DocumentTodoEntity, DocumentTodoModel>()
                .Include<SurveyTodoEntity, SurveyTodoModel>()
                .Include<MetricTodoEntity, MetricTodoModel>()
                .ForMember(x => x.TodoType, x => x.Ignore());

            CreateMap<DocumentTodoEntity, DocumentTodoModel>()
                .ForMember(x => x.Documents, x => x.MapFrom(c => c.DocumentTodoItems));

            CreateMap<SurveyTodoEntity, SurveyTodoModel>()
                .ForMember(x => x.Surveys, x => x.MapFrom(c => c.SurveyTodoItems));

            CreateMap<MetricTodoEntity, MetricTodoModel>()
                .ForMember(x => x.Metrics, x => x.MapFrom(c => c.MetricTodoItems));

            CreateMap<DocumentTodoItemEntity, DocumentTodoItemModel>()
                .ForMember(x => x.ConsiderationTime, x => x.MapFrom(c => (int)c.ConsiderationTime));

            CreateMap<SurveyTodoItemEntity, SurveyTodoItemModel>()
                .ForMember(x => x.TotalScores, x => x.MapFrom(c => c.TotalScores));

            CreateMap<MetricTodoItemEntity, MetricTodoItemModel>()
                .ForMember(x => x.MetricTrendId, x => x.MapFrom(c => (int)c.MetricTrend))
                .ForMember(x => x.ConsiderationTime, x => x.MapFrom(c => (int)c.ConsiderationTime));
        }

        private void TodoModelToEntity()
        {
            CreateMap<CategoryModel, SustainabilityCategoryEntity>();

            CreateMap<BaseTodoModel, BaseTodoEntity>()
                .Include<DocumentTodoModel, DocumentTodoEntity>()
                .Include<SurveyTodoModel, SurveyTodoEntity>()
                .Include<MetricTodoModel, MetricTodoEntity>();

            CreateMap<DocumentTodoModel, DocumentTodoEntity>()
                .ForMember(x => x.DocumentTodoItems, x => x.MapFrom(c => c.Documents));

            CreateMap<SurveyTodoModel, SurveyTodoEntity>()
                .ForMember(x => x.SurveyTodoItems, x => x.MapFrom(c => c.Surveys));

            CreateMap<MetricTodoModel, MetricTodoEntity>()
                .ForMember(x => x.MetricTodoItems, x => x.MapFrom(c => c.Metrics));

            CreateMap<BaseTodoItemModel, BaseTodoItemEntity>()
                .Include<DocumentTodoItemModel, DocumentTodoItemEntity>()
                .Include<SurveyTodoItemModel, SurveyTodoItemEntity>()
                .Include<MetricTodoItemModel, MetricTodoItemEntity>();

            CreateMap<DocumentTodoItemModel, DocumentTodoItemEntity>()
                .ForMember(x => x.ConsiderationTime, x => x.MapFrom(c => (DocumentConsideration)c.ConsiderationTime));

            CreateMap<SurveyTodoItemModel, SurveyTodoItemEntity>()
                .ForMember(x => x.TotalScores, x => x.MapFrom(c => c.TotalScores));

            CreateMap<MetricTodoItemModel, MetricTodoItemEntity>()
                .ForMember(x => x.MetricTrend, x => x.MapFrom(c => (MetricTrend)c.MetricTrendId))
                .ForMember(x => x.ConsiderationTime, x => x.MapFrom(c => (MetricConsideration)c.ConsiderationTime));

        }

        private void CompanyEntityToLeadCompany()
        {
            CreateMap<CompanyEntity, CompanyItemViewModel>()
                .ForMember(x => x.SustainabilityCategory, x => x.MapFrom(c => c.SustainabilityCategory == null ? string.Empty : c.SustainabilityCategory.Name))
                .ForMember(destination => destination.Address, options => options.MapFrom(c => $"{c.Address}" +
                                                               (string.IsNullOrEmpty(c.AddressSecond) ? "" : $", {c.AddressSecond}") +
                                                               $", {c.City}" +
                                                               (string.IsNullOrEmpty(c.Zip) ? "" :
                                                               $", {(c.Zip.Length > ZipCodeConstant.ZipCodeLength ? c.Zip.Insert(ZipCodeConstant.ZipCodeLength, "-") : c.Zip)}")));
        }
    }
}