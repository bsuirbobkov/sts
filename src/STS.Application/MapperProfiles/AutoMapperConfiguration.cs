﻿using System;
using System.Linq;
using System.Reflection;
using AutoMapper;

namespace STS.Application.MapperProfiles
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                RegisterAssemblyProfiles(typeof(CompanyAutoMapperProfile), x.AddProfile);
                RegisterAssemblyProfiles(typeof(UserAutoMapperProfile), x.AddProfile);
            });
        }

        private static void RegisterAssemblyProfiles(Type targetAssemblyType, Action<Profile> registerAction)
        {
            var types = Assembly.GetAssembly(targetAssemblyType).GetTypes()
                .Where(t => t != typeof(Profile) && typeof(Profile).IsAssignableFrom(t) && !t.IsAbstract);

            foreach (var type in types)
            {
                registerAction((Profile)Activator.CreateInstance(type));
            }
        }
    }
}