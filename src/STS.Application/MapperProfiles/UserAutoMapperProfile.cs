﻿using AutoMapper;
using STS.Application.Models.User;
using STS.Domain.Entities.Users;

namespace STS.Application.MapperProfiles
{
    public class UserAutoMapperProfile : Profile
    {

        public UserAutoMapperProfile()
        {
            CreateMap<UserEntity, UserViewModel>()
                .ForMember(x => x.UserName, x => x.MapFrom(c => c.UserName))
                .ForMember(x => x.Email, x => x.MapFrom(c => c.Email))
                .ForMember(x => x.ConfirmEmail, x => x.MapFrom(c => c.Email))
                .ForMember(x => x.FirstName, x => x.MapFrom(c => c.FirstName))
                .ForMember(x => x.LastName, x => x.MapFrom(c => c.LastName))
                .ForMember(x => x.PhoneNumber, x => x.MapFrom(c => c.PhoneNumber))
                .ReverseMap();

            CreateMap<UserEntity, UserEditViewModel>()
                .ForMember(x => x.UserName, x => x.MapFrom(c => c.UserName))
                .ForMember(x => x.Email, x => x.MapFrom(c => c.Email))
                .ForMember(x => x.ConfirmEmail, x => x.MapFrom(c => c.Email))
                .ForMember(x => x.FirstName, x => x.MapFrom(c => c.FirstName))
                .ForMember(x => x.LastName, x => x.MapFrom(c => c.LastName))
                .ForMember(x => x.PhoneNumber, x => x.MapFrom(c => c.PhoneNumber))
                .ForMember(x => x.UserStatus, x => x.MapFrom(c => c.UserStatus))
                .ForMember(x => x.EditMode, x => x.Ignore())
                .ReverseMap();
        }
    }
}
