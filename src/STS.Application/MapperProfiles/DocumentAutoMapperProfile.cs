﻿using AutoMapper;
using STS.Application.Models;
using STS.Application.Models.Document;
using STS.Domain.Entities.Document;

namespace STS.Application.MapperProfiles
{
    public class DocumentAutoMapperProfile : Profile
    {

        public DocumentAutoMapperProfile()
        {
            CreateMap<DocumentTypeEntity, BaseEnumModel>();

            CreateMap<DocumentEntity, DocumentItemViewModel>()
                .ForMember(x => x.Id, x => x.MapFrom(c => c.Id))
                .ForMember(x => x.FileName, x => x.MapFrom(p => p.OriginalFileName))
                .ForMember(x => x.Description, x => x.MapFrom(c => c.Description))
                .ForMember(x => x.DocumentType, x => x.MapFrom(p => p.DocumentType.Name))
                .ForMember(x => x.UploadDate, x => x.MapFrom(p => p.UploadDate))
                .ForMember(x => x.UploadedBy, x => x.MapFrom(p => $"{p.User.FirstName} {p.User.LastName}"));
        }
    }
}