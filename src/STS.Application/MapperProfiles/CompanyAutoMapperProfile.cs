﻿using System;
using AutoMapper;
using System.Globalization;
using STS.Application.Models.Company;
using STS.Application.Models.Sustainability;
using STS.Application.Models.Sustainability.AchievementItems;
using STS.Application.Models.Sustainability.AchievementItems.SuitableItems;
using STS.Application.Models.User;
using STS.CrossCutting.Enums;
using STS.Domain.Entities;
using STS.Domain.Entities.Document;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Entities.Sustainability.Todo;
using STS.Domain.Entities.Sustainability.TodoItems;
using STS.Domain.Entities.Users;

namespace STS.Application.MapperProfiles
{
    public class CompanyAutoMapperProfile : Profile
    {
        private const string CultureName = "en-US";

        public CompanyAutoMapperProfile()
        {
            CreateMap<UserEntity, UserItemViewModel>()
                .ForMember(x => x.Name, x => x.MapFrom(c => $"{c.FirstName} {c.LastName}"))
                .ForMember(x => x.Email, x => x.MapFrom(c => c.Email))
                .ForMember(x => x.Status, x => x.MapFrom(c => c.UserStatus.ToString()));

            CreateMap<MetricEntity, MetricsViewModel>()
                .ForMember(x => x.LastDayOfTheYear, x => x.MapFrom(p => p.Id == 0
                    ? DateTime.Now.ToString("d", CultureInfo.CreateSpecificCulture(CultureName))
                    : p.MetricDate.ToString("d", CultureInfo.CreateSpecificCulture(CultureName))))
                .ForMember(x => x.Metrics, x => x.Ignore());

            CreateMap<MetricsViewModel, MetricEntity>()
                .ForMember(x => x.MetricDate, x => x.MapFrom(p => DateTime.Parse(p.LastDayOfTheYear, new CultureInfo(CultureName))));

            CreateMap<MetricEntity, MetricItemViewModel>();

            CreateMap<QuestionEntity, QuestionViewModel>()
                .ForMember(x => x.QuestionId, x => x.MapFrom(c => c.Id))
                .ForMember(x => x.Text, x => x.MapFrom(c => c.Text))
                .ForMember(x => x.Answer, x => x.Ignore());

            CreateMap<QuestionViewModel, AnswerEntity>()
                .ForMember(x => x.Id, x => x.Ignore())
                .ForMember(x => x.QuestionId, x => x.MapFrom(c => c.QuestionId))
                .ForMember(x => x.Answer, x => x.MapFrom(c => c.Answer));

            InitializeSustainabilityCategoryToCategoryModel();
        }

        private void InitializeSustainabilityCategoryToCategoryModel()
        {
            InitializeTodoToAchievement();
            InitializeTodoItemToAchievementItem();
        }

        private void InitializeTodoToAchievement()
        {
            CreateMap<BaseTodoEntity, AchievementViewModel>()
                .Include<DocumentTodoEntity, AchievementViewModel>()
                .Include<SurveyTodoEntity, AchievementViewModel>()
                .Include<MetricTodoEntity, AchievementViewModel>()
                .ForMember(x => x.AchievementItems, x => x.MapFrom(c => c.GetItems()));

            CreateMap<DocumentTodoEntity, AchievementViewModel>()
                .ForMember(x => x.AchievementType, x => x.UseValue(TodoType.DocumentTodo));

            CreateMap<SurveyTodoEntity, AchievementViewModel>()
                .ForMember(x => x.AchievementType, x => x.UseValue(TodoType.SurveyTodo));

            CreateMap<MetricTodoEntity, AchievementViewModel>()
                .ForMember(x => x.AchievementType, x => x.UseValue(TodoType.MetricTodo));
        }

        private void InitializeTodoItemToAchievementItem()
        {
            CreateMap<BaseTodoItemEntity, BaseAchievementItemViewModel>()
                .Include<DocumentTodoItemEntity, DocumentAchievementItemViewModel>()
                .Include<SurveyTodoItemEntity, SurveyAchievementItemViewModel>()
                .Include<MetricTodoItemEntity, MetricAchievementItemViewModel>();

            CreateMap<DocumentTodoItemEntity, DocumentAchievementItemViewModel>()
                .ForMember(x => x.DocumentTypeName, x => x.MapFrom(c => c.DocumentType.Name));

            CreateMap<DocumentEntity, DocumentItemViewModel>()
                .ForMember(x => x.Name, x => x.MapFrom(c => c.OriginalFileName));

            CreateMap<SurveyTodoItemEntity, SurveyAchievementItemViewModel>()
                .ForMember(x => x.RequiredScores, x => x.MapFrom(c => c.TotalScores));

            CreateMap<MetricTodoItemEntity, MetricAchievementItemViewModel>()
                .ForMember(x => x.ConsiderationTime, x => x.MapFrom(c => c.ConsiderationTime))
                .ForMember(x => x.ThresholdByRule, x => x.MapFrom(c => c.RangeInPercents));
        }
    }
}
