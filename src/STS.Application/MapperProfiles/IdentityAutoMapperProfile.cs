﻿using System;
using AutoMapper;
using STS.Application.Identity;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Users;

namespace STS.Application.MapperProfiles
{
    public class IdentityAutoMapperProfile : Profile
    {
        public IdentityAutoMapperProfile()
        {
            CreateMap<UserEntity, ApplicationUser>()
                .ForMember(x => x.Id, x => x.MapFrom(c => c.Id))
                .ForMember(x => x.UserName, x => x.MapFrom(c => c.UserName))
                .ForMember(x => x.NormalizedUserName, x => x.MapFrom(c => c.NormalizedUserName))
                .ForMember(x => x.PasswordHash, x => x.MapFrom(c => c.PasswordHash))
                .ForMember(x => x.Email, x => x.MapFrom(c => c.Email))
                .ForMember(x => x.UserStatus, x => x.MapFrom(c => c.UserStatus))
                .ForMember(x => x.LockoutEnabled, x => x.MapFrom(c => c.LockoutEnabled))
                .ForMember(x => x.AccessFailedCount, x => x.MapFrom(c => c.AccessFailedCount))
                .ForMember(x => x.LockoutEndDate, x => x.MapFrom(c => c.LockoutEndDate))
                .ForMember(x => x.CompanyId, x => x.MapFrom(c => c.CompanyId))
                .ForMember(x => x.CompanyName, x => x.MapFrom(c => c.Company == null ? string.Empty : c.Company.Name))
                .ForMember(x => x.Roles, x => x.MapFrom(c => c.Roles));

            CreateMap<ApplicationUser, UserEntity>()
                .ForMember(x => x.Id, x => x.MapFrom(c => c.Id))
                .ForMember(x => x.UserName, x => x.MapFrom(c => c.UserName))
                .ForMember(x => x.NormalizedUserName, x => x.MapFrom(c => c.NormalizedUserName))
                .ForMember(x => x.PasswordHash, x => x.MapFrom(c => c.PasswordHash))
                .ForMember(x => x.Email, x => x.MapFrom(c => c.Email))
                .ForMember(x => x.UserStatus, x => x.MapFrom(c => c.UserStatus))
                .ForMember(x => x.LockoutEnabled, x => x.MapFrom(c => c.LockoutEnabled))
                .ForMember(x => x.AccessFailedCount, x => x.MapFrom(c => c.AccessFailedCount))
                .ForMember(x => x.LockoutEndDate, x => x.MapFrom(c => c.LockoutEndDate))
                .ForMember(x => x.Roles, x => x.Ignore());

            CreateMap<RoleEntity, ApplicationRole>()
                .ForMember(x => x.Id, x => x.MapFrom(c => c.Id))
                .ForMember(x => x.Name, x => x.MapFrom(c => c.Name))
                .ForMember(x => x.UserRole, x => x.MapFrom(c => Enum.Parse(typeof(UserRole), c.Name)));

            CreateMap<ApplicationRole, RoleEntity>()
                .ForMember(x => x.Id, x => x.MapFrom(c => c.Id))
                .ForMember(x => x.Name, x => x.MapFrom(c => c.Name));
        }
    }
}