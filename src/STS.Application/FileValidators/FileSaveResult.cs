﻿namespace STS.Application.FileValidators
{
    public class FileSaveResult
    {
        public FileSaveResult() { }

        public FileSaveResult(bool error, string errorMessage, string fileName, string fileNameGuid)
        {
            Error = error;
            ErrorMessage = errorMessage;
            FileName = fileName;
            FileNameGuid = fileNameGuid;
        }

        public bool Error { get; set; }
        public string ErrorMessage { get; set; }
        public string FileName { get; set; }
        public string FileNameGuid { get; set; }
    }
}
