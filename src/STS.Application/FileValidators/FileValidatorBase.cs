﻿using System.Collections.Generic;
using System.Linq;

namespace STS.Application.FileValidators
{
    public class FileValidatorBase
    {
        public byte[] FileSignature { get; protected set; }

        public FileValidatorBase() { }

        public FileValidatorBase(params byte[] fileSignature)
        {
            FileSignature = fileSignature;
        }

        public virtual bool IsValid(byte[] data)
        {
            var result = CheckHeaderBytes(data, FileSignature);
            return result;
        }

        protected bool CheckHeaderBytes(byte[] data, byte[] signature)
        {
            return !signature.Where((@byte, index) => @byte != data[index]).Any();
        }

        protected bool ContainSequence(byte[] data, byte[] sequence)
        {
            if (data.Length < sequence.Length)
            {
                return false;
            }

            return CheckForSequence(data, sequence);
        }

        private static bool CheckForSequence(IReadOnlyList<byte> data, IReadOnlyList<byte> sequence)
        {
            var sequenceLength = sequence.Count;

            for (var i = 0; i <= data.Count - sequenceLength; i++)
            {
                if (data[i] != sequence[0])
                {
                    continue;
                }

                var position = 0;
                while (data[i + position] == sequence[position])
                {
                    position++;
                    if (position >= sequenceLength)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}