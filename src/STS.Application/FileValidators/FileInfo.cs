﻿using System;

namespace STS.Application.FileValidators
{
    public class FileInfo
    {
        public FileInfo() { }

        public FileInfo(byte[] data, string fileName, string expectedExtension)
        {
            Data = data;
            FileName = fileName;
            ExpectedExtension = expectedExtension;
        }

        public FileInfo(string base64, string fileName, string expectedExtension)
        {
            Data = ParseBase64ToBytes(base64);
            FileName = fileName;
            ExpectedExtension = expectedExtension;
        }

        public byte[] Data { get; set; }
        public string ExpectedExtension { get; set; }
        public string FileName { get; set; }

        private static byte[] ParseBase64ToBytes(string base64)
        {
            if (string.IsNullOrEmpty(base64))
            {
                return null;
            }

            var startAt = base64.LastIndexOf(',') + 1;
            return Convert.FromBase64String(base64.Substring(startAt));
        }
    }
}
