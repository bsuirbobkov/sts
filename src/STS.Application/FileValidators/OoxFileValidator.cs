﻿using System;
using System.IO;
using System.IO.Packaging;

namespace STS.Application.FileValidators
{
    public class OoxFileValidator : FileValidatorBase
    {
        private readonly byte[] defaultOoxSignature = { 0x50, 0x4B, 0x03, 0x04 };
        private readonly string contentType;

        public OoxFileValidator(string contentType)
        {
            this.contentType = contentType;
        }

        public override bool IsValid(byte[] data)
        {
            return IsOoxFile(data) && IsContentTypeMatched(data);
        }

        private bool IsOoxFile(byte[] data)
        {
            return CheckHeaderBytes(data, defaultOoxSignature);
        }

        private bool IsContentTypeMatched(byte[] data)
        {
            try
            {
                using (var stream = new MemoryStream(data))
                {
                    var package = Package.Open(stream);
                    var result = package.PartExists(new Uri(contentType, UriKind.Relative));
                    return result;
                }
            }
            catch (IOException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}