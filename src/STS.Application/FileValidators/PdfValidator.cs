﻿namespace STS.Application.FileValidators
{
    public class PdfValidator : FileValidatorBase
    {
        public PdfValidator()
        {
            FileSignature = new byte[] { 0x25, 0x50, 0x44, 0x46 };
        }

        public override bool IsValid(byte[] data)
        {
            return ContainSequence(data, FileSignature);
        }
    }
}