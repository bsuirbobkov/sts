﻿namespace STS.Application.FileValidators
{
    public class CfbFileValidator : FileValidatorBase
    {
        private readonly byte[] defaultCfbSignature = { 0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1 };

        public CfbFileValidator(params byte[] fileSignature)
        {
            FileSignature = fileSignature;
        }

        public override bool IsValid(byte[] data)
        {
            return IsCfbFile(data) && ContainSequence(data, FileSignature);
        }

        private bool IsCfbFile(byte[] data)
        {
            return CheckHeaderBytes(data, defaultCfbSignature);
        }
    }
}