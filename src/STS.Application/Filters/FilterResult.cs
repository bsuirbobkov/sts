﻿using System.Collections.Generic;

namespace STS.Application.Filters
{
    public class FilterResult<TModel> : IFilterResult<TModel>
    {
        public IFilter Filter { get; set; }
        public IEnumerable<TModel> Items { get; set; }
        public int TotalPages { get; set; }
        public int TotalItems { get; set; }
    }
}