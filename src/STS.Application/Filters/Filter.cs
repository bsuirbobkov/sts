﻿namespace STS.Application.Filters
{
    public class Filter : IFilter
    {
        public string SortColumns { get; set; }
        public bool SortAscending { get; set; }
        public int CurrentPage { get; set; }
        public int RowsPerPage { get; set; }
    }
}