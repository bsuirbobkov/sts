﻿using System.Collections.Generic;

namespace STS.Application.Filters
{
    public interface IFilterResult<TModel>
    {
        IFilter Filter { get; set; }
        IEnumerable<TModel> Items { get; set; }
        int TotalPages { get; set; }
        int TotalItems { get; set; }
    }
}