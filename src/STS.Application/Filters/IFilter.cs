﻿namespace STS.Application.Filters
{
    public interface IFilter
    {
        string SortColumns { get; set; }
        bool SortAscending { get; set; }
        int CurrentPage { get; set; }
        int RowsPerPage { get; set; }
    }
}