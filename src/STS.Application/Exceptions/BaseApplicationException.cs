﻿using System;

namespace STS.Application.Exceptions
{
    [Serializable]
    public class BaseApplicationException : ApplicationException
    {
        public BaseApplicationException()
        {
        }

        public BaseApplicationException(string message) : base(message)
        {
        }

        public BaseApplicationException(string message, Exception innerException) : base(message, innerException) { }
    }
}