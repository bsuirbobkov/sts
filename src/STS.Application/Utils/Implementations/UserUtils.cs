﻿using System;
using System.Collections.Generic;
using AutoMapper;
using STS.Application.Exceptions;
using STS.Application.Models.User;
using STS.Application.Utils.Interfaces;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Users;
using STS.Domain.Repositories.Interfaces;

namespace STS.Application.Utils.Implementations
{
    public class UserUtils : IUserUtils
    {
        private readonly IUserRepository userRepository;
        private readonly IRoleRepository roleRepository;

        public UserUtils(IUserRepository userRepository, IRoleRepository roleRepository)
        {
            this.userRepository = userRepository;
            this.roleRepository = roleRepository;
        }

        public UserEntity AddUser(UserEditViewModel viewModel, UserRole role)
        {
            var user = Mapper.Map<UserEntity>(viewModel);
            return CreateInternal(user, role);
        }

        public UserEntity AddUser(UserViewModel viewModel, UserRole role)
        {
            var user = Mapper.Map<UserEntity>(viewModel);
            return CreateInternal(user, role);
        }

        private UserEntity CreateInternal(UserEntity user, UserRole role)
        {
            if (userRepository.IsUserNameExists(user.UserName))
            {
                throw new BaseApplicationException("There is user with given login.");
            }
            if (userRepository.IsEmailExists(user.Email))
            {
                throw new BaseApplicationException("There is user with given email.");
            }

            user.LastSignedIn = null;
            user.NormalizedUserName = user.UserName.ToUpperInvariant();
            user.CreatedOn = DateTime.Now;
            user.LockoutEnabled = true;
            SetRole(user, role);
            return userRepository.Add(user);
        }

        private void SetRole(UserEntity user, UserRole role)
        {
            var targetRole = roleRepository.GetByName(Enum.GetName(typeof(UserRole), role));
            if (user.Roles == null)
            {
                user.Roles = new List<RoleEntity>(new[] { targetRole });
                return;
            }
            user.Roles.Clear();
            user.Roles.Add(targetRole);
        }
    }
}