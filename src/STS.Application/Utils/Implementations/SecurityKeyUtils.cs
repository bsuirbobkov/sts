﻿using System;
using System.Globalization;
using STS.Application.Utils.Interfaces;
using STS.CrossCutting.Constans;
using STS.CrossCutting.Models.Cryptography;
using STS.Domain.Entities;
using STS.Domain.Repositories.Interfaces;

namespace STS.Application.Utils.Implementations
{
    public class SecurityKeyUtils : ISecurityKeyUtils
    {
        private readonly ISecurityKeyRepository securityKeyRepository;
        private readonly ICrypter crypter;
        private readonly Constants constants;

        public SecurityKeyUtils(
            ISecurityKeyRepository securityKeyRepository, 
            Constants constants, 
            ICrypter crypter)
        {
            this.securityKeyRepository = securityKeyRepository;
            this.constants = constants;
            this.crypter = crypter;
        }

        public SecurityKeyEntity GenerateSecurityLink()
        {
            return securityKeyRepository.Add(new SecurityKeyEntity
            {
                ExpireDate = DateTime.Now.AddDays(constants.Auth.ExpirationTime),
                IsVisited = false
            });
        }

        public int GetLinkId(string key)
        {
            return ParseKeyInternal(key).Item1;
        }

        public int GetUserId(string key)
        {
            return ParseKeyInternal(key).Item2;
        }

        public bool IsFormatKeyValid(string key)
        {
            return ParseKeyInternal(key).Item3;
        }

        public string CreateLink(int linkId, int userId)
        {
            var cryptSource = string.Format(CultureInfo.InvariantCulture, "{0};{1}", linkId, userId);
            return crypter.Encrypt(cryptSource);
        }

        public bool ExpireKeyState(string key)
        {
            var link = GetKeyInternal(key);
            link.IsVisited = true;
            securityKeyRepository.Update(link);
            return true;
        }

        public bool IsKeyValid(int linkId)
        {
            var securityKey = securityKeyRepository.Get(linkId);
            return securityKey != null && !securityKey.IsVisited && securityKey.ExpireDate > DateTime.Now;
        }

        private SecurityKeyEntity GetKeyInternal(string key)
        {
            var linkId = GetLinkId(key);
            var link = securityKeyRepository.Get(linkId);
            return link;
        }

        private Tuple<int, int, bool> ParseKeyInternal(string key)
        {
            var source = crypter.Decrypt(key);
            var link = source.Split(';');
            if (string.IsNullOrEmpty(source))
            {
                return new Tuple<int, int, bool>(0, 0, false);
            }
            var linkId = int.Parse(link[0], CultureInfo.InvariantCulture);
            var userId = int.Parse(link[1], CultureInfo.InvariantCulture);
            return new Tuple<int, int, bool>(linkId, userId, true);
        }
    }
}