﻿using System;
using System.Collections.Generic;
using System.Linq;
using STS.Application.MetricTrends;
using STS.Application.Processors;
using STS.Application.Utils.Interfaces;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Sustainability;

namespace STS.Application.Utils.Implementations
{
    public class TodoProcessorUtils : ITodoProcessorUtils
    {
        private readonly MetricTrendCalculator metricTrendCalculator;

        public TodoProcessorUtils(MetricTrendCalculator metricTrendCalculator)
        {
            this.metricTrendCalculator = metricTrendCalculator;
        }

        public Dictionary<TodoType, IBaseTodoProcessor> GetProcessors()
        {
            var processorType = typeof(IBaseTodoProcessor);
            var types = processorType.Assembly.GetTypes()
                .Where(type => processorType.IsAssignableFrom(type) && type.IsClass);

            var todoProcessors = new Dictionary<TodoType, IBaseTodoProcessor>();
            types.ToList().ForEach(type =>
            {
                var todoType = type.GetCustomAttributes(true).OfType<TodoAttribute>().FirstOrDefault()?.TodoType;
                if (todoType != null)
                {
                    var todoTypeValue = todoType.Value;
                    todoProcessors[todoTypeValue] = GetTodoProcessor(todoTypeValue, type);
                }
            });

            return todoProcessors;
        }

        public Dictionary<TodoType, BaseTodoResultEntity> GetTodoResults(IEnumerable<BaseTodoResultEntity> todoResults)
        {
            var baseTodoResults = new Dictionary<TodoType, BaseTodoResultEntity>();

            foreach (var todoResult in todoResults)
            {
                var todoType = todoResult.GetType().GetCustomAttributes(true).OfType<TodoAttribute>().FirstOrDefault()?.TodoType;
                if (todoType != null)
                {
                    baseTodoResults.Add(todoType.Value, todoResult);
                }
            }

            return baseTodoResults;
        }

        protected IBaseTodoProcessor GetTodoProcessor(TodoType todoType, Type type)
        {
            return todoType != TodoType.MetricTodo
                ? (IBaseTodoProcessor)Activator.CreateInstance(type)
                : (IBaseTodoProcessor)Activator.CreateInstance(type, metricTrendCalculator);
        }
    }
}