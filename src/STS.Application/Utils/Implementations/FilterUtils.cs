﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper;
using STS.Application.Filters;
using STS.Application.Utils.Interfaces;
using STS.Domain.Entities;
using STS.Domain.Repositories;

namespace STS.Application.Utils.Implementations
{
    public class FilterUtils : IFilterUtils
    {
        public IFilterResult<TModel> Filter<TEntity, TModel>(IFilter filter, Expression<Func<TEntity, bool>> filterFn,
            IBaseRepository<TEntity> repository) where TEntity : EntityBase
        {
            var rowsPerPage = filter.RowsPerPage;
            var startRow = rowsPerPage * (filter.CurrentPage > 0 ? filter.CurrentPage - 1 : 0);
            var totalItems = repository.Count(filterFn);
            var models = GetModels<TEntity, TModel>(filter, repository, filterFn, startRow, rowsPerPage);

            return GenerateResult(filter, models, totalItems, rowsPerPage);
        }

        private static IEnumerable<TModel> GetModels<TEntity, TModel>(IFilter filter,
            IBaseRepository<TEntity> repository, Expression<Func<TEntity, bool>> filterFn, int startRow, int rowsPerPage)
            where TEntity : EntityBase
        {
            var columnsToSort = filter.SortColumns?.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            var entities = repository.GetPage(filterFn, startRow, rowsPerPage,
                !filter.SortAscending, columnsToSort);

            var models = Mapper.Map<IEnumerable<TModel>>(entities);

            return models;
        }

        private static FilterResult<TModel> GenerateResult<TModel>(IFilter filter,
            IEnumerable<TModel> models, int totalItems, int rowsPerPage)
        {
            var result = new FilterResult<TModel>
            {
                Filter = filter,
                Items = models,
                TotalPages = (int)Math.Ceiling(totalItems / (double)rowsPerPage),
                TotalItems = totalItems
            };

            return result;
        }
    }
}