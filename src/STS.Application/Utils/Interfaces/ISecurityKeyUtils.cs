﻿using STS.Domain.Entities;

namespace STS.Application.Utils.Interfaces
{
    public interface ISecurityKeyUtils : IBaseUtils
    {
        SecurityKeyEntity GenerateSecurityLink();
        int GetLinkId(string key);
        int GetUserId(string key);
        bool IsFormatKeyValid(string key);
        string CreateLink(int userId, int linkId);
        bool ExpireKeyState(string key);
        bool IsKeyValid(int linkId);
    }
}