﻿using System;
using System.Linq.Expressions;
using STS.Application.Filters;
using STS.Domain.Entities;
using STS.Domain.Repositories;

namespace STS.Application.Utils.Interfaces
{
    public interface IFilterUtils : IBaseUtils
    {
        IFilterResult<TModel> Filter<TEntity, TModel>(
            IFilter filter,
            Expression<Func<TEntity, bool>> filterFn,
            IBaseRepository<TEntity> repository) where TEntity : EntityBase;
    }
}