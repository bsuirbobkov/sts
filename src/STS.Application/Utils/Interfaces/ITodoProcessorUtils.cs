﻿using System.Collections.Generic;
using STS.Application.Processors;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Sustainability;

namespace STS.Application.Utils.Interfaces
{
    public interface ITodoProcessorUtils : IBaseUtils
    {
        Dictionary<TodoType, IBaseTodoProcessor> GetProcessors();
        Dictionary<TodoType, BaseTodoResultEntity> GetTodoResults(IEnumerable<BaseTodoResultEntity> todoResults);
    }
}