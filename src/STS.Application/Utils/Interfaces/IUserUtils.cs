﻿using STS.Application.Models.User;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Users;

namespace STS.Application.Utils.Interfaces
{
    public interface IUserUtils : IBaseUtils
    {
        UserEntity AddUser(UserEditViewModel viewModel, UserRole role);
        UserEntity AddUser(UserViewModel viewModel, UserRole role);
    }
}