﻿using STS.Application.Models.Report;
using STS.Application.Models.Report.Data;

namespace STS.Application.Services.Interfaces
{
    public interface IReportDataService : IBaseApplicationService
    {
        BaseReportData GetReportData(ReportsRequestModel request);
        CompanyDistributionBySustainabilityLevelTotalReportData SupplierDistributionBySustainabilityLevelTotalReport(ReportsRequestModel request);
    }
}