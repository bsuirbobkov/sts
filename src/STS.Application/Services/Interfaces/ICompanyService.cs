using System.Collections.Generic;
using STS.Application.FileValidators;
using STS.Application.Filters;
using STS.Application.Models.Company;
using STS.Application.Models.Document;
using STS.Application.Models.Sustainability;
using STS.Application.Models.User;

namespace STS.Application.Services.Interfaces
{
    public interface ICompanyService : IBaseApplicationService
    {
        FileSaveResult SaveDocumentTemporary(DocumentModel documentModel);
        void SaveDocument(DocumentUploadModel documentUploadModel, int userId);
        DocumentUploadViewModel GetDocumentUploadViewModel(int companyId);
        IFilterResult<UserItemViewModel> FilterUsers(IFilter filter, int companyId);
        IFilterResult<DocumentItemViewModel> FilterDocuments(IFilter filter, int companyId);
        int AddMetric(MetricsViewModel metricsViewModel, int companyId);
        void UpdateMetric(MetricsViewModel metricsViewModel, int companyId);
        MetricsViewModel GetMetricsViewModel(int metricId, int companyId);
        IEnumerable<MetricItemViewModel> GetMetrics(int companyId);
        IEnumerable<string> CheckYear(int companyId, MetricsViewModel model);
        SustainabilityPageViewModel GetSustainabilityPageViewModel(int companyId, int categoryId);
        int GetActualCategoryId(int companyId);
        bool IsCategoryExist(int categoryId);
        bool HasAccess(int companyId, int categoryId);
        void RestoreMetricsViewModel(MetricsViewModel model, int companyId);
        UserEditViewModel GetUserEditViewModel(int? userId);
        void UpdateUser(UserEditViewModel viewModel, int companyId);
        void AddUser(UserEditViewModel viewModel, int companyId, int userId);
        SurveyViewModel GetSurveyViewModel(int companyId);
        void SubmitQuestions(IEnumerable<QuestionViewModel> questions, int companyId);
    }
}