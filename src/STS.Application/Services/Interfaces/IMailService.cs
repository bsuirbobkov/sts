using STS.Domain.Entities;

namespace STS.Application.Services.Interfaces
{
    public interface IMailService : IBaseApplicationService
    {
        void SendSetPasswordEmail(int userId, int linkId, int inviterId);
        void SendChangeLevelEmail(CompanyEntity company, bool isIncrease, string newCompanySustainabilityCategoryName, string companySustainabilityCategoryName);
    }
}