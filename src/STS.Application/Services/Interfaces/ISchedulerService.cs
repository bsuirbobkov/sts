﻿namespace STS.Application.Services.Interfaces
{
    public interface ISchedulerService : IBaseApplicationService
    {
        void DeterminateSustainabilityLevel();
        void DeterminateSustainabilityLevel(int companyId);
        void ProcessingSustainabilityCategory(int companyId);
    }
}