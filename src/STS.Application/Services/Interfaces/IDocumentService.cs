﻿using STS.Application.Models.Document;

namespace STS.Application.Services.Interfaces
{
    public interface IDocumentService : IBaseApplicationService
    {
        FileDownloadModel GetDocument(int id);
        bool HasAccess(int userId, int documentId);
    }
}