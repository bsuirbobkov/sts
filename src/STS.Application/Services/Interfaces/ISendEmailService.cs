﻿using STS.Application.Models.EmailTemplates;

namespace STS.Application.Services.Interfaces
{
    public interface ISendEmailService
    {
        string RenderSignUpTemplate(SignUpModel model);
        string RenderPasswordRestoreTemplate(LinkModel model);
        string RenderChangeLevelCompanyTemplate(ChangeLevelModel model);
        string RenderChangeAdminLevelTemplate(ChangeLevelModel model);
    }
}