﻿using System.Collections.Generic;
using STS.Application.Filters;
using STS.Application.Models;
using STS.Application.Models.Admin;
using STS.Application.Models.Category;
using STS.Application.Models.Company;
using STS.Application.Models.ManageSustainability;
using STS.Application.Models.Report;
using STS.Application.Models.User;

namespace STS.Application.Services.Interfaces
{
    public interface IAdminService : IBaseApplicationService
    {
        IFilterResult<UserItemViewModel> FilterUsers(Filter filter);
        UserEditViewModel GetUserEditViewModel(int userId);
        void UpdateUser(UserEditViewModel userModel);
        void AddUser(UserEditViewModel viewModel, int userId);

        IFilterResult<CompanyItemViewModel> FilterCompanies(IFilter filter);
        void RestoreCompanyEditViewModel(CompanyEditViewModel viewModel);
        CompanyEditViewModel GetCompanyEditViewModel(int? companyId = null);
        void UpdateCompany(CompanyEditViewModel viewModel, int userId);
        void AddCompany(CompanyEditViewModel viewModel, int userId);

        CategoriesViewModel GetCategoriesViewModel();
        void UpdateCategories(IEnumerable<CategoryModel> categories);

        ReportsViewModel GetReportViewModel();
        DatesErrorModel ValidateReportDates(string startDateString, string endDateString);
        BaseReportData GetReportData(int reportType, string startDate, string endDate);


        IEnumerable<BaseEnumModel> GetCompanies();
        int GetTargetOrFirstCompanyId(int companyId);
        DocumentsViewModel GetDocumentsViewModel(int companyId);
        MetricsViewModel GetMetricsViewModel(int companyId);
        MetricsViewModel GetMetricsViewModel(int companyId, int metricId);
        int GetActualCategoryIdForCompany(int companyId);
        bool CompanyHasAccess(int companyId, int categoryId);
        CompanySustainabilityViewModel GetCategories(int supplierId, int categoryId);

        IFilterResult<QuestionItemViewModel> FilterQuestions(IFilter filter);
        QuestionEditViewModel GetQuestionEditViewModel(int questionId);
        void UpdateQuestion(QuestionEditViewModel viewModel);
        void AddQuestions(QuestionEditViewModel viewModel);
    }
}