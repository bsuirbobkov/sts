using STS.Application.Identity;
using STS.Application.Models.Account;

namespace STS.Application.Services.Interfaces
{
    public interface IAccountService : IBaseApplicationService
    {
        ApplicationUser GetByUserName(string userName);
        ApplicationUser GetByEmail(string email);
        ApplicationUser GetByUserKeyAsync(string key);
        SignUpViewModel GetSignUpViewModel(string key);
        bool IsKeyValid(string key);
        bool ExpireKeyState(string key);
        void RestorePassword(ApplicationUser user);
        ResetPasswordViewModel GetResetPasswordViewModel(string code);
        void ResendEmail(int userId, int curentUserId);
        bool IsPasswordExistsForKey(string key);
    }
}