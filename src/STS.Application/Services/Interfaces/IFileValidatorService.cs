﻿using STS.Application.FileValidators;

namespace STS.Application.Services.Interfaces
{
    public interface IFileValidatorService : IBaseApplicationService
    {
        bool IsValid(FileInfo fileInfo);
    }
}