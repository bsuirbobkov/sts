﻿using STS.Application.FileValidators;
using STS.Application.Models.Document;

namespace STS.Application.Services.Interfaces
{
    public interface IFileService : IBaseApplicationService
    {
        FileSaveResult Save(FileInfo file, string containerName);
        bool Move(string fileName, string fromPath, string toPath);
        FileDownloadModel GetDownloadData(string fileName, string fromPath);
    }
}
