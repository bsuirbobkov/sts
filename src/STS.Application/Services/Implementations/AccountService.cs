﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using STS.Application.Identity;
using STS.Application.Models.Account;
using STS.Application.Models.EmailTemplates;
using STS.Application.Services.Interfaces;
using STS.Application.Utils.Interfaces;
using STS.CrossCutting.Constans;
using STS.CrossCutting.Email;
using STS.Domain.Repositories.Interfaces;

namespace STS.Application.Services.Implementations
{
    public class AccountService : IAccountService
    {
        private readonly IUserRepository userRepository;
        private readonly ISendEmailService sendEmailService;
        private readonly IMailService mailService;
        private readonly ISecurityKeyUtils securityKeyUtils;
        private readonly EmailManager emailManager;
        private readonly Constants constants;

        public AccountService(EmailManager emailManager,
            IUserRepository userRepository,
            ISendEmailService sendEmailService,
            IMailService mailService,
            Constants constants, ISecurityKeyUtils securityKeyUtils)
        {
            this.emailManager = emailManager;
            this.userRepository = userRepository;
            this.sendEmailService = sendEmailService;
            this.mailService = mailService;
            this.constants = constants;
            this.securityKeyUtils = securityKeyUtils;
        }

        public ApplicationUser GetByUserName(string userName)
        {
            var entity = userRepository.GetByUserName(userName);
            if (entity == null)
            {
                return null;
            }
            var applicationUser = Mapper.Map<ApplicationUser>(entity);
            return applicationUser;
        }

        public ApplicationUser GetByEmail(string email)
        {
            var entity = userRepository.GetByEmail(email);
            var applicationUser = Mapper.Map<ApplicationUser>(entity);
            return applicationUser;
        }

        public ApplicationUser GetByUserKeyAsync(string key)
        {
            var userId = securityKeyUtils.GetUserId(key);
            var user = userRepository.Get(userId);
            if (user == null)
            {
                return null;
            }
            var applicationUser = Mapper.Map<ApplicationUser>(user);
            return applicationUser;
        }

        public SignUpViewModel GetSignUpViewModel(string key)
        {
            var linkId = securityKeyUtils.GetLinkId(key);
            var userId = securityKeyUtils.GetUserId(key);
            var user = userRepository.Get(userId);

            if (user == null || !securityKeyUtils.IsKeyValid(linkId))
            {
                return null;
            }

            var viewModel = new SignUpViewModel
            {
                SecurityKey = securityKeyUtils.CreateLink(linkId, userId),
                Email = user.Email,
                UserName = user.UserName
            };

            return viewModel;
        }

        public bool IsKeyValid(string key)
        {
            var linkId = securityKeyUtils.GetLinkId(key);
            return securityKeyUtils.IsKeyValid(linkId);
        }

        public bool ExpireKeyState(string key)
        {
            return securityKeyUtils.ExpireKeyState(key);
        }

        public void RestorePassword(ApplicationUser user)
        {
            var link = securityKeyUtils.GenerateSecurityLink();
            SendRestorePasswordEmail(user.Id, link.Id);
        }

        public ResetPasswordViewModel GetResetPasswordViewModel(string code)
        {
            var linkId = securityKeyUtils.GetLinkId(code);
            var userId = securityKeyUtils.GetUserId(code);
            var user = userRepository.Get(userId);

            if (user == null || !securityKeyUtils.IsKeyValid(linkId))
            {
                return null;
            }

            return new ResetPasswordViewModel
            {
                Code = securityKeyUtils.CreateLink(linkId, userId)
            };
        }

        public void ResendEmail(int userId, int curentUserId)
        {
            var user = userRepository.Get(curentUserId);
            if (user.Company.Users.Any(x => x.Id == userId))
            {
                var link = securityKeyUtils.GenerateSecurityLink();
                mailService.SendSetPasswordEmail(userId, link.Id, curentUserId);
            }
        }

        public bool IsPasswordExistsForKey(string key)
        {
            if (securityKeyUtils.IsFormatKeyValid(key))
            {
                return false;
            }
            var userId = securityKeyUtils.GetUserId(key);
            var user = userRepository.Get(userId);
            return !string.IsNullOrEmpty(user.PasswordHash);
        }

        private void SendRestorePasswordEmail(int userId, int linkId)
        {
            var user = userRepository.Get(userId);
            var cryptedString = securityKeyUtils.CreateLink(linkId, userId);

            var resetPasswordUrl = constants.Links.RestorePassword + HttpUtility.UrlEncode(cryptedString);
            const string subject = "Restore password";

            emailManager.SendMessage(user.Email, user.Email, subject, sendEmailService.RenderPasswordRestoreTemplate(new LinkModel { Link = resetPasswordUrl }));
        }
    }
}