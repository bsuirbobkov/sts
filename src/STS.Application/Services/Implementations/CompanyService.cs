using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using STS.Application.FileValidators;
using STS.Application.Filters;
using STS.Application.Models;
using STS.Application.Models.Company;
using STS.Application.Models.Document;
using STS.Application.Models.Sustainability;
using STS.Application.Models.User;
using STS.Application.Services.Interfaces;
using STS.Application.Utils.Interfaces;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Constans;
using STS.CrossCutting.Enums;
using STS.Domain.Entities;
using STS.Domain.Entities.Document;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Entities.Sustainability.TodoResults;
using STS.Domain.Entities.Users;
using STS.Domain.Repositories.Interfaces;
using SustainabilityLevel = STS.Application.Models.Sustainability.SustainabilityLevel;

namespace STS.Application.Services.Implementations
{
    public class CompanyService : ICompanyService
    {
        private readonly ICompanyRepository companyRepository;
        private readonly IDocumentRepository documentRepository;
        private readonly IDocumentTypeRepository documentTypeRepository;

        private readonly IUserRepository userRepository;
        private readonly IMetricRepository metricRepository;
        private readonly IQuestionRepository questionRepository;

        private readonly ISustainabilityCategoryRepository sustainabilityCategoryRepository;
        private readonly IDocumentTodoResultRepository documentTodoResultRepository;
        private readonly ISurveyTodoResultRepository surveyTodoResultRepository;
        private readonly IMetricTodoResultRepository metricTodoResultRepository;

        private readonly ISchedulerService schedulerService;
        private readonly PathConstant pathConstant;

        private readonly IFileService fileApplicationService;
        private readonly IMailService mailService;

        private readonly IFilterUtils filterUtilses;
        private readonly ITodoProcessorUtils todoProcessorUtilse;
        private readonly IUserUtils userUtils;
        private readonly ISecurityKeyUtils securityKeyUtils;

        public CompanyService(
            ICompanyRepository companyRepository,
            IDocumentRepository documentRepository,
            IDocumentTypeRepository documentTypeRepository,

            IUserRepository userRepository,
            IMetricRepository metricRepository,
            IQuestionRepository questionRepository,

            ISustainabilityCategoryRepository sustainabilityCategoryRepository,
            IDocumentTodoResultRepository documentTodoResultRepository,
            ISurveyTodoResultRepository surveyTodoResultRepository,
            IMetricTodoResultRepository metricTodoResultRepository,

            ISchedulerService schedulerService,
            PathConstant pathConstant,
            IFileService fileApplicationService,
            IMailService mailService,
            IFilterUtils filterUtilses,
            ITodoProcessorUtils todoProcessorUtilse,
            IUserUtils userUtils,
            ISecurityKeyUtils securityKeyUtils)
        {
            this.companyRepository = companyRepository;
            this.documentRepository = documentRepository;
            this.documentTypeRepository = documentTypeRepository;
            this.userRepository = userRepository;
            this.metricRepository = metricRepository;
            this.questionRepository = questionRepository;
            this.sustainabilityCategoryRepository = sustainabilityCategoryRepository;
            this.documentTodoResultRepository = documentTodoResultRepository;
            this.surveyTodoResultRepository = surveyTodoResultRepository;
            this.metricTodoResultRepository = metricTodoResultRepository;
            this.schedulerService = schedulerService;
            this.pathConstant = pathConstant;
            this.fileApplicationService = fileApplicationService;
            this.mailService = mailService;
            this.filterUtilses = filterUtilses;
            this.todoProcessorUtilse = todoProcessorUtilse;
            this.userUtils = userUtils;
            this.securityKeyUtils = securityKeyUtils;
        }

        public FileSaveResult SaveDocumentTemporary(DocumentModel documentModel)
        {
            if (!string.IsNullOrEmpty(documentModel.FileNameGuid))
                return new FileSaveResult(false, null, documentModel.FileName, documentModel.FileNameGuid);

            var fileInfo = new FileInfo(documentModel.FileBase64Data, documentModel.FileName,
                documentModel.ExpectedExtension);

            var saveResult = fileApplicationService.Save(fileInfo, pathConstant.TemporaryStorePath);

            documentModel.FileNameGuid = saveResult.Error
                ? documentModel.FileNameGuid
                : saveResult.FileNameGuid;

            return saveResult;
        }

        public void SaveDocument(DocumentUploadModel documentUploadModel, int userId)
        {
            fileApplicationService.Move(documentUploadModel.DocumentModel.FileNameGuid,
                pathConstant.TemporaryStorePath, pathConstant.FileStorePath);

            var user = userRepository.Get(userId);
            var company = companyRepository.Get(user.Company.Id);
            var document = GetDocumentEntity(documentUploadModel, userId);
            UpdateDocumentTodoResult(company, document);
            schedulerService.DeterminateSustainabilityLevel(company.Id);
        }

        public DocumentUploadViewModel GetDocumentUploadViewModel(int companyId)
        {
            var types = documentTypeRepository.GetAll();
            var viewModel = new DocumentUploadViewModel
            {
                DocumentTypes = Mapper.Map<IEnumerable<BaseEnumModel>>(types)
            };
            return viewModel;
        }

        public IFilterResult<UserItemViewModel> FilterUsers(IFilter filter, int companyId)
        {
            Expression<Func<UserEntity, bool>> filterFn = c => c.CompanyId == companyId;
            return filterUtilses.Filter<UserEntity, UserItemViewModel>(filter, filterFn, userRepository);
        }

        public IFilterResult<DocumentItemViewModel> FilterDocuments(IFilter filter, int companyId)
        {
            Expression<Func<DocumentEntity, bool>> filterFn = c => c.User.Company.Id == companyId;
            return filterUtilses.Filter<DocumentEntity, DocumentItemViewModel>(filter, filterFn, documentRepository);
        }

        public int AddMetric(MetricsViewModel metricsViewModel, int companyId)
        {
            var metric = Mapper.Map<MetricEntity>(metricsViewModel);
            metric.CreateDate = DateTime.Now;
            UpdateMetricTodoResult(companyId, metric);
            schedulerService.DeterminateSustainabilityLevel(companyId);
            return metric.Id;
        }

        public void UpdateMetric(MetricsViewModel metricsViewModel, int companyId)
        {
            var metric = metricRepository.Get(metricsViewModel.Id);
            Mapper.Map(metricsViewModel, metric);
            metricRepository.Update(metric);
            schedulerService.DeterminateSustainabilityLevel(companyId);
        }

        public MetricsViewModel GetMetricsViewModel(int metricId, int companyId)
        {
            var metric = metricRepository.Get(metricId) ?? new MetricEntity();
            var result = Mapper.Map<MetricsViewModel>(metric);
            result.Metrics = GetMetrics(companyId);

            var documentFiscalYear = GetFiscalYearFromDocuments(companyId);
            var metricFiscalYear = GetFiscalYearFromMetrics(companyId);
            result.BaseYear = documentFiscalYear == null && metricFiscalYear == null;

            if (metricId == 0 && metricFiscalYear != null)
            {
                result.LastDayOfTheYear = result.Metrics.Max(m => m.MetricDate).AddYears(1)
                    .ToString("d", CultureInfo.CreateSpecificCulture("en-US"));
            }
            else if (metricId == 0 && documentFiscalYear != null)
            {
                result.LastDayOfTheYear = documentFiscalYear.Value.ToString("d",
                    CultureInfo.CreateSpecificCulture("en-US"));
            }

            return result;
        }

        public IEnumerable<MetricItemViewModel> GetMetrics(int companyId)
        {
            var metricTodoResult = metricTodoResultRepository.GetByCompanyId(companyId);
            var metrics = Mapper.Map<IEnumerable<MetricItemViewModel>>(metricTodoResult?.Metrics ?? Enumerable.Empty<MetricEntity>());
            return metrics.OrderBy(m => m.MetricDate);
        }

        public IEnumerable<string> CheckYear(int companyId, MetricsViewModel model)
        {
            var errors = new List<string>();

            var metrics = GetMetrics(companyId).ToList();
            var date = DateTime.Parse(model.LastDayOfTheYear, new CultureInfo("en-US"));
            var baseYear = metrics.OrderBy(m => m.MetricDate).FirstOrDefault();

            if (metrics.Any())
            {
                var otherMetrics = metrics.Where(m => m.Id != model.Id).ToList();
                if (otherMetrics.Any())
                {
                    if (date != otherMetrics.Max(x => x.MetricDate).AddYears(1)
                        && date != metrics.FirstOrDefault(m => m.Id == model.Id)?.MetricDate)
                    {
                        errors.Add("Date should be 12 months after the last metrics.");
                    }
                }
            }

            if (baseYear != null)
            {
                if (date.Date < baseYear.MetricDate.Date)
                {
                    errors.Add("Period is before the base year.");
                }
                if (model.Id == baseYear.Id && date.Date != baseYear.MetricDate)
                {
                    errors.Add("Base year is not editable.");
                }
            }

            if (date > DateTime.Now)
            {
                errors.Add("Period is in future.");
            }

            foreach (var metric in metrics)
            {
                // TODO: simplify if condition, try to extract into readable predicates
                if (
                    !((date.Date >= metric.MetricDate.Date.AddYears(1)) ||
                      (metric.MetricDate.Date >= date.Date.AddYears(1))) && (metric.Id != model.Id))
                {
                    errors.Add("Metrics for this period already exist.");
                }
            }

            return errors;
        }

        public SustainabilityPageViewModel GetSustainabilityPageViewModel(int companyId, int categoryId)
        {
            var targetCategory = sustainabilityCategoryRepository.Get(categoryId);
            var targetCategoryId = targetCategory?.Id ?? 0;
            var targetCategoryWeight = targetCategory?.OrderWeight ?? 0;
            var targetCategoryName = targetCategory?.Name ?? "No Response";

            var company = companyRepository.Get(companyId);
            var currentCategory = company.SustainabilityCategory;
            var currentCategoryId = currentCategory?.Id ?? 0;
            var currentCategoryWeight = currentCategory?.OrderWeight ?? 0;

            var categories = sustainabilityCategoryRepository.GetAll();
            var categoryItems = Mapper.Map<List<CategoryItemViewModel>>(categories);
            var isCurrent = targetCategoryWeight == currentCategoryWeight;
            var categoryViewModel = GetCategoryViewModel(isCurrent, categories, targetCategoryId, company);
            categoryItems.ForEach(x => x.Level = GetSustainabilityLevel(x.OrderWeight, currentCategoryWeight));

            var result = new SustainabilityPageViewModel
            {
                CurrentCategoryId = currentCategoryId,
                CurrentCategoryWeight = currentCategoryWeight,

                TargetCategoryId = targetCategoryId,
                TargetCategoryWeight = targetCategoryWeight,
                TargetCategoryName = targetCategoryName,
                TargetCategoryLevel = GetSustainabilityLevel(targetCategoryWeight, currentCategoryWeight),

                TargetCategoryModel = categoryViewModel,
                Categories = categoryItems
            };

            return result;
        }

        private CategoryViewModel GetCategoryViewModel(bool isCurrent, IEnumerable<SustainabilityCategoryEntity> categories,
            int targetCategoryId, CompanyEntity company)
        {
            var categoryViewModel = new CategoryViewModel();
            var todoItems = GetTodoItems(categories, targetCategoryId, isCurrent);
            var todoProcessors = todoProcessorUtilse.GetProcessors();
            var todoResults = todoProcessorUtilse.GetTodoResults(company.TodoResults);

            foreach (var todoItem in todoItems)
            {
                var targetType = todoItem.Key;

                var items = todoItems.ContainsKey(targetType)
                    ? todoItems[targetType].ToList()
                    : new List<BaseTodoItemEntity>();

                var todoResult = todoResults.ContainsKey(targetType)
                    ? todoResults[targetType]
                    : default(BaseTodoResultEntity);

                var itemsToInsert = todoProcessors[targetType].Validate(items, todoResult);
                var targetAchievement = categoryViewModel.Achievements.First(x => x.AchievementType == targetType);
                itemsToInsert.ToList().ForEach(item => targetAchievement.AchievementItems.Add(item));
            }
            return categoryViewModel;
        }

        private static Dictionary<TodoType, IEnumerable<BaseTodoItemEntity>> GetTodoItems(
            IEnumerable<SustainabilityCategoryEntity> categories,
            int targetWeight, bool includePreviousCategories)
        {
            var filteredCategories = includePreviousCategories
                ? categories.Where(x => x.OrderWeight <= targetWeight)
                : categories.Where(x => x.OrderWeight == targetWeight);

            var actualCategories = filteredCategories
                .OrderByDescending(x => x.OrderWeight);

            var groupedTodoItems = actualCategories
                .SelectMany(x => x.Todos)
                .Where(x => x.Enabled)
                .GroupBy(x => x.GetType());

            Func<Type, TodoType> getTodoType =
                type => type.GetCustomAttributes(true).OfType<TodoAttribute>().First().TodoType;

            var todoItems = groupedTodoItems.ToDictionary(
                x => getTodoType(x.Key),
                x => x.SelectMany(c => c.GetItems()));

            return todoItems;
        }

        public int GetActualCategoryId(int companyId)
        {
            var company = companyRepository.Get(companyId);
            return company.SustainabilityCategory?.Id ?? 0;
        }

        public bool IsCategoryExist(int categoryId)
        {
            var category = sustainabilityCategoryRepository.Get(categoryId);

            return category != null;
        }

        public bool HasAccess(int companyId, int categoryId)
        {
            if (categoryId == 0)
            {
                return true;
            }
            var targetCategory = sustainabilityCategoryRepository.Get(categoryId);
            if (targetCategory == null)
            {
                return false;
            }
            var currentCategory = companyRepository.Get(companyId).SustainabilityCategory;
            if (currentCategory == null)
            {
                return true;
            }
            return targetCategory.OrderWeight >= currentCategory.OrderWeight;
        }

        public void RestoreMetricsViewModel(MetricsViewModel model, int companyId)
        {
            model.Metrics = GetMetrics(companyId);
            var documentFiscalYear = GetFiscalYearFromDocuments(companyId);
            var metricFiscalYear = GetFiscalYearFromMetrics(companyId);
            model.BaseYear = documentFiscalYear == null && metricFiscalYear == null;
        }

        public UserEditViewModel GetUserEditViewModel(int? userId)
        {
            if (userId == null || userId == 0)
            {
                return new UserEditViewModel { UserStatus = UserStatus.Active };
            }
            var entityUser = userRepository.Get(userId.Value);
            var viewModel = Mapper.Map<UserEditViewModel>(entityUser);
            viewModel.EditMode = true;
            return viewModel;
        }

        public void UpdateUser(UserEditViewModel viewModel, int companyId)
        {
            var user = userRepository.Get(viewModel.Id);
            Mapper.Map(viewModel, user);
            userRepository.Update(user);
        }

        public void AddUser(UserEditViewModel viewModel, int companyId, int userId)
        {
            var user = userUtils.AddUser(viewModel, UserRole.CompanyUser);
            var link = securityKeyUtils.GenerateSecurityLink();
            mailService.SendSetPasswordEmail(user.Id, link.Id, userId);
        }

        public SurveyViewModel GetSurveyViewModel(int companyId)
        {
            var entities = questionRepository.GetAll();
            var questions = Mapper.Map<List<QuestionViewModel>>(entities);
            var surveyTodoResult = surveyTodoResultRepository.GetByCompanyId(companyId);

            if (surveyTodoResult?.Answers != null)
            {
                var answers = surveyTodoResult.Answers;
                questions.ForEach(question =>
                {
                    var entry = answers.FirstOrDefault(c => c.QuestionId == question.QuestionId);
                    if (entry != null)
                    {
                        question.Answer = entry.Answer;
                    }
                });
            }

            var viewModel = new SurveyViewModel
            {
                Questions = questions
            };
            return viewModel;
        }

        public void SubmitQuestions(IEnumerable<QuestionViewModel> questions, int companyId)
        {
            UpdateSurveyTodoResult(questions, companyId);
            schedulerService.DeterminateSustainabilityLevel(companyId);
        }

        private void UpdateSurveyTodoResult(IEnumerable<QuestionViewModel> questions, int companyId)
        {
            var answers = Mapper.Map<List<AnswerEntity>>(questions);
            var surveyTodoResult = surveyTodoResultRepository.GetByCompanyId(companyId);
            if (surveyTodoResult == null)
            {
                var entity = new SurveyTodoResultEntity
                {
                    CompanyId = companyId,
                    Answers = answers
                };
                surveyTodoResultRepository.Add(entity);
                return;
            }
            surveyTodoResult.Answers.Clear();
            surveyTodoResultRepository.SaveChanges();
            answers.ForEach(x => surveyTodoResult.Answers.Add(x));
            surveyTodoResultRepository.SaveChanges();
        }

        private static SustainabilityLevel GetSustainabilityLevel(int expectedWeight, int actualWeight)
        {
            if (expectedWeight == actualWeight)
            {
                return SustainabilityLevel.Current;
            }

            if (expectedWeight == actualWeight + 1)
            {
                return SustainabilityLevel.Next;
            }

            return expectedWeight > actualWeight
                ? SustainabilityLevel.Future
                : SustainabilityLevel.Previous;
        }

        private static DocumentEntity GetDocumentEntity(DocumentUploadModel documentUploadModel, int userId)
        {
            var originalFileName = documentUploadModel.DocumentModel.FileName +
                                   documentUploadModel.DocumentModel.ExpectedExtension;

            var document = new DocumentEntity
            {
                OriginalFileName = originalFileName,
                FileName = documentUploadModel.DocumentModel.FileNameGuid,
                Description = documentUploadModel.Description,
                UploadDate = DateTime.Today,
                FromDate = DateTime.Parse(documentUploadModel.FromDate, new CultureInfo("en-US")),
                ToDate = DateTime.Parse(documentUploadModel.ToDate, new CultureInfo("en-US")),

                UserId = userId,
                DocumentTypeId = documentUploadModel.DocumentTypeId
            };

            return document;
        }

        private void UpdateDocumentTodoResult(CompanyEntity company, DocumentEntity document)
        {
            var documentTodoResult = documentTodoResultRepository.GetByCompanyId(company.Id);

            if (documentTodoResult == null)
            {
                var entityToCreate = new DocumentTodoResultEntity
                {
                    Company = company,
                    Documents = new List<DocumentEntity>(new[] { document })
                };
                documentTodoResultRepository.Add(entityToCreate);
                return;
            }

            documentTodoResult.Documents.Add(document);
            documentTodoResultRepository.Update(documentTodoResult);
        }

        private void UpdateMetricTodoResult(int companyId, MetricEntity metric)
        {
            var metricTodoResult = metricTodoResultRepository.GetByCompanyId(companyId);
            if (metricTodoResult == null)
            {
                metricTodoResult = new MetricTodoResultEntity
                {
                    CompanyId = companyId,
                    Metrics = new List<MetricEntity>(new List<MetricEntity> { metric })
                };
                metricTodoResultRepository.Add(metricTodoResult);
            }
            metricTodoResult.Metrics.Add(metric);
            metricTodoResultRepository.Update(metricTodoResult);
        }

        private DateTime? GetFiscalYearFromMetrics(int companyId)
        {
            var metricTodoResult = metricTodoResultRepository.GetByCompanyId(companyId);
            var metrics = metricTodoResult?.Metrics ?? new List<MetricEntity>();
            var metric = metrics.OrderBy(x => x.MetricDate).FirstOrDefault();
            return metric?.MetricDate;
        }

        private DateTime? GetFiscalYearFromDocuments(int companyId)
        {
            var documentTodoResult = documentTodoResultRepository.GetByCompanyId(companyId);
            var documents = documentTodoResult?.Documents ?? new List<DocumentEntity>();
            var document = documents.OrderBy(x => x.FromDate).FirstOrDefault(x => x.ToDate == null);
            return document?.FromDate;
        }
    }
}