﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using STS.Application.Filters;
using STS.Application.Models;
using STS.Application.Models.Admin;
using STS.Application.Models.Category;
using STS.Application.Models.Company;
using STS.Application.Models.ManageSustainability;
using STS.Application.Services.Interfaces;
using STS.Application.Utils.Interfaces;
using STS.Application.Models.Report;
using STS.Application.Models.User;
using STS.CrossCutting.Enums;
using STS.CrossCutting.Utils;
using STS.Domain.Entities;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Entities.Users;
using STS.Domain.Repositories.Interfaces;

namespace STS.Application.Services.Implementations
{
    public class AdminService : IAdminService
    {
        private readonly ISustainabilityCategoryRepository sustainabilityCategoryRepository;
        private readonly IDocumentTypeRepository documentTypeRepository;
        private readonly ICompanyRepository companyRepository;

        private readonly IUserRepository userRepository;

        private readonly IReportDataService reportDataService;
        private readonly IMetricRepository metricRepository;
        private readonly IMetricTodoResultRepository metricTodoResultRepository;

        private readonly IFilterUtils filterUtils;

        private readonly IUserUtils userUtils;
        private readonly ISecurityKeyUtils securityKeyUtils;

        private readonly ISchedulerService schedulerService;
        private readonly IMailService mailService;
        private readonly ICompanyService companyService;

        private readonly IQuestionRepository questionRepository;

        public AdminService(ISustainabilityCategoryRepository sustainabilityCategoryRepository, IDocumentTypeRepository documentTypeRepository, ICompanyRepository companyRepository, IUserRepository userRepository, IReportDataService reportDataService, IMetricRepository metricRepository, IMetricTodoResultRepository metricTodoResultRepository, IFilterUtils filterUtils, IUserUtils userUtils, ISchedulerService schedulerService, IMailService mailService, ICompanyService companyService, ISecurityKeyUtils securityKeyUtils, IQuestionRepository questionRepository)
        {
            this.sustainabilityCategoryRepository = sustainabilityCategoryRepository;
            this.documentTypeRepository = documentTypeRepository;
            this.companyRepository = companyRepository;
            this.userRepository = userRepository;
            this.reportDataService = reportDataService;
            this.metricRepository = metricRepository;
            this.metricTodoResultRepository = metricTodoResultRepository;
            this.filterUtils = filterUtils;
            this.userUtils = userUtils;
            this.schedulerService = schedulerService;
            this.mailService = mailService;
            this.companyService = companyService;
            this.securityKeyUtils = securityKeyUtils;
            this.questionRepository = questionRepository;
        }

        public IFilterResult<UserItemViewModel> FilterUsers(Filter filter)
        {
            Expression<Func<UserEntity, bool>> filterFn = c => c.Roles.Any(x => x.Name == UserRole.SuperAdmin.ToString());
            return filterUtils.Filter<UserEntity, UserItemViewModel>(filter, filterFn, userRepository);
        }

        public UserEditViewModel GetUserEditViewModel(int userId)
        {
            if (userId == 0)
            {
                return new UserEditViewModel { UserStatus = UserStatus.Active };
            }
            var entityUser = userRepository.Get(userId);
            var viewModel = Mapper.Map<UserEditViewModel>(entityUser);
            return viewModel;
        }

        public void UpdateUser(UserEditViewModel userModel)
        {
            var user = userRepository.Get(userModel.Id);
            Mapper.Map(userModel, user);
            userRepository.Update(user);
        }

        public void AddUser(UserEditViewModel viewModel, int userId)
        {
            var userEntity = userUtils.AddUser(viewModel, UserRole.SuperAdmin);
            var link = securityKeyUtils.GenerateSecurityLink();
            mailService.SendSetPasswordEmail(userEntity.Id, link.Id, userId);
        }

        // Companies

        public IFilterResult<CompanyItemViewModel> FilterCompanies(IFilter filter)
        {
            return filterUtils.Filter<CompanyEntity, CompanyItemViewModel>(filter, x => true, companyRepository);
        }

        public void RestoreCompanyEditViewModel(CompanyEditViewModel viewModel)
        {
            if (!viewModel.EditMode) return;
            var previousViewModel = GetCompanyEditViewModel(viewModel.Company.Id);
            viewModel.Administrator = previousViewModel.Administrator;
        }

        public CompanyEditViewModel GetCompanyEditViewModel(int? companyId = null)
        {
            var companyEditViewModel = new CompanyEditViewModel();
            if (companyId == null)
            {
                companyEditViewModel.Company = new CompanyViewModel();
                companyEditViewModel.Administrator = new UserViewModel();
                return companyEditViewModel;
            }
            var company = companyRepository.Get(companyId.Value);
            companyEditViewModel.Company = Mapper.Map<CompanyViewModel>(company);
            companyEditViewModel.EditMode = true;
            return companyEditViewModel;
        }

        public void UpdateCompany(CompanyEditViewModel viewModel, int userId)
        {
            var company = companyRepository.Get(viewModel.Company.Id);
            Mapper.Map(viewModel.Company, company);
            companyRepository.Update(company);
        }

        public void AddCompany(CompanyEditViewModel viewModel, int userId)
        {
            var company = Mapper.Map<CompanyEntity>(viewModel.Company);
            company.CreatedOn = DateTime.Now;
            company = companyRepository.Add(company);
            AddCompanyUser(viewModel, company, userId);
        }

        // Rules

        public CategoriesViewModel GetCategoriesViewModel()
        {
            var documentTypes = documentTypeRepository.GetAll();
            var categories = sustainabilityCategoryRepository.GetAll();

            var viewModel = new CategoriesViewModel
            {
                DocumentTypes = Mapper.Map<IEnumerable<BaseEnumModel>>(documentTypes),
                Categories = Mapper.Map<IEnumerable<CategoryModel>>(categories),
                MetricTrends = EnumUtils.DisplayAllValues<MetricTrend>()
                    .Select(x => new BaseEnumModel { Id = x.Key, Name = x.Value }),
                DocumentConsiderations = EnumUtils.DisplayAllValues<DocumentConsideration>()
                    .Select(x => new BaseEnumModel { Id = x.Key, Name = x.Value }),
                MetricConsiderations = EnumUtils.DisplayAllValues<MetricConsideration>()
                    .Select(x => new BaseEnumModel { Id = x.Key, Name = x.Value })
            };

            return viewModel;
        }

        public void UpdateCategories(IEnumerable<CategoryModel> categories)
        {
            foreach (var categoryModel in categories)
            {
                var entity = sustainabilityCategoryRepository.Get(categoryModel.Id);
                if (entity == null)
                {
                    continue;
                }
                var todos = Mapper.Map<List<BaseTodoEntity>>(categoryModel.Todos);
                UpdateTodos(entity, todos);
                sustainabilityCategoryRepository.UpdateTodoItems(entity, todos);
            }
            schedulerService.DeterminateSustainabilityLevel();
        }

        // Reports

        public ReportsViewModel GetReportViewModel()
        {
            var result = new ReportsViewModel
            {
                StartDate = new DateTime(2014, 1, 1).ToString("d", CultureInfo.CreateSpecificCulture("en-US")),
                EndDate = DateTime.Now.ToString("d", CultureInfo.CreateSpecificCulture("en-US")),
                Reports = EnumUtils.DisplayAllValues<ReportTypes>()
                            .Select(x => new SelectListItem { Text = x.Value, Value = x.Key.ToString() })
            };
            return result;
        }

        public DatesErrorModel ValidateReportDates(string startDateString, string endDateString)
        {
            var result = new DatesErrorModel();
            var startDate = DateTime.Parse(startDateString, CultureInfo.CreateSpecificCulture("en-US"));
            var endDate = DateTime.Parse(endDateString, CultureInfo.CreateSpecificCulture("en-US"));
            if (startDate > endDate)
            {
                result.HasError = true;
                result.StartDateMessage = "Start date should be before the end date";
                result.EndDateMessage = "End date should be after the start date";
            }
            if (startDate > DateTime.Now)
            {
                result.HasError = true;
                result.StartDateMessage = "Start date should be in the past";
            }
            if (endDate > DateTime.Now)
            {
                result.HasError = true;
                result.EndDateMessage = "End date should be in the past";
            }
            return result;
        }

        public BaseReportData GetReportData(int reportType, string startDate, string endDate)
        {
            var request = new ReportsRequestModel
            {
                ReportType = reportType,
                StartDate = DateTime.Parse(startDate, CultureInfo.CreateSpecificCulture("en-US")),
                EndDate = DateTime.Parse(endDate, CultureInfo.CreateSpecificCulture("en-US")),
            };
            var result = reportDataService.GetReportData(request);
            return result;
        }

        // Sustainability

        public IEnumerable<BaseEnumModel> GetCompanies()
        {
            var entities = companyRepository.GetAll();
            var companies = Mapper.Map<IEnumerable<BaseEnumModel>>(entities);
            return companies;
        }

        public int GetTargetOrFirstCompanyId(int companyId)
        {
            var company = companyRepository.Get(companyId);
            return company == null
                ? companyRepository.GetFirstCompany()?.Id ?? 0
                : company.Id;
        }

        public DocumentsViewModel GetDocumentsViewModel(int companyId)
        {
            var documentTypes = documentTypeRepository.GetAll();
            return new DocumentsViewModel
            {
                CompanyId = companyId,
                DocumentTypes = Mapper.Map<IEnumerable<BaseEnumModel>>(documentTypes)
            };
        }

        public MetricsViewModel GetMetricsViewModel(int companyId)
        {
            var metricTodo = metricTodoResultRepository.GetAll().FirstOrDefault(x => x.Company.Id == companyId);
            var metric = metricTodo?.Metrics.OrderByDescending(x => x.MetricDate).FirstOrDefault();
            if (metric == null)
            {
                return null;
            }
            var result = Mapper.Map<MetricsViewModel>(metric);

            result.Metrics = GetMetrics(companyId);
            return result;
        }

        public MetricsViewModel GetMetricsViewModel(int companyId, int metricId)
        {
            var metric = metricRepository.Get(metricId);
            if (metric == null)
            {
                return null;
            }

            if (metricTodoResultRepository.GetAll().FirstOrDefault(x => x.Company.Id == companyId && x.Metrics.FirstOrDefault(m => m.Id == metricId) != null) == null)
            {
                return null;
            }

            var result = Mapper.Map<MetricsViewModel>(metric);

            result.Metrics = GetMetrics(companyId);
            return result;
        }

        public int GetActualCategoryIdForCompany(int companyId)
        {
            var company = companyRepository.Get(companyId);
            return company == null
                ? 0
                : company.SustainabilityCategory?.Id ?? 0;
        }

        public bool CompanyHasAccess(int companyId, int categoryId)
        {
            return companyService.HasAccess(companyId, categoryId);
        }

        public CompanySustainabilityViewModel GetCategories(int supplierId, int categoryId)
        {
            var communicateSustainabilityViewModel = companyService.GetSustainabilityPageViewModel(supplierId, categoryId);
            var result = new CompanySustainabilityViewModel
            {
                SustainabilityPageViewModel = communicateSustainabilityViewModel,
                CompanyId = supplierId
            };
            return result;
        }

        // Questions

        public IFilterResult<QuestionItemViewModel> FilterQuestions(IFilter filter)
        {
            Expression<Func<QuestionEntity, bool>> filterFn = c => true;
            return filterUtils.Filter<QuestionEntity, QuestionItemViewModel>(filter, filterFn, questionRepository);
        }

        public QuestionEditViewModel GetQuestionEditViewModel(int questionId)
        {
            var entity = questionRepository.Get(questionId);
            if (entity == null)
            {
                return new QuestionEditViewModel();
            }
            var viewModel = Mapper.Map<QuestionEditViewModel>(entity);
            viewModel.EditMode = true;
            return viewModel;
        }

        public void UpdateQuestion(QuestionEditViewModel viewModel)
        {
            var entity = Mapper.Map<QuestionEntity>(viewModel);
            questionRepository.Update(entity);
            schedulerService.DeterminateSustainabilityLevel();
        }

        public void AddQuestions(QuestionEditViewModel viewModel)
        {
            var entity = Mapper.Map<QuestionEntity>(viewModel);
            questionRepository.Add(entity);
            schedulerService.DeterminateSustainabilityLevel();
        }

        private static void UpdateTodos(SustainabilityCategoryEntity entity, IList<BaseTodoEntity> todos)
        {
            foreach (var todo in entity.Todos)
            {
                var entry = todos.FirstOrDefault(x => x.Id == todo.Id);
                if (entry != null)
                {
                    todo.Enabled = entry.Enabled;
                }
            }
        }

        private void AddCompanyUser(CompanyEditViewModel viewModel, CompanyEntity company, int inviterId)
        {
            var userEntity = userUtils.AddUser(viewModel.Administrator, UserRole.CompanyUser);
            userEntity.Company = company;
            userEntity.UserStatus = UserStatus.Active;
            userRepository.Update(userEntity);
            var link = securityKeyUtils.GenerateSecurityLink();
            mailService.SendSetPasswordEmail(userEntity.Id, link.Id, inviterId);
        }

        private IEnumerable<MetricItemViewModel> GetMetrics(int companyId)
        {
            var metricTodoResult = metricTodoResultRepository.GetByCompanyId(companyId);
            var metrics = Mapper.Map<IEnumerable<MetricItemViewModel>>(metricTodoResult?.Metrics ?? Enumerable.Empty<MetricEntity>());
            return metrics.OrderBy(m => m.MetricDate);
        }
    }
}