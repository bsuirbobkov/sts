﻿using System;
using System.Collections.Generic;
using System.Linq;
using STS.Application.FileValidators;
using STS.Application.Services.Interfaces;

namespace STS.Application.Services.Implementations
{
    public class FileValidatorService : IFileValidatorService
    {
        private readonly IDictionary<string, FileValidatorBase> validators;

        public FileValidatorService()
        {
            validators = new Dictionary<string, FileValidatorBase>(StringComparer.OrdinalIgnoreCase)
            {
                {".xlsx", new OoxFileValidator("/xl/workbook.xml")},
                {".docx", new OoxFileValidator("/word/document.xml")},
                {".doc", new CfbFileValidator(0x57, 0x6F, 0x72, 0x64, 0x2E, 0x44, 0x6F, 0x63, 0x75, 0x6D, 0x65, 0x6E, 0x74, 0x2E)},
                {".xls", new CfbFileValidator(0x4D, 0x69, 0x63, 0x72, 0x6F, 0x73, 0x6F, 0x66, 0x74, 0x20, 0x45, 0x78, 0x63, 0x65, 0x6C, 0x00)},
                {".pdf", new PdfValidator()}
            };
        }

        public bool IsValid(FileInfo fileInfo)
        {
            if (IsDataEmpty(fileInfo) || !IsExtensionExists(fileInfo))
            {
                return false;
            }

            var validator = validators[fileInfo.ExpectedExtension];
            var result = validator.IsValid(fileInfo.Data);

            return result;
        }

        private bool IsExtensionExists(FileInfo fileInfo)
        {
            return fileInfo.ExpectedExtension != null &&
                   validators.ContainsKey(fileInfo.ExpectedExtension);
        }

        private static bool IsDataEmpty(FileInfo fileInfo)
        {
            return fileInfo.Data == null ||
                   !fileInfo.Data.Any();
        }
    }
}