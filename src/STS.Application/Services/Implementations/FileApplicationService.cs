﻿using System;
using System.Diagnostics;
using System.IO;
using System.Web;
using STS.Application.FileValidators;
using STS.Application.Models.Document;
using STS.Application.Services.Interfaces;
using TwentyTwenty.Storage;
using FileInfo = STS.Application.FileValidators.FileInfo;

namespace STS.Application.Services.Implementations
{
    public class FileApplicationService : IFileService
    {
        private readonly IStorageProvider provider;
        private readonly IFileValidatorService fileValidator;

        public FileApplicationService(IStorageProvider provider, IFileValidatorService fileValidator)
        {
            this.provider = provider;
            this.fileValidator = fileValidator;
        }

        public FileSaveResult Save(FileInfo file, string containerName)
        {
            var oldFileName = file.FileName;

            if (!fileValidator.IsValid(file))
            {
                return new FileSaveResult(true, "File is not valid", oldFileName, null);
            }

            try
            {
                var fileNameGuid = Guid.NewGuid() + file.ExpectedExtension;
                provider.SaveBlobStream(containerName, fileNameGuid, new MemoryStream(file.Data));
                return new FileSaveResult(false, null, oldFileName, fileNameGuid);
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
                return new FileSaveResult(true, "File saving error", oldFileName, null);
            }
        }

        public bool Move(string fileName, string fromPath, string toPath)
        {
            try
            {
                var fileToMove = provider.GetBlobStream(fromPath, fileName);
                provider.SaveBlobStream(toPath, fileName, fileToMove);
                fileToMove.Close();
                provider.DeleteBlob(fromPath, fileName);
                return true;
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
                return false;
            }
        }

        public FileDownloadModel GetDownloadData(string fileName, string fromPath)
        {
            FileDownloadModel result = null;
            using (var stream = provider.GetBlobStream(fromPath, fileName))
            {
                var bytes = ReadFully(stream);
                var mimeType = MimeMapping.GetMimeMapping(fileName);

                result = new FileDownloadModel
                {
                    Data = bytes,
                    MimeType = mimeType
                };

                stream.Close();
            }

            return result;
        }

        public static byte[] ReadFully(Stream input)
        {
            var buffer = new byte[16 * 1024];
            using (var ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
