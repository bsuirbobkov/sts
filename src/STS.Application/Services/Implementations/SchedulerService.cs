﻿using System.Collections.Generic;
using System.Linq;
using Hangfire;
using STS.Application.Processors;
using STS.Application.Services.Interfaces;
using STS.Application.Utils.Interfaces;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;
using STS.Domain.Entities.Sustainability;
using STS.Domain.Repositories.Interfaces;

namespace STS.Application.Services.Implementations
{
    public class SchedulerService : ISchedulerService
    {
        private readonly ISustainabilityCategoryRepository sustainabilityCategoryRepository;
        private readonly ICompanyRepository companyRepository;
        private readonly ITodoProcessorUtils todoProcessorUtils;
        private readonly Dictionary<TodoType, IBaseTodoProcessor> todoProcessors;
        private readonly IMailService mailService;

        public SchedulerService(
            ISustainabilityCategoryRepository sustainabilityCategoryRepository,
            ICompanyRepository companyRepository,
            ITodoProcessorUtils todoProcessorUtils,
            IMailService mailService)
        {
            this.sustainabilityCategoryRepository = sustainabilityCategoryRepository;
            this.companyRepository = companyRepository;
            this.todoProcessorUtils = todoProcessorUtils;
            this.mailService = mailService;
            todoProcessors = todoProcessorUtils.GetProcessors();
        }

        public void DeterminateSustainabilityLevel()
        {
            var companies = companyRepository.GetAll();
            foreach (var company in companies)
            {
                BackgroundJob.Enqueue<ISchedulerService>(x => x.ProcessingSustainabilityCategory(company.Id));
            }
        }

        public void DeterminateSustainabilityLevel(int companyId)
        {
            BackgroundJob.Enqueue<ISchedulerService>(x => x.ProcessingSustainabilityCategory(companyId));
        }

        public bool CheckTodoRules(SustainabilityCategoryEntity category, ICollection<BaseTodoResultEntity> todoResults)
        {
            var baseTodoResults = todoProcessorUtils.GetTodoResults(todoResults);

            var result = true;

            foreach (var todo in category.Todos.Where(x => x.Enabled))
            {
                var todoType = GetTodoTypeByTodo(todo);
                if (todoType == null)
                {
                    continue;
                }

                var todoTypeValue = todoType.Value;

                var processor = todoProcessors[todoTypeValue];
                var baseTodoResult = baseTodoResults.ContainsKey(todoTypeValue) ? baseTodoResults[todoTypeValue] : null;
                var verify = processor.Verify(todo, baseTodoResult);
                result &= verify;
            }

            return result;
        }

        private static TodoType? GetTodoTypeByTodo(BaseTodoEntity todo)
        {
            return todo.GetType().GetCustomAttributes(true).OfType<TodoAttribute>().FirstOrDefault()?.TodoType;
        }

        public void ProcessingSustainabilityCategory(int companyId)
        {
            var company = companyRepository.Get(companyId);

            var categories = sustainabilityCategoryRepository.GetAll().OrderBy(x => x.OrderWeight).ToList();

            SustainabilityCategoryEntity previousSustainabilityCategory = null;
            SustainabilityCategoryEntity curentSustainabilityCategory = null;

            foreach (var category in categories)
            {
                if (!CheckTodoRules(category, company.TodoResults))
                {
                    curentSustainabilityCategory = previousSustainabilityCategory;
                    break;
                }
                previousSustainabilityCategory = category;
            }
            var newCompanySustainabilityCategory = curentSustainabilityCategory ?? previousSustainabilityCategory;

            bool? isIncrease = null;
            if (newCompanySustainabilityCategory?.OrderWeight > company.SustainabilityCategory?.OrderWeight || newCompanySustainabilityCategory != null && company.SustainabilityCategory == null)
            {
                isIncrease = true;
            }
            else if (newCompanySustainabilityCategory?.OrderWeight < company.SustainabilityCategory?.OrderWeight || newCompanySustainabilityCategory == null && company.SustainabilityCategory != null)
            {
                isIncrease = false;
            }

            if (isIncrease.HasValue)
            {
                mailService.SendChangeLevelEmail(company, isIncrease.Value, newCompanySustainabilityCategory?.Name, company.SustainabilityCategory?.Name);
                company.SustainabilityCategory = newCompanySustainabilityCategory;
            }

            companyRepository.Update(company);
        }
    }
}