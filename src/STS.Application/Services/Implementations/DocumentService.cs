﻿using System.Linq;
using STS.Application.Models.Document;
using STS.Application.Services.Interfaces;
using STS.CrossCutting.Constans;
using STS.Domain.Repositories.Interfaces;

namespace STS.Application.Services.Implementations
{
    public class DocumentService : IDocumentService
    {
        private readonly IDocumentRepository documentRepository;
        private readonly IFileService fileApplicationService;
        private readonly PathConstant pathConstant;

        public DocumentService(
            IDocumentRepository documentRepository,
            IFileService fileApplicationService, PathConstant pathConstant)
        {
            this.documentRepository = documentRepository;
            this.fileApplicationService = fileApplicationService;
            this.pathConstant = pathConstant;
        }

        public FileDownloadModel GetDocument(int id)
        {
            var document = documentRepository.Get(id);
            var result = fileApplicationService.GetDownloadData(document.FileName, pathConstant.FileStorePath);
            result.FileName = document.OriginalFileName;
            return result;
        }

        public bool HasAccess(int userId, int documentId)
        {
            var document = documentRepository.Get(documentId);
            if (document == null)
            {
                return false;
            }
            var targetCompany = document.User.Company;
            var shouldGiveAccess = targetCompany.Users.Any(x => x.Id == userId);
            return shouldGiveAccess;
        }
    }
}