using System.Linq;
using STS.Application.Models.Report;
using STS.Application.Models.Report.Data;
using STS.Application.Models.Report.Series;
using STS.Application.Services.Interfaces;
using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;
using STS.Domain.Repositories.Interfaces;

namespace STS.Application.Services.Implementations
{
    public class ReportDataService : IReportDataService
    {
        private readonly ICompanyRepository companyRepository;

        public ReportDataService(ICompanyRepository companyRepository)
        {
            this.companyRepository = companyRepository;
        }

        public BaseReportData GetReportData(ReportsRequestModel request)
        {
            var methodInfo =
                typeof(ReportDataService).Assembly.GetTypes()
                    .SelectMany(t => t.GetMethods())
                    .FirstOrDefault(m =>
                    {
                        var reportTypeAttribute =
                            m.GetCustomAttributes(typeof(ReportTypeAttribute), false).FirstOrDefault() as
                                ReportTypeAttribute;

                        return reportTypeAttribute != null && (int) reportTypeAttribute.ReportType == request.ReportType;
                    });
            var result = (BaseReportData) methodInfo?.Invoke(this, new object[] {request});
            return result;
        }

        [ReportType(ReportTypes.CompanyDistributionBySustainabilityLevelTotal)]
        public CompanyDistributionBySustainabilityLevelTotalReportData
            SupplierDistributionBySustainabilityLevelTotalReport(ReportsRequestModel request)
        {
            var result = new CompanyDistributionBySustainabilityLevelTotalReportData
            {
                ReportType = request.ReportType
            };

            var companies = companyRepository.GetAll().ToList();
            var totalCount = companies.Count;
            if (totalCount == 0)
            {
                return result;
            }

            var groupBy = companies.GroupBy(x => x.SustainabilityCategory, x => x).ToList();
            result.Series = groupBy.Select(x =>
            {
                var name = x.Key?.Name;
                var y = (decimal) x.Count() * 100 / totalCount;
                return new TotalSerie(name, y, x.Count());
            }).ToList();
            return result;
        }
    }
}