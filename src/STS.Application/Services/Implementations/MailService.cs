﻿using System.Globalization;
using System.Linq;
using System.Web;
using STS.Application.Models.EmailTemplates;
using STS.Application.Services.Interfaces;
using STS.CrossCutting.Constans;
using STS.CrossCutting.Email;
using STS.CrossCutting.Enums;
using STS.CrossCutting.Models.Cryptography;
using STS.Domain.Entities;
using STS.Domain.Entities.Users;
using STS.Domain.Repositories.Interfaces;

namespace STS.Application.Services.Implementations
{
    public class MailService : IMailService
    {
        private readonly IUserRepository userRepository;
        private readonly ISecurityKeyRepository securityKeyRepository;
        private readonly Constants constants;
        private readonly EmailManager emailManager;
        private readonly ISendEmailService sendEmailService;
        private readonly ICrypter crypter;

        public MailService(
            IUserRepository userRepository,
            ICrypter crypter,
            ISecurityKeyRepository securityKeyRepository,
            Constants constants,
            EmailManager emailManager,
            ISendEmailService sendEmailService)
        {
            this.userRepository = userRepository;
            this.crypter = crypter;
            this.securityKeyRepository = securityKeyRepository;
            this.constants = constants;
            this.emailManager = emailManager;
            this.sendEmailService = sendEmailService;
        }

        public void SendSetPasswordEmail(int userId, int linkId, int inviterId)
        {
            var user = userRepository.Get(userId);
            var inviter = userRepository.Get(inviterId);
            var link = securityKeyRepository.Get(linkId);
            var cryptedString = crypter.Encrypt(string.Format(CultureInfo.InvariantCulture, "{0};{1}", linkId, userId));

            var resetPasswordUrl = constants.Links.SetPassword + HttpUtility.UrlEncode(cryptedString);

            const string subject = "Welcome!";

            var linkModel = new LinkModel
            {
                Link = resetPasswordUrl,
                ExpireDate = link.ExpireDate.ToString("d", CultureInfo.GetCultureInfo("en-US"))
            };

            emailManager.SendMessage(user.Email, user.Email, subject, GetEmailBody(linkModel, inviter));
        }

        public void SendChangeLevelEmail(CompanyEntity company, bool isIncrease, string newCompanySustainabilityCategoryName, string companySustainabilityCategoryName)
        {
            var changeLevelModel = new ChangeLevelModel
            {
                CompanyName = company.Name,
                CommunicateSustainabilityLink = constants.Links.CommunicateSustainability,
                CompanySustainabilityLink = $"{constants.Links.SupplierSustainability}{company.Id}",
                IsIncrease = isIncrease,
                NewCategoryName = newCompanySustainabilityCategoryName,
                PreviousCategoryName = companySustainabilityCategoryName
            };

            string subject = $"Change Level for {company.Name}";

            foreach (var user in company.Users.Where(x => x.Roles.Any(r => r.Name == nameof(UserRole.CompanyUser))))
            {
                emailManager.SendMessage(user.Email, user.Email, subject, sendEmailService.RenderChangeLevelCompanyTemplate(changeLevelModel));
            }

            foreach (var user in userRepository.GetAllSystemAdmins())
            {
                emailManager.SendMessage(user.Email, user.Email, subject, sendEmailService.RenderChangeAdminLevelTemplate(changeLevelModel));
            }
        }

        private string GetEmailBody(LinkModel linkModel, UserEntity inviter)
        {
            return sendEmailService.RenderSignUpTemplate(new SignUpModel
            {
                InviterName = $"{inviter?.FirstName} {inviter?.LastName}",
                Link = linkModel.Link,
                ExpirationDate = linkModel.ExpireDate
            });
        }
    }
}