﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using STS.CrossCutting.Constans;
using STS.Domain.Entities.Users;
using STS.Domain.Repositories.Interfaces;

namespace STS.Application.Identity
{
    public class ApplicationUserStore :
        IUserPasswordStore<ApplicationUser>,
        IUserRoleStore<ApplicationUser>,
        IUserClaimStore<ApplicationUser>,
        IUserLockoutStore<ApplicationUser>
    {
        private readonly IUserRepository userRepository;
        private bool disposed;

        public ApplicationUserStore(
            IUserRepository userRepository)
        {
            this.userRepository = userRepository;
            disposed = false;
        }

        public async Task<string> GetUserIdAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() => user.Id.ToString(), cancellationToken);
        }

        public async Task<string> GetUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() => user.UserName, cancellationToken);
        }

        public async Task SetUserNameAsync(ApplicationUser user, string userName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            user.UserName = userName;
            await Task.FromResult(0);
        }

        public async Task<string> GetNormalizedUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() => user.NormalizedUserName, cancellationToken);
        }

        public async Task SetNormalizedUserNameAsync(ApplicationUser user, string normalizedName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            user.NormalizedUserName = normalizedName;
            await Task.FromResult(0);
        }

        public async Task<IdentityResult> CreateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() =>
            {
                try
                {
                    var userEntity = Mapper.Map<UserEntity>(user);
                    userRepository.Add(userEntity);
                    return IdentityResult.Success;
                }
                catch
                {
                    return IdentityResult.Failed();
                }
            }, cancellationToken);
        }

        public async Task<IdentityResult> UpdateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() =>
            {
                var userEntity = userRepository.Get(user.Id);
                if (userEntity == null)
                {
                    return IdentityResult.Failed();
                }
                Mapper.Map(user, userEntity);
                userRepository.Update(userEntity);
                return IdentityResult.Success;
            }, cancellationToken);
        }

        public async Task<IdentityResult> DeleteAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() =>
            {
                try
                {
                    var userEntity = Mapper.Map<UserEntity>(user);
                    userRepository.Remove(userEntity);
                    return IdentityResult.Success;
                }
                catch
                {
                    return IdentityResult.Failed();
                }
            }, cancellationToken);
        }

        public async Task<ApplicationUser> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() =>
            {
                try
                {
                    int id;
                    int.TryParse(userId, out id);
                    var userEntity = userRepository.Get(id);
                    var user = Mapper.Map<ApplicationUser>(userEntity);
                    return user;
                }
                catch
                {
                    return null;
                }
            }, cancellationToken);
        }

        public async Task<ApplicationUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() =>
            {
                var userEntity = userRepository.GetByNormalizeUserName(normalizedUserName);
                var applicationUser = Mapper.Map<ApplicationUser>(userEntity);
                return applicationUser;
            }, cancellationToken);
        }

        public async Task SetPasswordHashAsync(ApplicationUser user, string passwordHash, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            user.PasswordHash = passwordHash;
            await Task.FromResult(0);
        }

        public async Task<string> GetPasswordHashAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() => user.PasswordHash, cancellationToken);
        }

        public async Task<bool> HasPasswordAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() => !string.IsNullOrEmpty(user.PasswordHash), cancellationToken);
        }

        // UserRoleStore

        public Task AddToRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task RemoveFromRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public async Task<IList<string>> GetRolesAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() =>
            {
                var roles = user.Roles.Select(x => x.Name).ToList();
                return roles;
            }, cancellationToken);
        }

        public async Task<bool> IsInRoleAsync(ApplicationUser user, string roleName, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.Factory.StartNew(() => user.Roles.Any(x => string.Equals(x.Name, roleName, StringComparison.InvariantCultureIgnoreCase)), cancellationToken);
        }

        public Task<IList<ApplicationUser>> GetUsersInRoleAsync(string roleName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        // UserClaimStore

        public async Task<IList<Claim>> GetClaimsAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            var claims = new List<Claim>();
            if (user.CompanyId != null)
            {
                claims.Add(new Claim(ClaimTypesConstant.CompanyId, user.CompanyId.ToString()));
            }
            if (!string.IsNullOrEmpty(user.CompanyName))
            {
                claims.Add(new Claim(ClaimTypesConstant.CompanyName, user.UserName));
            }
            claims.Add(new Claim(ClaimTypesConstant.UserName, user.UserName));
            return await Task.FromResult(claims);
        }

        public Task AddClaimsAsync(ApplicationUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task ReplaceClaimAsync(ApplicationUser user, Claim claim, Claim newClaim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task RemoveClaimsAsync(ApplicationUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<IList<ApplicationUser>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        // Locout store


        public async Task<DateTimeOffset?> GetLockoutEndDateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.FromResult(user.LockoutEndDate);
        }

        public async Task SetLockoutEndDateAsync(ApplicationUser user, DateTimeOffset? lockoutEnd, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            user.LockoutEndDate = lockoutEnd;
            await Task.FromResult(0);
        }

        public async Task<int> IncrementAccessFailedCountAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            user.AccessFailedCount++;
            return await Task.FromResult(user.AccessFailedCount);
        }

        public async Task ResetAccessFailedCountAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            user.AccessFailedCount = 0;
            await Task.FromResult(0);
        }

        public async Task<int> GetAccessFailedCountAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.FromResult(user.AccessFailedCount);
        }

        public async Task<bool> GetLockoutEnabledAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            return await Task.FromResult(user.LockoutEnabled);
        }

        public async Task SetLockoutEnabledAsync(ApplicationUser user, bool enabled, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            ThrowIfDisposed();
            user.LockoutEnabled = enabled;
            await Task.FromResult(0);
        }

        private void ThrowIfDisposed()
        {
            if (disposed)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            disposed = disposing;
        }

    }
}