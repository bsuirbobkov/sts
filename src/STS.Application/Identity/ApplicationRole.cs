﻿using STS.CrossCutting.Enums;

namespace STS.Application.Identity
{
    public class ApplicationRole
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public UserRole UserRole { get; set; }

        public ApplicationRole()
        {
            UserRole = UserRole.None;
        }

        public ApplicationRole(UserRole role)
        {
            UserRole = role;
        }
    }
}