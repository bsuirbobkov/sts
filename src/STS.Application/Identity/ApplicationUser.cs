﻿using System;
using System.Collections.Generic;
using System.Linq;
using STS.CrossCutting.Enums;

namespace STS.Application.Identity
{
    public class ApplicationUser
    {
        public ApplicationUser()
        {
            Roles = new List<ApplicationRole>();
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public UserStatus UserStatus { get; set; }

        public DateTimeOffset? LockoutEndDate { get; set; }
        public int AccessFailedCount { get; set; }
        public bool LockoutEnabled { get; set; }


        public int? CompanyId { get; set; }
        public string CompanyName { get; set; }

        public IList<ApplicationRole> Roles { get; set; }

        public bool IsInRole(UserRole userRole)
        {
            var hasRole = Roles.Any(x => x.UserRole == userRole);
            return hasRole;
        }
    }
}