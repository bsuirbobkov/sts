﻿namespace STS.Application.Models
{
    public class BaseEnumModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}