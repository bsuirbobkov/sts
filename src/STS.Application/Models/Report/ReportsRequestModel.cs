﻿using System;
using System.ComponentModel.DataAnnotations;

namespace STS.Application.Models.Report
{
    public class ReportsRequestModel
    {
        [Required]
        public int ReportType { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
