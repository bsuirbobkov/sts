﻿namespace STS.Application.Models.Report
{
    public class DatesErrorModel
    {
        public string StartDateMessage { get; set; }
        public string EndDateMessage { get; set; }
        public bool HasError { get; set; }
    }
}
