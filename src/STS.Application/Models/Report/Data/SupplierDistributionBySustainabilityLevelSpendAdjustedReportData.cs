﻿using System.Collections.Generic;
using STS.Application.Models.Report.Series;

namespace STS.Application.Models.Report.Data
{
    public class SupplierDistributionBySustainabilityLevelSpendAdjustedReportData : BaseReportData
    {
        public IList<Serie> Series { get; set; }
    }
}
