﻿using System.Collections.Generic;
using STS.Application.Models.Report.Series;

namespace STS.Application.Models.Report.Data
{
    public class MonthlySupplierParticipationTrendSpendAdjustedData : BaseReportData
    {
        public IList<ParticipationSerie> Series { get; set; }
    }
}
