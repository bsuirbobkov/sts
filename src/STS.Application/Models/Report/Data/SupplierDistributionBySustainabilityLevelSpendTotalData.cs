﻿using System.Collections.Generic;
using STS.Application.Models.Report.Series;

namespace STS.Application.Models.Report.Data
{
    public class CompanyDistributionBySustainabilityLevelTotalReportData : BaseReportData
    {
        public IList<TotalSerie> Series { get; set; }
    }
}
