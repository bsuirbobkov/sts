﻿using System.Collections.Generic;
using STS.Application.Models.Report.Series;

namespace STS.Application.Models.Report.Data
{
    public class CompanyScope1AndScope2GhgEmissionsData : BaseReportData
    {
        public IList<EmissionsSerie> Series { get; set; }
    }
}
