﻿namespace STS.Application.Models.Report.Series
{
    public class Serie
    {
        public string Name { get; set; }
        public decimal Y { get; set; }
    }
}