﻿using System;

namespace STS.Application.Models.Report.Series
{
    public class ParticipationSerie
    {
        public DateTime MonthYear { get; set; }
        public int Suppliers { get; set; }
        public decimal NotCompletedSurvey { get; set; }
        public decimal CompletedSurvey { get; set; }
        public decimal HaveSubscription { get; set; }
    }
}
