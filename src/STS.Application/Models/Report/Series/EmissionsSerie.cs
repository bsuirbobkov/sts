﻿using System;

namespace STS.Application.Models.Report.Series
{
    public class EmissionsSerie
    {
        public decimal Scope1 { get; set; }
        public decimal Scope2 { get; set; }
        public decimal Y { get; set; }
        public int SupplierCount { get; set; }
        public DateTime MonthYear { get; set; }
    }
}
