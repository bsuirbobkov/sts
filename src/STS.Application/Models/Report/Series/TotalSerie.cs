﻿namespace STS.Application.Models.Report.Series
{
    public class TotalSerie
    {
        public TotalSerie(string name, decimal y, int number)
        {
            Name = name;
            Y = y;
            Number = number;
        }

        public string Name { get; set; }
        public decimal Y { get; set; }
        public int Number { get; set; }
    }
}
