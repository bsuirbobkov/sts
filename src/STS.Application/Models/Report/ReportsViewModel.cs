﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace STS.Application.Models.Report
{
    public class ReportsViewModel
    {
        [Required]
        [Display(Name = "Report Type")]
        public int ReportType { get; set; }

        public IEnumerable<SelectListItem> Reports { get; set; }

        [Display(Name = "Start Date")]
        public string StartDate { get; set; }

        [Display(Name = "End Date")]
        public string EndDate { get; set; }
    }
}
