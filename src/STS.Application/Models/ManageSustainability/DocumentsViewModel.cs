﻿using System.Collections.Generic;

namespace STS.Application.Models.ManageSustainability
{
    public class DocumentsViewModel : BaseSustainabilityTabModel
    {
        public IEnumerable<BaseEnumModel> DocumentTypes { get; set; }
    }
}