﻿namespace STS.Application.Models.ManageSustainability
{
    public class ErrorModel : BaseSustainabilityTabModel
    {
        public string Message { get; set; }
    }
}
