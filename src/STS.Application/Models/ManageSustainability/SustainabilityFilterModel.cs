﻿using System.Collections.Generic;

namespace STS.Application.Models.ManageSustainability
{
    public class SustainabilityFilterModel
    {
        public int CompanyId { get; set; }
        public IEnumerable<BaseEnumModel> Suppliers { get; set; }
    }
}