﻿using STS.Application.Models.Sustainability;

namespace STS.Application.Models.ManageSustainability
{
    public class CompanySustainabilityViewModel : BaseSustainabilityTabModel
    {
        public SustainabilityPageViewModel SustainabilityPageViewModel { get; set; }
    }
}