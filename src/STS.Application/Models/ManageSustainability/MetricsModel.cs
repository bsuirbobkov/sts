﻿
using STS.Application.Models.Company;

namespace STS.Application.Models.ManageSustainability
{
    public class MetricsModel : BaseSustainabilityTabModel
    {
        public MetricsViewModel Metrics { get; set; }
    }
}
