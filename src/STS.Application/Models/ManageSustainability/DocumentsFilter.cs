﻿using STS.Application.Filters;

namespace STS.Application.Models.ManageSustainability
{
    public class DocumentsFilter : IFilter
    {
        public string SortColumns { get; set; }
        public bool SortAscending { get; set; }
        public int CurrentPage { get; set; }
        public int RowsPerPage { get; set; }
        public int DocumentTypeId { get; set; }
    }
}