﻿namespace STS.Application.Models.EmailTemplates
{
    public class SignUpSupplierPrimaryModel
    {
        public string LeadCompanyName { get; set; }
        public string SupplierCompanyName { get; set; }
        public string Link { get; set; }
        public string ExpirationDate { get; set; }
        public string LeadCompanyPrimaryContactName { get; set; }
        public string LeadCompanyPrimaryContactEmail { get; set; }
        public bool IsAdmin { get; set; }
    }
}
