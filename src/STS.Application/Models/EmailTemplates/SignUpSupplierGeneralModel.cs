﻿namespace STS.Application.Models.EmailTemplates
{
    public class SignUpSupplierGeneralModel
    {
        public string SupplierCompanyAdminName { get; set; }
        public string Link { get; set; }
        public string ExpirationDate { get; set; }
    }
}
