﻿using System;

namespace STS.Application.Models.EmailTemplates
{
    public class SetPasswordModel
    {
        public DateTime ExpireDate { get; set; }
        public string Link { get; set; }
        public string LeadCompanyName { get; set; }
        public string SupplierCompanyName { get; set; }
        public string PrimaryContactName { get; set; }
        
    }
}