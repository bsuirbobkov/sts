﻿namespace STS.Application.Models.EmailTemplates
{
    public class SignUpSupplierAdminModel
    {
        public string LeadCompanyName { get; set; }
        public string SupplierCompanyName { get; set; }
        public string PrimaryContactName { get; set; }
        public string Link { get; set; }
    }
}
