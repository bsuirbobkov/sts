﻿namespace STS.Application.Models.EmailTemplates
{
    public class ChangeLevelModel
    {
        public bool IsIncrease { get; set; }
        public string CompanyName { get; set; }
        public string NewCategoryName { get; set; }
        public string PreviousCategoryName { get; set; }
        public string CommunicateSustainability { get; set; }
        public string CommunicateSustainabilityLink { get; set; }
        public string CompanySustainabilityLink { get; set; }
    }
}