﻿namespace STS.Application.Models.EmailTemplates
{
    public class SignUpModel
    {
        public string Link { get; set; }
        public string ExpirationDate { get; set; }
        public string InviterName { get; set; }
    }
}
