﻿namespace STS.Application.Models.EmailTemplates
{
    public class SignUpLeadGeneralModel
    {
        public string LeadCompanyAdminName { get; set; }
        public string Link { get; set; }
        public string ExpirationDate { get; set; }
    }
}
