﻿namespace STS.Application.Models.EmailTemplates
{
    public class LinkModel
    {
        public string Link { get; set; }
        public string ExpireDate { get; set; }
    }
}
