﻿using System.Collections.Generic;
using STS.Application.Models.Sustainability.AchievementItems;
using STS.CrossCutting.Enums;

namespace STS.Application.Models.Sustainability
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
            Achievements = new List<AchievementViewModel>
            {
                new AchievementViewModel
                {
                    AchievementItems = new List<BaseAchievementItemViewModel>(),
                    AchievementType = TodoType.DocumentTodo
                },
                new AchievementViewModel
                {
                    AchievementItems = new List<BaseAchievementItemViewModel>(),
                    AchievementType = TodoType.SurveyTodo
                },
                new AchievementViewModel
                {
                    AchievementItems = new List<BaseAchievementItemViewModel>(),
                    AchievementType = TodoType.MetricTodo
                }
            };
        }

        public IList<AchievementViewModel> Achievements { get; set; }
    }
}