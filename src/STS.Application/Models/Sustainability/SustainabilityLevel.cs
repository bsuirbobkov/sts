﻿namespace STS.Application.Models.Sustainability
{
    public enum SustainabilityLevel
    {
        Previous = 1,
        Current = 2,
        Next = 3,
        Future = 4
    }
}