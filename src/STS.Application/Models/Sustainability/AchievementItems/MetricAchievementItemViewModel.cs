﻿using System;
using STS.CrossCutting.Enums;

namespace STS.Application.Models.Sustainability.AchievementItems
{
    public class MetricAchievementItemViewModel : BaseAchievementItemViewModel
    {
        public bool Increase { get; set; }

        public MetricTrend MetricTrend { get; set; }

        public int ThresholdByRule { get; set; }

        public decimal ThresholdCurrent { get; set; }

        public MetricConsideration ConsiderationTime { get; set; }

        public DateTime ExpireDate { get; set; }
    }
}