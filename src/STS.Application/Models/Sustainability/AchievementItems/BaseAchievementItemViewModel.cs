﻿namespace STS.Application.Models.Sustainability.AchievementItems
{
    public abstract class BaseAchievementItemViewModel
    {
        public bool Achieved { get; set; }
        public int ConsiderationTimeInMonths { get; set; }
    }
}