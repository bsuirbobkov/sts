﻿using System;

namespace STS.Application.Models.Sustainability.AchievementItems.SuitableItems
{
    public class DocumentItemViewModel : BaseEnumModel
    {
        public DateTime ExpireDate { get; set; }
    }
}
