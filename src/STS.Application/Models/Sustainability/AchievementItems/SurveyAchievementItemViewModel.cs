﻿namespace STS.Application.Models.Sustainability.AchievementItems
{
    public class SurveyAchievementItemViewModel : BaseAchievementItemViewModel
    {
        public int RequiredScores { get; set; }
    }
}