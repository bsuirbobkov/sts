﻿using System.Collections.Generic;
using STS.Application.Models.Sustainability.AchievementItems.SuitableItems;
using STS.CrossCutting.Enums;

namespace STS.Application.Models.Sustainability.AchievementItems
{
    public class DocumentAchievementItemViewModel : BaseAchievementItemViewModel
    {
        public string DocumentTypeName { get; set; }

        public int Quantity { get; set; }

        public DocumentConsideration ConsiderationTime { get; set; }

        public ICollection<DocumentItemViewModel> Documents { get; set; }
    }
}