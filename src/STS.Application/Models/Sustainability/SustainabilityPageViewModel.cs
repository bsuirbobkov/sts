﻿using System.Collections.Generic;

namespace STS.Application.Models.Sustainability
{
    public class SustainabilityPageViewModel
    {
        public int CurrentCategoryId { get; set; }
        public int CurrentCategoryWeight { get; set; }

        public int TargetCategoryId { get; set; }
        public int TargetCategoryWeight { get; set; }
        public string TargetCategoryName { get; set; }
        public SustainabilityLevel TargetCategoryLevel { get; set; }

        public IEnumerable<CategoryItemViewModel> Categories { get; set; }
        public CategoryViewModel TargetCategoryModel { get; set; }
    }
}