﻿using System.Collections.Generic;
using STS.Application.Models.Sustainability.AchievementItems;
using STS.CrossCutting.Enums;

namespace STS.Application.Models.Sustainability
{
    public class AchievementViewModel
    {
        public TodoType AchievementType { get; set; }
        public IList<BaseAchievementItemViewModel> AchievementItems { get; set; }
    }
}