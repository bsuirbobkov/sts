﻿namespace STS.Application.Models.Sustainability
{
    public class CategoryItemViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public SustainabilityLevel Level { get; set; }
        public int OrderWeight { get; set; }
    }
}