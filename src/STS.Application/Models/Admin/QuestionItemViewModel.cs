﻿namespace STS.Application.Models.Admin
{
    public class QuestionItemViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }

        public int YesAnswerPoints { get; set; }
        public int NoAnswerPoints { get; set; }
    }
}