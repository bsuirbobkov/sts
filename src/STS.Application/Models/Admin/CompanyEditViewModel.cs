﻿using STS.Application.Models.User;

namespace STS.Application.Models.Admin
{
    public class CompanyEditViewModel
    {
        public CompanyViewModel Company { get; set; }
        public UserViewModel Administrator { get; set; }
        public bool EditMode { get; set; }
    }
}
