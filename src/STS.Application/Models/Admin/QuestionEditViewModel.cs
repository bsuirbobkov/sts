﻿using System.ComponentModel.DataAnnotations;

namespace STS.Application.Models.Admin
{
    public class QuestionEditViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Question Text")]
        [MinLength(4, ErrorMessage = "The {0} should be be at least {1} characters long")]
        [MaxLength(120, ErrorMessage = "The {0} should be no more than {1} characters long")]
        public string Text { get; set; }

        [Required]
        [Display(Name = "Yes Answer Points")]
        [Range(0, double.MaxValue, ErrorMessage = "The {0} must be greater than 0")]
        public int YesAnswerPoints { get; set; }

        [Required]
        [Display(Name = "No Answer Points")]
        [Range(0, double.MaxValue, ErrorMessage = "The {0} must be greater than 0")]
        public int NoAnswerPoints { get; set; }

        public bool EditMode { get; set; }
    }
}