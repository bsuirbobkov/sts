﻿using System;
using STS.CrossCutting.Attributes;

namespace STS.Application.Models.Admin
{
    public class CompanyItemViewModel : BaseEnumModel
    {
        public string Address { get; set; }

        public string Site { get; set; }

        public decimal AnnualProcurementBudget { get; set; }

        [ConsistOf("SustainabilityCategory.OrderWeight")]
        public string SustainabilityCategory { get; set; }

        public bool Disabled { get; set; }
    }
}
