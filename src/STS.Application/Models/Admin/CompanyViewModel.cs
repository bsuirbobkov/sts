﻿using System.ComponentModel.DataAnnotations;

namespace STS.Application.Models.Admin
{
    public sealed class CompanyViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        [MinLength(4, ErrorMessage = "The {0} should be be at least {1} characters long")]
        [MaxLength(30, ErrorMessage = "The {0} should be no more than {1} characters long")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Company Address")]
        [MinLength(8, ErrorMessage = "The {0} should be be at least {1} characters long")]
        [MaxLength(30, ErrorMessage = "The {0} should be no more than {1} characters long")]
        public string Address { get; set; }

        [Display(Name = "Address 2")]
        [MaxLength(30, ErrorMessage = "The {0} should be no more than {1} characters long")]
        public string AddressSecond { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "The {0} should be be at least {1} characters long")]
        [MaxLength(30, ErrorMessage = "The {0} should be no more than {1} characters long")]
        public string Country { get; set; }

        [Required]
        [MinLength(4, ErrorMessage = "The {0} should be be at least {1} characters long")]
        [MaxLength(30, ErrorMessage = "The {0} should be no more than {1} characters long")]
        public string City { get; set; }

        [Display(Name = "Zip Code")]
        [RegularExpression(@"^(((\d{9})|\d{5}))?$", ErrorMessage = "Please enter a five-digit ZIP code or a nine-digit ZIP+4 code.")]
        public string Zip { get; set; }

        [Display(Name = "Company Website")]
        [RegularExpression(@"^(http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?$", ErrorMessage = "{0} is not valid url")]
        [Required]
        public string Site { get; set; }

        [Required(ErrorMessage = "Annual Budget is required")]
        [Display(Name = "Annual Procurement Budget")]
        [Range(0.01, double.MaxValue, ErrorMessage = "The {0} must be greater than 0")]
        public decimal AnnualProcurementBudget { get; set; }
    }
}
