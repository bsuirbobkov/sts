﻿namespace STS.Application.Models.Document
{
    public class DocumentUploadModel
    {
        public DocumentModel DocumentModel { get; set; }
        public string Description { get; set; }
        public int DocumentTypeId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}