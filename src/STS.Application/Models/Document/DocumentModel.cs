﻿namespace STS.Application.Models.Document
{
    public class DocumentModel
    {
        public string FileName { get; set; }
        public string FileNameGuid { get; set; }
        public string FileBase64Data { get; set; }
        public string ExpectedExtension { get; set; }
    }
}