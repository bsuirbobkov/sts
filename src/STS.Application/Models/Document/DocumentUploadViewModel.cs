﻿using System.Collections.Generic;

namespace STS.Application.Models.Document
{
    public class DocumentUploadViewModel
    {
        public IEnumerable<BaseEnumModel> DocumentTypes { get; set; }
        public DocumentUploadModel DocumentUploadModel { get; set; }
    }
}