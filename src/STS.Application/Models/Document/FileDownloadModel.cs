﻿namespace STS.Application.Models.Document
{
    public class FileDownloadModel
    {
        public byte[] Data { get; set; }
        public string MimeType { get; set; }
        public string FileName { get; set; }
    }
}