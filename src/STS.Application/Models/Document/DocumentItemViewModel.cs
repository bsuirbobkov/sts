﻿using System;
using STS.CrossCutting.Attributes;

namespace STS.Application.Models.Document
{
    public class DocumentItemViewModel
    {
        public int Id { get; set; }

        public string Description { get; set; }

        [ConsistOf("OriginalFileName")]
        public string FileName { get; set; }

        [ConsistOf("DocumentType.Name")]
        public string DocumentType { get; set; }

        [ConsistOf("UploadDate")]
        public DateTime UploadDate { get; set; }

        [ConsistOf("User.FirstName", SecondaryColumns = "User.LastName")]
        public string UploadedBy { get; set; }
    }
}