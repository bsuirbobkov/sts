﻿using System;

namespace STS.Application.Models.Company
{
    public class MetricItemViewModel
    {
        public int Id { get; set; }
        public DateTime MetricDate { get; set; }
    }
}