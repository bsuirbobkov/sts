﻿using STS.CrossCutting.Enums;

namespace STS.Application.Models.Company
{
    public class QuestionViewModel
    {
        public int QuestionId { get; set; }
        public string Text { get; set; }
        public AnswerType Answer { get; set; }
    }
}