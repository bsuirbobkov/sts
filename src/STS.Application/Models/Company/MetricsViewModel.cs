﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace STS.Application.Models.Company
{
    public class MetricsViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = @"Last Day of the Year")]
        public string LastDayOfTheYear { get; set; }

        [Required]
        public long? Revenue { get; set; }

        [Display(Name="YES")]
        public bool Merged { get; set; }
        public string Details { get; set; }

        [Display(Name = "Scope 1 (Metric tons of CO2 equivalents)")]
        public long? Scope1 { get; set; }

        [Display(Name = "Scope 2 (Metric tons of CO2 equivalents)")]
        public long? Scope2 { get; set; }

        [Display(Name = "Scope 3 (Metric tons of CO2 equivalents)")]
        public long? Scope3 { get; set; }

        [Display(Name = "Electrical (kwh)")]
        public long? Electrical { get; set; }

        [Display(Name = "Natural Gas (therms)")]
        public long? NaturalGas { get; set; }

        [Display(Name = "Gasoline (gallons)")]
        public long? Gasoline { get; set; }

        [Display(Name = "Diesel (gallons)")]
        public long? Diesel { get; set; }

        [Display(Name = "Aviation Fuel (gallons)")]
        public long? AviationFuel { get; set; }

        [Display(Name = "Propane (kwh)")]
        public long? Propane { get; set; }

        [Display(Name = "Potable Gallons Used")]
        public long? PotableGallons { get; set; }

        [Display(Name = "Non-Potable Gallons Used")]
        public long? NonPotableGallons { get; set; }

        [Display(Name = "Amount Sent to Landfills (tons)")]
        public long? AmountSentToLandfills { get; set; }

        [Display(Name = "Amount Recycled (tons)")]
        public long? AmountRecycled { get; set; }

        [Display(Name = "Amount Composted (tons)")]
        public long? AmountComposted { get; set; }

        public bool BaseYear { get; set; }

        public IEnumerable<MetricItemViewModel> Metrics { get; set; }
    }
}
