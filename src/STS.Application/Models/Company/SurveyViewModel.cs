﻿using System.Collections.Generic;

namespace STS.Application.Models.Company
{
    public class SurveyViewModel
    {
        public IList<QuestionViewModel> Questions { get; set; }
    }
}