﻿using System.Collections.Generic;
using STS.Application.Models.Todo;

namespace STS.Application.Models.Category
{
    public class CategoryModel : BaseEnumModel
    {
        public int OrderWeight { get; set; }
        public IEnumerable<BaseTodoModel> Todos { get; set; }
    }
}