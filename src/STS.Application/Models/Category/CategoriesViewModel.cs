﻿using System.Collections.Generic;

namespace STS.Application.Models.Category
{
    public class CategoriesViewModel
    {
        public IEnumerable<CategoryModel> Categories { get; set; }
        public IEnumerable<BaseEnumModel> DocumentTypes { get; set; }
        public IEnumerable<BaseEnumModel> MetricTrends { get; set; }
        public IEnumerable<BaseEnumModel> DocumentConsiderations { get; set; }
        public IEnumerable<BaseEnumModel> MetricConsiderations { get; set; }
    }
}