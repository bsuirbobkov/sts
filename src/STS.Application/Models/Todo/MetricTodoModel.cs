﻿using System.Collections.Generic;
using STS.Application.Models.Todo.Items;
using STS.CrossCutting.Attributes;

namespace STS.Application.Models.Todo
{
    [TodoType(nameof(CrossCutting.Enums.TodoType.MetricTodo), typeof(MetricTodoModel))]
    public class MetricTodoModel : BaseTodoModel
    {
        public IEnumerable<MetricTodoItemModel> Metrics { get; set; }
        public override string TodoType => nameof(CrossCutting.Enums.TodoType.MetricTodo);
    }
}