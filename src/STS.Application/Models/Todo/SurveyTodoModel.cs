﻿using System.Collections.Generic;
using STS.Application.Models.Todo.Items;
using STS.CrossCutting.Attributes;

namespace STS.Application.Models.Todo
{
    [TodoType(nameof(CrossCutting.Enums.TodoType.SurveyTodo), typeof(SurveyTodoModel))]
    public class SurveyTodoModel : BaseTodoModel
    {
        public IEnumerable<SurveyTodoItemModel> Surveys { get; set; }
        public override string TodoType => nameof(CrossCutting.Enums.TodoType.SurveyTodo);
    }
}