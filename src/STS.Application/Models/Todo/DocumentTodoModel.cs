﻿using System.Collections.Generic;
using STS.Application.Models.Todo.Items;
using STS.CrossCutting.Attributes;

namespace STS.Application.Models.Todo
{
    [TodoType(nameof(CrossCutting.Enums.TodoType.DocumentTodo), typeof(DocumentTodoModel))]
    public class DocumentTodoModel : BaseTodoModel
    {
        public IEnumerable<DocumentTodoItemModel> Documents { get; set; }
        public override string TodoType => nameof(CrossCutting.Enums.TodoType.DocumentTodo);
    }
}