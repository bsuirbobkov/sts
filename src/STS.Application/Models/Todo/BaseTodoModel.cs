﻿namespace STS.Application.Models.Todo
{
    public abstract class BaseTodoModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Enabled { get; set; }
        public abstract string TodoType { get; }
    }
}