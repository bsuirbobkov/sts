﻿namespace STS.Application.Models.Todo.Items
{
    public class MetricTodoItemModel : BaseTodoItemModel
    {
        public int ConsiderationTime { get; set; }
        public bool Increase { get; set; }
        public int MetricTrendId { get; set; }
        public decimal RangeInPercents { get; set; }
    }
}