﻿namespace STS.Application.Models.Todo.Items
{
    public abstract class BaseTodoItemModel
    {
        public int Id { get; set; }
        public int ConsiderationTimeInMonths { get; set; }
    }
}