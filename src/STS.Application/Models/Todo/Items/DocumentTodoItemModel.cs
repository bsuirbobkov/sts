﻿namespace STS.Application.Models.Todo.Items
{
    public class DocumentTodoItemModel : BaseTodoItemModel
    {
        public int ConsiderationTime { get; set; }
        public int? DocumentTypeId { get; set; }
        public string Hyperlink { get; set; }
        public int Quantity { get; set; }
    }
}