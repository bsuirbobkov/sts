﻿namespace STS.Application.Models.Todo.Items
{
    public class SurveyTodoItemModel : BaseTodoItemModel
    {
        public decimal TotalScores { get; set; }
    }
}