﻿using System.ComponentModel.DataAnnotations;
using STS.CrossCutting.Enums;

namespace STS.Application.Models.User
{
    public class UserEditViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Username")]
        [MinLength(4, ErrorMessage = "The {0} should be be at least {1} characters long")]
        [MaxLength(20, ErrorMessage = "The {0} should be no more than {1} characters long")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Email Address")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "The confirm password cannot be empty.")]
        [EmailAddress]
        [Compare("Email", ErrorMessage = "The Email and Confirm Email do not match.")]
        [Display(Name = @"Confirm Email Address")]
        public string ConfirmEmail { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [MaxLength(20, ErrorMessage = "The {0} should be no more than {1} characters long")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [MaxLength(20, ErrorMessage = "The {0} should be no more than {1} characters long")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid {0}")]
        public string PhoneNumber { get; set; }

        public UserStatus UserStatus { get; set; }

        public bool EditMode { get; set; }
    }
}