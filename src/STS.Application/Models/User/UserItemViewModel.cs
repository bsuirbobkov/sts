﻿using STS.CrossCutting.Attributes;

namespace STS.Application.Models.User
{
    public class UserItemViewModel
    {
        public int Id { get; set; }

        [ConsistOf("FirstName", SecondaryColumns = "LastName")]
        public string Name { get; set; }
        public string Email { get; set; }

        [ConsistOf("UserStatus")]
        public string Status { get; set; }
    }
}