﻿using System.ComponentModel.DataAnnotations;

namespace STS.Application.Models.Account
{
    public class ResetPasswordViewModel
    {
        [Required]
        [MinLength(6, ErrorMessage = "The {0} should be be at least {1} characters long")]
        [MaxLength(30, ErrorMessage = "The {0} should be no more than {1} characters long")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        public string Code { get; set; }
    }
}
