﻿using System.ComponentModel.DataAnnotations;

namespace STS.Application.Models.Account
{
    public class SignInViewModel
    {
        [Required(ErrorMessage = @"Username cannot be empty.")]
        [Display(Name = @"Username")]
        public string UserName { get; set; }

        [Required(ErrorMessage = @"The password cannot be empty.")]
        [DataType(DataType.Password)]
        [Display(Name = @"Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me")]
        public bool RememberMe { get; set; }
    }
}
