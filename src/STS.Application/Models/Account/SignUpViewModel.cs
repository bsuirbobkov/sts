﻿using System.ComponentModel.DataAnnotations;

namespace STS.Application.Models.Account
{
    public class SignUpViewModel
    {
        public string SecurityKey { get; set; }

        [StringLength(100)]
        public string UserName { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [Required(ErrorMessage = @"The password cannot be empty.")]
        [MinLength(6, ErrorMessage = "The {0} should be be at least {1} characters long")]
        [MaxLength(20, ErrorMessage = "The {0} should be no more than {1} characters long")]
        [DataType(DataType.Password)]
        [Display(Name = @"Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "The confirm password cannot be empty.")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [Display(Name = @"Confirm password")]
        public string ConfirmPassword { get; set; }
    }
}
