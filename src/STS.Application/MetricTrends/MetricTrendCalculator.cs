﻿using STS.CrossCutting.Attributes;
using STS.CrossCutting.Enums;

namespace STS.Application.MetricTrends
{
    public class MetricTrendCalculator
    {
        [MetricTrendType(MetricTrend.ChangeInAbsoluteScopes)]
        public decimal? ChangeInAbsoluteScopes(MetricTrendInputModel model)
        {
            if (model?.MetricBaseYear == null || model.MetricRecentYear == null)
            {
                return null;
            }

            var baseYear = model.MetricBaseYear;
            var recentYear = model.MetricRecentYear;
            var baseYearScopeSum = baseYear.Scope1 + baseYear.Scope2;
            if (baseYearScopeSum == 0)
            {
                return null;
            }

            var result = ((recentYear.Scope1 + recentYear.Scope2) / baseYearScopeSum - 1) * 100;
            return result;
        }

        [MetricTrendType(MetricTrend.ChangeEmissionsInScopes)]
        public decimal? ChangeEmissionsInScopes(MetricTrendInputModel model)
        {
            if (model?.MetricBaseYear == null || model?.MetricRecentYear == null)
            {
                return null;
            }

            var baseYear = model.MetricBaseYear;
            var recentYear = model.MetricRecentYear;
            var baseYearScopes = baseYear.Scope1 + baseYear.Scope2;
            if (recentYear.Revenue == 0 || baseYear.Revenue == 0 || baseYearScopes == 0)
            {
                return null;
            }

            var result = ((recentYear.Scope1 + recentYear.Scope2) / recentYear.Revenue / (baseYearScopes / baseYear.Revenue) - 1) * 100;
            return result;
        }

        [MetricTrendType(MetricTrend.TotalWaterUse)]
        public decimal? TotalWaterUse(MetricTrendInputModel model)
        {
            if (model?.MetricBaseYear == null || model?.MetricRecentYear == null)
            {
                return null;
            }

            var baseYear = model.MetricBaseYear;
            var recentYear = model.MetricRecentYear;
            var baseYearGallons = baseYear.PotableGallons + baseYear.NonPotableGallons;

            if (baseYearGallons == 0)
            {
                return null;
            }

            var result = ((recentYear.PotableGallons + recentYear.NonPotableGallons) / baseYearGallons - 1) * 100;
            return result;
        }

        [MetricTrendType(MetricTrend.TotalWaterUseIntensity)]
        public decimal? TotalWaterUseIntensity(MetricTrendInputModel model)
        {
            if (model?.MetricBaseYear == null || model?.MetricRecentYear == null)
            {
                return null;
            }

            var baseYear = model.MetricBaseYear;
            var recentYear = model.MetricRecentYear;
            var baseYearGallons = baseYear.PotableGallons + baseYear.NonPotableGallons;
            if (recentYear.Revenue == 0 || baseYear.Revenue == 0 || baseYearGallons == 0)
            {
                return null;
            }

            var result = (((recentYear.PotableGallons + recentYear.NonPotableGallons) / recentYear.Revenue) / (baseYearGallons / baseYear.Revenue) - 1) * 100;
            return result;
        }

        [MetricTrendType(MetricTrend.TotalLandfillWaste)]
        public decimal? TotalLandfillWaste(MetricTrendInputModel model)
        {
            if (model?.MetricBaseYear == null || model?.MetricRecentYear == null)
            {
                return null;
            }

            var baseYear = model.MetricBaseYear;
            var recentYear = model.MetricRecentYear;
            if (baseYear.AmountSentToLandfills == 0)
            {
                return null;
            }

            var result = (recentYear.AmountSentToLandfills / baseYear.AmountSentToLandfills - 1) * 100;
            return result;
        }

        [MetricTrendType(MetricTrend.LandfillWasteIntensity)]
        public decimal? LandfillWasteIntensity(MetricTrendInputModel model)
        {
            if (model?.MetricBaseYear == null || model?.MetricRecentYear == null)
            {
                return null;
            }

            var baseYear = model.MetricBaseYear;
            var recentYear = model.MetricRecentYear;
            var baseAmountSentToLandfills = baseYear.AmountSentToLandfills;
            if (recentYear.Revenue == 0 || baseYear.Revenue == 0 || baseAmountSentToLandfills == 0)
            {
                return null;
            }

            var result = ((recentYear.AmountSentToLandfills / recentYear.Revenue) / (baseAmountSentToLandfills / baseYear.Revenue) - 1) * 100;
            return result;
        }
    }
}