﻿using STS.Domain.Entities;

namespace STS.Application.MetricTrends
{
    public class MetricTrendInputModel
    {
        public MetricEntity MetricRecentYear { get; set; }
        public MetricEntity MetricBaseYear { get; set; }
    }
}