﻿using System;

namespace STS.CrossCutting.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TodoTypeAttribute : Attribute
    {
        public string TodoName { get; }
        public Type TodoType { get; }

        public TodoTypeAttribute(string todoName, Type todoType)
        {
            TodoName = todoName;
            TodoType = todoType;
        }
    }
}