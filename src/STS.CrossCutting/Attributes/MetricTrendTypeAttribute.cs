﻿using System;
using STS.CrossCutting.Enums;

namespace STS.CrossCutting.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class MetricTrendTypeAttribute : Attribute
    {
        public MetricTrend MetricTrend { get; }

        public MetricTrendTypeAttribute(MetricTrend metricTrend)
        {
            MetricTrend = metricTrend;
        }
    }
}