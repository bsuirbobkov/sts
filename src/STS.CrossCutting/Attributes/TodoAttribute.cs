﻿using System;
using STS.CrossCutting.Enums;

namespace STS.CrossCutting.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class TodoAttribute : Attribute
    {
        public TodoType TodoType { get; }

        public TodoAttribute(TodoType todoType)
        {
            TodoType = todoType;
        }
    }
}