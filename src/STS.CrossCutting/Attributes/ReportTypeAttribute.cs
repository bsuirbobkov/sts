﻿using System;
using STS.CrossCutting.Enums;

namespace STS.CrossCutting.Attributes
{
    [AttributeUsage(AttributeTargets.All)]
    public class ReportTypeAttribute : Attribute
    {
        public ReportTypes ReportType { get; }

        public ReportTypeAttribute(ReportTypes reportType)
        {
            ReportType = reportType;
        }
    }
}