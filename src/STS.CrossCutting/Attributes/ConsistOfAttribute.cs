﻿using System;

namespace STS.CrossCutting.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ConsistOfAttribute : Attribute
    {
        public string SecondaryColumns;
        private readonly string primaryColumn;

        public ConsistOfAttribute(string primaryColumn)
        {
            this.primaryColumn = primaryColumn;

        }

        public string Fields => string.IsNullOrEmpty(SecondaryColumns)
            ? primaryColumn
            : $"{primaryColumn},{SecondaryColumns}";
    }
}