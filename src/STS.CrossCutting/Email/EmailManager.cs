﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using Hangfire;
using Microsoft.Extensions.Options;
using STS.CrossCutting.Constans;
using STS.CrossCutting.Models.Configuration;

namespace STS.CrossCutting.Email
{
    public class EmailManager
    {
        private readonly Constants constants;
        private readonly EmailSettings emailSettings;

        public EmailManager() { }

        public EmailManager(
            IOptions<EmailSettings> emailSettingsOptions,
            Constants constants
            )
        {
            this.constants = constants;
            emailSettings = emailSettingsOptions.Value;
        }

        public virtual void SendMessage(string to, string toName, string subject, string body)
        {
            BackgroundJob.Enqueue<EmailManager>(x =>
                x.SendMessageFull(emailSettings.MailFromEmail, emailSettings.MailFromDisplayName, to, toName, subject, body));
        }

        public virtual void SendMessageFull(string from, string fromName, string to, string toName, string subject, string body)
        {
            var mailMessage = new MailMessage(new MailAddress(from, fromName), new MailAddress(to, toName))
            {
                IsBodyHtml = true,
                Body = body,
                Subject = subject
            };

            var plainView = AlternateView.CreateAlternateViewFromString("Please view this email in an application that supports HTML.", null, MediaTypeNames.Text.Plain);
            var htmlView = AlternateView.CreateAlternateViewFromString(mailMessage.Body, null, MediaTypeNames.Text.Html);

            foreach (var linkedResource in ImageResources)
            {
                htmlView.LinkedResources.Add(linkedResource);
            }

            mailMessage.AlternateViews.Add(plainView);
            mailMessage.AlternateViews.Add(htmlView);
            mailMessage.Body = string.Empty;

            mailMessage.Headers.Add("Message-Id", string.Format(CultureInfo.InvariantCulture, "<{0}@{1}>", Guid.NewGuid(), emailSettings.MailHost));

            var client = new SmtpClient
            {
                Port = emailSettings.SmtpPort,
                Host = emailSettings.SmtpHost,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailSettings.SmtpUserName, emailSettings.SmtpPassword),
                EnableSsl = true
            };

            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;

            client.Send(mailMessage);
        }

        private IEnumerable<LinkedResource> ImageResources => new List<LinkedResource>
        {
            new LinkedResource(constants.Paths.EmailHeader, MediaTypeNames.Image.Jpeg) { ContentId = "emailHeader" },
            new LinkedResource(constants.Paths.EmailIcon, MediaTypeNames.Image.Jpeg) { ContentId = "emailIcon" },
            new LinkedResource(constants.Paths.FacebookIcon, MediaTypeNames.Image.Jpeg) { ContentId = "facebookIcon" },
            new LinkedResource(constants.Paths.LinkedInIcon, MediaTypeNames.Image.Jpeg) { ContentId = "linkedInIcon" },
            new LinkedResource(constants.Paths.TwitterIcon, MediaTypeNames.Image.Jpeg) { ContentId = "twitterIcon" },
        };
    }
}