﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace STS.CrossCutting.Utils
{
    public static class EnumUtils
    {
        public static string DisplayName(this Enum value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            var enumType = value.GetType();
            var enumValue = Enum.GetName(enumType, value);
            var memberInfo = enumType.GetMember(enumValue)[0];

            var displayAttribute = memberInfo.GetCustomAttributes(typeof(DisplayAttribute), false)
                .FirstOrDefault() as DisplayAttribute;

            return displayAttribute?.Name ?? value.ToString();
        }

        public static IDictionary<int, string> DisplayAllValues<TEnum>()
        {
            var values = Enum.GetValues(typeof(TEnum));
            var dictionary = new Dictionary<int, string>();

            foreach (var entry in values)
            {
                var key = (int)entry;
                var value = ((Enum)entry).DisplayName();
                dictionary.Add(key, value);
            }

            return dictionary;
        }
    }
}