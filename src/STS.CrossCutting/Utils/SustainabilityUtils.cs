﻿using STS.CrossCutting.Constans;

namespace STS.CrossCutting.Utils
{
    public static class SustainabilityUtils
    {
        public static string NoResponseIsNull(string value)
        {
            return value ?? WebConstant.NoResponse;
        }
    }
}