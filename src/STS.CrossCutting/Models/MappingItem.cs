﻿namespace STS.CrossCutting.Models
{
    public class MappingItem
    {
        public string Title { get; set; }
        public string Key { get; set; }
    }
}