﻿namespace STS.CrossCutting.Models.Configuration
{
    public class EmailSettings
    {
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpUserName { get; set; }
        public string SmtpPassword { get; set; }

        public string MailFromEmail { get; set; }
        public string MailFromDisplayName { get; set; }
        public string MailHost { get; set; }
    }
}