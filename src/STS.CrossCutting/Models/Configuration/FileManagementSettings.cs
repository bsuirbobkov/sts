﻿namespace STS.CrossCutting.Models.Configuration
{
    public class FileManagementSettings
    {
        public string Type { get; set; }
        public string Path { get; set; }
        public string TemporaryFolderName { get; set; }
        public string FileFolderName { get; set; }
    }
}
