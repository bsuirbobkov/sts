﻿namespace STS.CrossCutting.Models.Configuration
{
    public class AuthenticationSettings
    {
        public int ExpirationTime { get; set; }
        public int ExpirationTimeForNewSupplierCompany { get; set; }
        public int MaxFailedAccessAttempts { get; set; }
    }
}
