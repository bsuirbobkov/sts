﻿namespace STS.CrossCutting.Models.Configuration
{
    public class RelativePathSettings
    {
        public string EmailHeader { get; set; } 
        public string FacebookIcon { get; set; } 
        public string EmailIcon { get; set; } 
        public string LinkedInIcon { get; set; } 
        public string TwitterIcon { get; set; } 

        public string SetPassword { get; set; }
        public string RestorePassword { get; set; }
        public string SupplierSustainability { get; set; }
        public string CommunicateSustainability { get; set; }
    }
}