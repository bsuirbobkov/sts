﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace STS.CrossCutting.Models.Cryptography
{
    public static class CryptUtility
    {
        private static readonly byte[] Key = GenerateKey();

        public static string Encrypt(string input)
        {
            var tdesAlgorithm = new TripleDESCryptoServiceProvider
                {
                    Key = Key, 
                    Mode = CipherMode.ECB, 
                    Padding = PaddingMode.PKCS7
                };

            byte[] results;
            var dataToEncrypt = Encoding.UTF8.GetBytes(input);

            try
            {
                var encryptor = tdesAlgorithm.CreateEncryptor();
                results = encryptor.TransformFinalBlock(dataToEncrypt, 0, dataToEncrypt.Length);
            }
            finally
            {
                tdesAlgorithm.Clear();
            }

            return Convert.ToBase64String(results);
        }

        public static string Decrypt(string input)
        {
            var tdesAlgorithm = new TripleDESCryptoServiceProvider
                {
                    Key = Key, 
                    Mode = CipherMode.ECB, 
                    Padding = PaddingMode.PKCS7
                };

            var dataToDecrypt = Convert.FromBase64String(input);

            byte[] results;

            try
            {
                var decryptor = tdesAlgorithm.CreateDecryptor();
                results = decryptor.TransformFinalBlock(dataToDecrypt, 0, dataToDecrypt.Length);
            }
            finally
            {
                tdesAlgorithm.Clear(); 
            }

            return Encoding.UTF8.GetString(results);
        }

        private static byte[] GenerateKey()
        {
            var utf8 = new UTF8Encoding();
            var hashProvider = new MD5CryptoServiceProvider();
            var tdesKey = hashProvider.ComputeHash(utf8.GetBytes(Guid.NewGuid().ToString()));
            hashProvider.Clear();

            return tdesKey;
        }
    }
}
