﻿using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace STS.CrossCutting.Models.Cryptography
{
    public static class StringExtensions
    {
        public static string CreateHash(this string value)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(value);
            var csp = new MD5CryptoServiceProvider();
            byte[] byteHash = csp.ComputeHash(bytes);
            return byteHash.Aggregate(string.Empty, (current, b) => current + string.Format(CultureInfo.InvariantCulture, "{0:x2}", b));
        }
    }
}
