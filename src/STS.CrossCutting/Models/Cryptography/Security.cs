﻿using System;
using System.Security.Cryptography;

namespace STS.CrossCutting.Models.Cryptography
{
    public static class Security
    {
        public static string CreateHashedPassword(string value, string salt)
        {
            string hashedPassword = value.CreateHash();
            string saltedPassword = string.Concat(hashedPassword, salt);
            return saltedPassword.CreateHash();
        }

        public static string CreateSalt()
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[64];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }
    }
}
