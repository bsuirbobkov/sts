﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace STS.CrossCutting.Models.Cryptography
{
    public class AesCrypter : ICrypter
    {
        private readonly AesCryptoServiceProvider cryptoProvider;

        public AesCrypter()
        {
            const string Key = "StsCryptoKey";
            const string Salt = "dnksualn";
            var byteConverter = new UnicodeEncoding();

            cryptoProvider = new AesCryptoServiceProvider
            {
                IV = byteConverter.GetBytes(Salt),
                Key = byteConverter.GetBytes(Key)
            };
        }

        public string Encrypt(string source)
        {
            var encryptor = cryptoProvider.CreateEncryptor();
            var encrypted = CryptSource(source, encryptor);
            var encryptedString = Convert.ToBase64String(encrypted);
            return encryptedString;
        }

        public string Decrypt(string source)
        {
            var decrypted = string.Empty;
            try
            {
                var decryptor = cryptoProvider.CreateDecryptor();
                var preparedSource = Convert.FromBase64String(source);
                decrypted = DecryptSource(preparedSource, decryptor);
            }
            catch
            {
                // ignored
            }
            return decrypted;
            
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Because it is necessary!")]
        private byte[] CryptSource(string source, ICryptoTransform transform)
        {
            byte[] encrypted;
            using (var memoryStreamEncrypt = new MemoryStream())
            {
                using (var cryptoStreamEncrypt = new CryptoStream(memoryStreamEncrypt, transform, CryptoStreamMode.Write))
                {
                    using (var streamWriterEncrypt = new StreamWriter(cryptoStreamEncrypt))
                    {
                        streamWriterEncrypt.Write(source);
                    }

                    encrypted = memoryStreamEncrypt.ToArray();
                }
            }

            return encrypted;
        }

        [SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Because it is necessary!")]
        private string DecryptSource(byte[] source, ICryptoTransform transform)
        {
            string encryptedString;
            var encryptedSource = source;
            using (var memoryStreamEncrypt = new MemoryStream(encryptedSource))
            {
                using (var cryptoStreamEncrypt = new CryptoStream(memoryStreamEncrypt, transform, CryptoStreamMode.Read))
                {
                    using (var streamReaderEncrypt = new StreamReader(cryptoStreamEncrypt))
                    {
                        encryptedString = streamReaderEncrypt.ReadToEnd();
                    }
                }
            }

            return encryptedString;
        }
    }
}
