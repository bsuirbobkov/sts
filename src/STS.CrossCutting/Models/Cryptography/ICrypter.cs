﻿namespace STS.CrossCutting.Models.Cryptography
{
    public interface ICrypter
    {
        string Encrypt(string source);
        string Decrypt(string source);
    }
}
