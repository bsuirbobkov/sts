﻿namespace STS.CrossCutting.Constans
{
    public static class WebConstant
    {
        public static readonly string NoResponse = "No Response";

        public static readonly string SignUpTemplate = "SignUpTemplate";
        public static readonly string PasswordRestoreTemplate = "PasswordRestoreTemplate";
        public static readonly string ChangeLevelCompanyTemplate = "ChangeLevelCompanyTemplate";
        public static readonly string ChangeLevelAdminTemplate = "ChangeLevelAdminTemplate";

        public static readonly string EmailTemplate = "_EmailTemplateLayout";
    }
}