﻿using Microsoft.Extensions.Options;
using STS.CrossCutting.Models.Configuration;

namespace STS.CrossCutting.Constans
{
    public class AuthConstant
    {
        private readonly AuthenticationSettings authenticationSettings;

        public AuthConstant(IOptions<AuthenticationSettings> optionsRelativePathSettings)
        {
            authenticationSettings = optionsRelativePathSettings.Value;
        }

        public int ExpirationTime => authenticationSettings.ExpirationTime;

        public int ExpirationTimeForNewSupplierCompany => authenticationSettings.ExpirationTimeForNewSupplierCompany;

        public int LogOnAttempts => authenticationSettings.MaxFailedAccessAttempts;
    }
}
