﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using STS.CrossCutting.Models.Configuration;

namespace STS.CrossCutting.Constans
{
    public class PathConstant
    {
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly RelativePathSettings relativePathSettings;
        private readonly FileManagementSettings fileManagementSettings;

        public PathConstant(IHostingEnvironment hostingEnvironment,
            IOptions<RelativePathSettings> optionsRelativePathOptions,
            IOptions<FileManagementSettings> fileManagementOptions)
        {
            this.hostingEnvironment = hostingEnvironment;
            relativePathSettings = optionsRelativePathOptions.Value;
            fileManagementSettings = fileManagementOptions.Value;
        }

        public virtual string EmailHeader => hostingEnvironment.WebRootPath + relativePathSettings.EmailHeader;
        public virtual string FacebookIcon => hostingEnvironment.WebRootPath + relativePathSettings.FacebookIcon;
        public virtual string EmailIcon => hostingEnvironment.WebRootPath + relativePathSettings.EmailIcon;
        public virtual string LinkedInIcon => hostingEnvironment.WebRootPath + relativePathSettings.LinkedInIcon;
        public virtual string TwitterIcon => hostingEnvironment.WebRootPath + relativePathSettings.TwitterIcon;

        public virtual string TemporaryStorePath =>
            hostingEnvironment.WebRootPath + fileManagementSettings.Path + fileManagementSettings.TemporaryFolderName;

        public virtual string FileStorePath =>
            hostingEnvironment.WebRootPath + fileManagementSettings.Path + fileManagementSettings.FileFolderName;
    }
}