﻿namespace STS.CrossCutting.Constans
{
    public class Constants
    {
        public Constants() { }

        public Constants(PathConstant pathConstant, LinkConstant linkConstant, AuthConstant authConstant)
        {
            // TODO: neetd to use interface to avoid this
            Paths = pathConstant;
            Links = linkConstant;
            Auth = authConstant;
        }

        public virtual PathConstant Paths { get; set; }
        public virtual LinkConstant Links { get; set; }
        public virtual AuthConstant Auth { get; set; }
    }
}