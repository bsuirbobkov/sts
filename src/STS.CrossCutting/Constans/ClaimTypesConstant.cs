﻿namespace STS.CrossCutting.Constans
{
    public static class ClaimTypesConstant
    {
        public static string UserName => "UserName";
        public static string CompanyId => "CompanyId";
        public static string CompanyName => "CompanyName";
    }
}
