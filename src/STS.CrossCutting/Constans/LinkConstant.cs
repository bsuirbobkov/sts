﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using STS.CrossCutting.Models.Configuration;

namespace STS.CrossCutting.Constans
{
    public class LinkConstant
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly RelativePathSettings relativePathSettings;

        public LinkConstant(
            IHttpContextAccessor httpContextAccessor,
            IOptions<RelativePathSettings> optionsRelativePathSettings
            )
        {
            this.httpContextAccessor = httpContextAccessor;
            relativePathSettings = optionsRelativePathSettings.Value;
        }

        public static string PathBase;

        public string SetPassword => PathBase + relativePathSettings.SetPassword;
        public string SupplierSustainability => PathBase + relativePathSettings.SupplierSustainability;
        public string CommunicateSustainability => PathBase + relativePathSettings.CommunicateSustainability;
        public string RestorePassword => PathBase + relativePathSettings.RestorePassword;

        public static void FillRequest(HttpRequest request)
        {
            PathBase = request.Scheme + "://" + request.Host.Value + request.PathBase;
        }
    }
}