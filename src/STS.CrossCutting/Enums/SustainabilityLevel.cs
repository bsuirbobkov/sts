﻿using System.ComponentModel.DataAnnotations;

namespace STS.CrossCutting.Enums
{
    public enum SustainabilityLevel
    {
        [Display(Name = "No Activity")]
        NoActivity = 1,
        Baselining = 2,
        Planing = 3,
        Reporting = 4,
        Implementing = 5,
        Achieving = 6
    }
}