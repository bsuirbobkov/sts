﻿namespace STS.CrossCutting.Enums
{
    public enum UserRole
    {
        None = 0,
        SuperAdmin = 1,
        CompanyUser = 2
    }
}