﻿using System.ComponentModel.DataAnnotations;

namespace STS.CrossCutting.Enums
{
    public enum MetricConsideration
    {
        [Display(Name = "From the date when the data point was entered")]
        FromEnterDate = 1,

        [Display(Name = "From the ending date of the year associated with the data point")]
        FromEndingDate = 2
    }
}