﻿using System.ComponentModel.DataAnnotations;

namespace STS.CrossCutting.Enums
{
    public enum ReportTypes
    {
        [Display(Name = "Company Distribution by Sustainability Level - Total")]
        CompanyDistributionBySustainabilityLevelTotal = 1
    }
}