﻿using System.ComponentModel.DataAnnotations;

namespace STS.CrossCutting.Enums
{
    public enum SurveyStatus
    {
        [Display(Name = "Completed")]
        Completed = 1,

        [Display(Name = "Completed With Percentage")]
        CompletedWithAnswersCount = 2
    }
}