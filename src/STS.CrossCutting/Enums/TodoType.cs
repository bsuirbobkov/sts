﻿namespace STS.CrossCutting.Enums
{
    public enum TodoType
    {
        DocumentTodo,
        SurveyTodo,
        MetricTodo
    }
}