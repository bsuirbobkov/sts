﻿using System.ComponentModel.DataAnnotations;

namespace STS.CrossCutting.Enums
{
    public enum MetricTrend
    {
        [Display(Name = "Change in absolute scope 1 and 2 GHG emissions")]
        ChangeInAbsoluteScopes = 1,

        [Display(Name = "Change in scope 1 and 2 GHG emissions intensity")]
        ChangeEmissionsInScopes = 2,

        [Display(Name = "Change in total water use")]
        TotalWaterUse = 3,

        [Display(Name = "Change in total water use intensity")]
        TotalWaterUseIntensity = 4,

        [Display(Name = "Change in total landfill waste")]
        TotalLandfillWaste = 5,

        [Display(Name = "Change in landfill waste intensity")]
        LandfillWasteIntensity = 6
    }
}