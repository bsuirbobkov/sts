﻿namespace STS.CrossCutting.Enums
{
    public enum AnswerType
    {
        None = 0,
        Yes = 1,
        No = 3
    }
}