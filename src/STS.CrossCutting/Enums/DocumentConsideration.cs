﻿using System.ComponentModel.DataAnnotations;

namespace STS.CrossCutting.Enums
{
    public enum DocumentConsideration
    {
        [Display(Name = "From the date the document was uploaded")]
        FromUploadingDate = 1,

        [Display(Name = "From the ending date of the year associated with the document")]
        FromEndingDate = 2
    }
}