﻿namespace STS.CrossCutting.Enums
{
    public enum UserStatus
    {
        None = 0,
        Active = 1,
        Deactivated = 2,
        Locked = 3
    }
}