﻿namespace STS.Web.Models
{
    public class ActionTagHelperModel
    {
        public string Controller { get; set; }
        public string Action { get; set; }
    }
}
