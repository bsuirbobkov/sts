﻿namespace STS.Web.Models
{
    public class SustainabilityPageSettingsViewModel
    {
        public string HeaderTitle { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
    }
}