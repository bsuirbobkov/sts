﻿namespace STS.Web.Models
{
    public class CategoryItemTagHelperViewModel
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string LinkClass { get; set; }
        public string LinkActiveClass { get; set; }
        public string Href { get; set; }
    }
}