﻿using Hangfire.Dashboard;

namespace STS.Web.ActionFilter
{
    public class AllowedAllAuthenticatedFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize(DashboardContext context)
        {
            return true;
        }
    }
}
