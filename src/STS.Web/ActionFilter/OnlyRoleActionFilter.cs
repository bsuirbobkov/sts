﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace STS.Web.ActionFilter
{
    public class OnlyRoleActionFilter : ActionFilterAttribute
    {
        public string Role;
        public string RedirectController;
        public string RedirectAction;

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.User.IsInRole(Role)) return;

            var controller = (Controller)context.Controller;
            context.Result = controller.RedirectToAction(RedirectAction, RedirectController);
        }
    }
}