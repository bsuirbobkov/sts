﻿using System;
using System.Linq;
using STS.Application.Filters;
using STS.Web.Grid.Builders.Interfaces;

namespace STS.Web.Extensions
{
    public static class BuilderExtensions
    {
        public static IGridBuilder<TModel> SetupByFilter<TModel>(this IGridBuilder<TModel> builder, IFilter filter)
        {
            builder.ColumnsBuilder.SetupByFilter(filter);
            builder.PagerBuilder.SetupByFilter(filter);

            return builder;
        }

        public static IColumnsBuilder<TModel> SetupByFilter<TModel>(this IColumnsBuilder<TModel> builder, IFilter filter)
        {
            if (string.IsNullOrEmpty(filter.SortColumns))
            {
                return builder;
            }

            Predicate<IColumnBuilder<TModel>> predicate = x => string.Equals(x.Name, filter.SortColumns,
                    StringComparison.InvariantCultureIgnoreCase);

            var index = builder.ColumnBuilders.ToList().FindIndex(predicate);
            builder.ColumnBuilders[index].SortAscending(filter.SortAscending);

            return builder;
        }

        public static IPagerBuilder SetupByFilter(this IPagerBuilder builder, IFilter filter)
        {
            builder.CurrentPage(filter.CurrentPage);
            builder.RowsPerPage(filter.RowsPerPage);
            return builder;
        }
    }
}