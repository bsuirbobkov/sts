﻿using System.Collections.Generic;

namespace STS.Web.Grid.Components
{
    public interface IGrid
    {
        string EmptyText { set; get; }
        IList<IColumn> Columns { get; set; }
        IEnumerable<object> Rows { get; set; }
        IPager Pager { get; set; }
    }
}