﻿using Microsoft.AspNetCore.Html;

namespace STS.Web.Grid.Components
{
    public interface IColumn
    {
        string Name { get; set; }
        string Title { get; set; }
        string With { get; set; }
        bool Sortable { get; set; }
        bool Encode { get; set; }
        bool SortAscending { get; set; }
        HtmlString GetCellText(object item);
    }
}