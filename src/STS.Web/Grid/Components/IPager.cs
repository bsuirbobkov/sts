using System.Collections.Generic;

namespace STS.Web.Grid.Components
{
    public interface IPager
    {
        int FirstDisplayPage { get; }
        int CurrentPage { get; set; }
        int TotalPages { get; set; }
        int PagesToDisplay { get; set; }
        int RowsPerPage { get; set; }
        IList<int> PageSizes { get; set; }
        string PartialViewName { get; set; }
    }
}