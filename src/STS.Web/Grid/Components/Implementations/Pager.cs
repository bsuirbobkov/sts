﻿using System;
using System.Collections.Generic;

namespace STS.Web.Grid.Components.Implementations
{
    public class Pager : IPager
    {
        public int FirstDisplayPage
        {
            get
            {
                var middlePage = (PagesToDisplay / 2) + (PagesToDisplay % 2);
                if (CurrentPage < middlePage)
                {
                    return 1;
                }

                if (CurrentPage - middlePage + PagesToDisplay > TotalPages)
                {
                    return Math.Max(TotalPages - PagesToDisplay + 1, 1);
                }

                return CurrentPage - middlePage + 1;
            }
        }

        public int CurrentPage { get; set; } = 1;
        public int TotalPages { get; set; }
        public int PagesToDisplay { get; set; } = 1;
        public int RowsPerPage { get; set; }
        public IList<int> PageSizes { get; set; }
        public string PartialViewName { get; set; }
    }
}