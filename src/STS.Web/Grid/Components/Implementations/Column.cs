﻿using System;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Html;

namespace STS.Web.Grid.Components.Implementations
{
    public class Column<TModel> : IColumn
    {
        private readonly Func<TModel, object> cellText;

        public Column(Func<TModel, object> cellText)
        {
            this.cellText = cellText;
        }

        public string Name { get; set; }
        public string Title { get; set; }
        public string With { get; set; }
        public bool Sortable { get; set; }
        public bool Encode { get; set; }
        public bool SortAscending { get; set; }

        public HtmlString GetCellText(object item)
        {
            if (item == null)
            {
                return HtmlString.Empty;
            }

            var model = (TModel)item;
            var text = cellText(model)?.ToString()
                ?? string.Empty;

            text = Encode
                ? HtmlEncoder.Default.Encode(text)
                : text;

            return new HtmlString(text);
        }
    }
}