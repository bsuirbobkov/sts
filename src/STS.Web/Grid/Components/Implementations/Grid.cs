using System.Collections.Generic;

namespace STS.Web.Grid.Components.Implementations
{
    public class Grid : IGrid
    {
        public string EmptyText { get; set; }
        public IList<IColumn> Columns { get; set; }
        public IEnumerable<object> Rows { get; set; }
        public IPager Pager { get; set; }
    }
}