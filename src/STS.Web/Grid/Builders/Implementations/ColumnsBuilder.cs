﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using STS.CrossCutting.Attributes;
using STS.Web.Grid.Builders.Interfaces;
using STS.Web.Grid.Components;

namespace STS.Web.Grid.Builders.Implementations
{
    public class ColumnsBuilder<TModel> : IColumnsBuilder<TModel>
    {
        public IList<IColumnBuilder<TModel>> ColumnBuilders { get; } = new List<IColumnBuilder<TModel>>();

        public IList<IColumn> Build()
        {
            var columns = ColumnBuilders.Select(b => b.Build()).ToList();
            return columns;
        }

        public IColumnBuilder<TModel> Define(string columnName)
        {
            return DefineColumn(columnName);
        }

        public IColumnBuilder<TModel> Define<TProperty>(Expression<Func<TModel, TProperty>> column)
        {
            var expression = column.Body as MemberExpression;
            if (expression == null)
            {
                throw new ArgumentException("Wrong column expression");
            }

            var metadataAttribute = GetMetadataAttribute(expression.Member);
            var columnName = IsMetdataNameDeclared(metadataAttribute)
                ? metadataAttribute?.Fields
                : expression.Member.Name;

            return DefineColumn(columnName);
        }

        private static ConsistOfAttribute GetMetadataAttribute(MemberInfo propertyInfo)
        {
            var attribute = propertyInfo.GetCustomAttributes(typeof(ConsistOfAttribute), false)
                .Select(x => x as ConsistOfAttribute)
                .FirstOrDefault();

            return attribute;
        }

        private static bool IsMetdataNameDeclared(ConsistOfAttribute attribute)
        {
            return !string.IsNullOrWhiteSpace(attribute?.Fields) &&
                   !string.IsNullOrWhiteSpace(attribute.Fields);
        }

        private ColumnBuilder<TModel> DefineColumn(string columnName)
        {
            var builder = new ColumnBuilder<TModel>(columnName);
            ColumnBuilders.Add(builder);
            return builder;
        }
    }
}