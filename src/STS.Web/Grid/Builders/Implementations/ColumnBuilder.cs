using System;
using STS.Web.Grid.Builders.Interfaces;
using STS.Web.Grid.Components;
using STS.Web.Grid.Components.Implementations;

namespace STS.Web.Grid.Builders.Implementations
{
    public class ColumnBuilder<TModel> : IColumnBuilder<TModel>
    {
        private bool sortable = true;
        private bool encode;
        private Func<TModel, object> renderValue;
        private string title;
        private bool sortOrder = true;
        private string width = string.Empty;

        public ColumnBuilder(string columnName)
        {
            Name = columnName;
        }

        public string Name { get; }

        public IColumn Build()
        {
            var column = new Column<TModel>(renderValue)
            {
                Name = Name,
                Title = title,
                Sortable = sortable,
                Encode = encode,
                SortAscending = sortOrder,
                With = width
            };

            return column;
        }

        public IColumnBuilder<TModel> Titled(string text)
        {
            title = text;
            return this;
        }

        public IColumnBuilder<TModel> HasWitdh(string width)
        {
            this.width = width;
            return this;
        }

        public IColumnBuilder<TModel> Sortable(bool sortable = true)
        {
            this.sortable = sortable;
            return this;
        }

        public IColumnBuilder<TModel> Encode(bool encode = false)
        {
            this.encode = encode;
            return this;
        }

        public IColumnBuilder<TModel> SortAscending(bool order = true)
        {
            sortOrder = order;
            return this;
        }

        public IColumnBuilder<TModel> RenderAs(Func<TModel, object> config)
        {
            renderValue = config;
            return this;
        }
    }
}