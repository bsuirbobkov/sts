﻿using System;
using System.Collections.Generic;
using System.Linq;
using STS.Web.Grid.Builders.Interfaces;
using STS.Web.Grid.Components;

namespace STS.Web.Grid.Builders.Implementations
{
    public class GridBuilder<TModel> : IGridBuilder<TModel>
    {
        private readonly IGrid grid = new Components.Implementations.Grid();

        public IColumnsBuilder<TModel> ColumnsBuilder { get; } = new ColumnsBuilder<TModel>();
        public IPagerBuilder PagerBuilder { get; } = new PagerBuilder();

        public IGrid Build()
        {
            grid.Columns = ColumnsBuilder.Build();
            grid.Pager = PagerBuilder.Build();

            ThrowIfNoColumns();
            return grid;
        }

        public IGridBuilder<TModel> EmptyText(string text)
        {
            grid.EmptyText = text;
            return this;
        }

        public IGridBuilder<TModel> Columns(Action<IColumnsBuilder<TModel>> setup)
        {
            setup(ColumnsBuilder);
            return this;
        }

        public IGridBuilder<TModel> Rows(IEnumerable<TModel> items)
        {
            grid.Rows = items as IEnumerable<object>;
            return this;
        }

        public IGridBuilder<TModel> Pagination(Action<IPagerBuilder> setup)
        {
            setup(PagerBuilder);
            return this;
        }

        private void ThrowIfNoColumns()
        {
            if (grid.Columns == null || !grid.Columns.Any())
            {
                throw new ArgumentException("Columns werent generated, please check you columns definition");
            }
        }
    }
}