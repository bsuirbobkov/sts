﻿using System;
using System.Linq;
using STS.Web.Grid.Builders.Interfaces;
using STS.Web.Grid.Components;
using STS.Web.Grid.Components.Implementations;

namespace STS.Web.Grid.Builders.Implementations
{
    public class PagerBuilder : IPagerBuilder
    {
        private readonly IPager pager = new Pager();
        private string viewName = "Grid/Pager";

        public IPager Build()
        {
            pager.PartialViewName = viewName;
            return pager;
        }

        public IPagerBuilder CurrentPage(int page = 1)
        {
            ThrowIfLessThanOne(page);
            pager.CurrentPage = page;
            return this;
        }

        public IPagerBuilder TotalPages(int count = 1)
        {
            pager.TotalPages = count;
            return this;
        }

        public IPagerBuilder PagesToDisplay(int count = 1)
        {
            ThrowIfLessThanOne(count);
            pager.PagesToDisplay = count;
            return this;
        }

        public IPagerBuilder RowsPerPage(int rows)
        {
            pager.RowsPerPage = rows;
            return this;
        }

        public IPagerBuilder PageSizes(params int[] rows)
        {
            if (rows == null || !rows.Any())
            {
                return this;
            }

            foreach (var totalRows in rows)
            {
                ThrowIfLessThanOne(totalRows);
            }

            pager.PageSizes = rows.ToList();
            return this;
        }

        public IPagerBuilder PartialViewName(string viewName)
        {
            this.viewName = viewName;
            return this;
        }

        private static void ThrowIfLessThanOne(int value)
        {
            if (value < 1)
            {
                throw new ArgumentException("Value cant be less than 1");
            }
        }
    }
}