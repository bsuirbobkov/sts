﻿using System;
using STS.Web.Grid.Components;

namespace STS.Web.Grid.Builders.Interfaces
{
    public interface IColumnBuilder<out TModel>
    {
        // TODO: think about extension columns builder for finding column by name
        string Name { get; }

        IColumn Build();
        IColumnBuilder<TModel> Titled(string text);
        IColumnBuilder<TModel> HasWitdh(string text);
        IColumnBuilder<TModel> Sortable(bool sortable = true);
        IColumnBuilder<TModel> Encode(bool encode = false);
        IColumnBuilder<TModel> SortAscending(bool order = true);
        IColumnBuilder<TModel> RenderAs(Func<TModel, object> config);
    }
}