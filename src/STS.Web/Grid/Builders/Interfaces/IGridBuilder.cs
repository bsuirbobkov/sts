﻿using System;
using System.Collections.Generic;
using STS.Web.Grid.Components;

namespace STS.Web.Grid.Builders.Interfaces
{
    public interface IGridBuilder<TModel>
    {
        IColumnsBuilder<TModel> ColumnsBuilder { get; }
        IPagerBuilder PagerBuilder { get; }

        IGrid Build();
        IGridBuilder<TModel> EmptyText(string text);
        IGridBuilder<TModel> Columns(Action<IColumnsBuilder<TModel>> setup);
        IGridBuilder<TModel> Rows(IEnumerable<TModel> items);
        IGridBuilder<TModel> Pagination(Action<IPagerBuilder> setup);
    }
}