﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using STS.Web.Grid.Components;

namespace STS.Web.Grid.Builders.Interfaces
{
    public interface IColumnsBuilder<TModel>
    {
        IList<IColumnBuilder<TModel>> ColumnBuilders { get; }
        IList<IColumn> Build();
        IColumnBuilder<TModel> Define(string columnName);
        IColumnBuilder<TModel> Define<TProperty>(Expression<Func<TModel, TProperty>> column);
    }
}