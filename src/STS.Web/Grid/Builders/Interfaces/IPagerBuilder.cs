﻿using STS.Web.Grid.Components;

namespace STS.Web.Grid.Builders.Interfaces
{
    public interface IPagerBuilder
    {
        IPager Build();
        IPagerBuilder CurrentPage(int page = 1);
        IPagerBuilder TotalPages(int count = 1);
        IPagerBuilder PagesToDisplay(int count = 1);
        IPagerBuilder RowsPerPage(int rows);
        IPagerBuilder PageSizes(params int[] rows);
        IPagerBuilder PartialViewName(string viewName);
    }
}