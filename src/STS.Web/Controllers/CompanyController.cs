using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STS.Application.Filters;
using STS.Application.Models.Company;
using STS.Application.Models.Document;
using STS.Application.Models.User;
using STS.Application.Services.Interfaces;
using STS.CrossCutting.Enums;
using STS.Web.Components;

namespace STS.Web.Controllers
{
    [Authorize(Roles = nameof(UserRole.CompanyUser))]
    public class CompanyController : BaseController
    {
        private readonly ICompanyService companyService;

        public CompanyController(ICompanyService companyService)
        {
            this.companyService = companyService;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region Documents
        public IActionResult Documents()
        {
            ViewBag.CompanyId = CompanyId;
            return View();
        }

        [ResponseCache(CacheProfileName = "NoChache")]
        public IActionResult GetDocuments(Filter filter)
        {
            return ViewComponent(typeof(CompanyDocumentsGridViewComponent), new { filter, CompanyId });
        }

        public IActionResult AddDocument()
        {
            var viewModel = companyService.GetDocumentUploadViewModel(CompanyId);
            return View(viewModel);
        }

        [HttpPost]
        public JsonResult AddDocument(DocumentUploadModel documentUploadModel)
        {
            var saveResult = companyService.SaveDocumentTemporary(documentUploadModel.DocumentModel);
            if (!ModelState.IsValid || saveResult.Error)
            {
                var fieldNames = ModelState.Keys.ToList();
                var errors = new Dictionary<string, string>();
                foreach (var fieldName in fieldNames)
                {
                    var error = ModelState[fieldName].Errors.FirstOrDefault();
                    if (error != null)
                        errors.Add(fieldName, error.ErrorMessage);
                }
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(errors);
            }
            companyService.SaveDocument(documentUploadModel, UserId);
            return Json(new { message = "Success" });
        }
        #endregion

        #region Users
        public IActionResult Users()
        {
            ViewBag.CompanyId = CompanyId;
            return View();
        }

        [ResponseCache(CacheProfileName = "NoChache")]
        public IActionResult GetUsers(Filter filter)
        {
            return ViewComponent(typeof(CompanyUsersGridViewComponent), new { filter, CompanyId });
        }

        [HttpGet]
        public IActionResult EditUser(int? userId = null)
        {
            var viewModel = companyService.GetUserEditViewModel(userId);
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult EditUser(UserEditViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Check input data");
                return View(viewModel);
            }
            if (viewModel.Id > 0)
            {
                companyService.UpdateUser(viewModel, CompanyId);
            }
            else
            {
                companyService.AddUser(viewModel, CompanyId, UserId);
            }
            return RedirectToAction(nameof(Users));
        }
        #endregion

        #region Metrics
        [HttpGet]
        [ResponseCache(CacheProfileName = "NoChache")]
        public IActionResult Metrics(int? id)
        {
            var model = companyService.GetMetricsViewModel(id ?? 0, CompanyId);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Metrics(MetricsViewModel model)
        {
            foreach (var error in companyService.CheckYear(CompanyId, model))
            {
                ModelState.AddModelError(nameof(model.LastDayOfTheYear), error);
            }
            if (!ModelState.IsValid)
            {
                companyService.RestoreMetricsViewModel(model, CompanyId);
                return View(model);
            }
            if (model.Id == 0)
            {
                var metricId = companyService.AddMetric(model, CompanyId);
                return RedirectToAction("Metrics", new { id = metricId });
            }
            if (model.Id > 0 && companyService.GetMetrics(CompanyId).Any(x => x.Id == model.Id))
            {
                companyService.UpdateMetric(model, CompanyId);
                return RedirectToAction("Metrics", new { id = model.Id });
            }
            return RedirectToAction("Metrics");
        }
        #endregion

        public IActionResult Sustainability(int? categoryId = null)
        {
            var hasAccess = companyService.HasAccess(CompanyId, categoryId ?? 0);
            var isCategoryExist = companyService.IsCategoryExist(categoryId ?? 0);
            if (!hasAccess && !isCategoryExist)
            {
                return RedirectToAction("PageNotFound", "Error");
            }
            var targetCategoryId = !hasAccess
                ? companyService.GetActualCategoryId(CompanyId)
                : categoryId ?? companyService.GetActualCategoryId(CompanyId);
            var model = companyService.GetSustainabilityPageViewModel(CompanyId, targetCategoryId);
            return View(model);
        }

        #region Survey

        public IActionResult Survey()
        {
            var viewModel = companyService.GetSurveyViewModel(CompanyId);
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Survey(SurveyViewModel viewModel)
        {
            companyService.SubmitQuestions(viewModel.Questions, CompanyId);
            return RedirectToAction("Index");
        }
        #endregion
    }
}