﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STS.CrossCutting.Enums;

namespace STS.Web.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            if (User.IsInRole(nameof(UserRole.CompanyUser)))
            {
                return RedirectToAction("Index", "Company");
            }
            if (User.IsInRole(nameof(UserRole.SuperAdmin)))
            {
                return RedirectToAction("Index", "Admin");
            }
            return RedirectToAction("Index", "Error");
        }
    }
}
