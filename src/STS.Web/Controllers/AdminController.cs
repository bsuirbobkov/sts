using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STS.Application.Filters;
using STS.Application.Models.Admin;
using STS.Application.Models.Category;
using STS.Application.Models.Company;
using STS.Application.Models.ManageSustainability;
using STS.Application.Models.User;
using STS.Application.Services.Interfaces;
using STS.CrossCutting.Enums;
using STS.Web.Components;

namespace STS.Web.Controllers
{
    [Authorize(Roles = nameof(UserRole.SuperAdmin))]
    public class AdminController : BaseController
    {
        private readonly IAdminService adminService;

        public AdminController(IAdminService adminService)
        {
            this.adminService = adminService;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region Users
        public IActionResult Users()
        {
            return View();
        }

        [ResponseCache(CacheProfileName = "NoChache")]
        public IActionResult GetUsers(Filter filter)
        {
            return ViewComponent(typeof(AdminUsersGridViewComponent), new { filter });
        }

        public IActionResult EditUser(int userId)
        {
            var model = adminService.GetUserEditViewModel(userId);
            return View(model);
        }

        [HttpPost]
        public IActionResult EditUser(UserEditViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Check input data");
                return View(viewModel);
            }
            if (viewModel.Id > 0)
            {
                adminService.UpdateUser(viewModel);
            }
            else
            {
                adminService.AddUser(viewModel, UserId);
            }
            return RedirectToAction("Users");
        }
        #endregion

        #region Companies
        public IActionResult Companies()
        {
            ViewBag.CompanyId = CompanyId;
            return View();
        }

        [ResponseCache(CacheProfileName = "NoChache")]
        public IActionResult GetCompanies(Filter filter)
        {
            return ViewComponent(typeof(AdminCompaniesGridViewComponent), new { filter });
        }

        public IActionResult EditCompany(int? companyId = null)
        {
            var viewModel = adminService.GetCompanyEditViewModel(companyId);
            viewModel.EditMode = companyId != null;
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult EditCompany(CompanyEditViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Lead Company information is not valid. Please, check it.");
                adminService.RestoreCompanyEditViewModel(viewModel);
                return View(viewModel);
            }
            if (viewModel.EditMode)
            {
                adminService.UpdateCompany(viewModel, UserId);
            }
            else
            {
                adminService.AddCompany(viewModel, UserId);
            }
            return RedirectToAction(nameof(Companies), "Admin");
        }
        #endregion

        #region Rules
        public IActionResult Rules()
        {
            var viewModel = adminService.GetCategoriesViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public bool Rules(IEnumerable<CategoryModel> categories)
        {
            adminService.UpdateCategories(categories);
            return true;
        }
        #endregion

        #region Reports
        [HttpGet]
        public IActionResult Reports()
        {
            var result = adminService.GetReportViewModel();
            return View(result);
        }

        [HttpPost]
        public IActionResult Reports(int reportType, string startDate, string endDate)
        {
            var validationResult = adminService.ValidateReportDates(startDate, endDate);
            if (validationResult.HasError)
            {
                return BadRequest(validationResult);
            }
            var result = adminService.GetReportData(reportType, startDate, endDate);
            return Json(result);
        }
        #endregion

        #region Company sustainability
        public IActionResult Sustainability(int? id = null, int? categoryId = null)
        {
            var companyId = id ?? adminService.GetTargetOrFirstCompanyId(CompanyId);
            if (companyId == 0)
            {
                return View("Sustainability/Error", new ErrorModel()
                {
                    CompanyId = companyId,
                    Message = "There are no companies."
                });
            }

            var targetCategoryId = categoryId ?? 0;
            var hasAccess = adminService.CompanyHasAccess(companyId, targetCategoryId);
            targetCategoryId = hasAccess && targetCategoryId != 0
                ? targetCategoryId
                : adminService.GetActualCategoryIdForCompany(companyId);

            var viewModel = adminService.GetCategories(companyId, targetCategoryId);
            return View("Sustainability/Sustainability", viewModel);
        }

        public IActionResult Documents(int? id = null)
        {
            var companyId = id ?? adminService.GetTargetOrFirstCompanyId(CompanyId);
            var model = adminService.GetDocumentsViewModel(companyId);
            model.CompanyId = companyId;
            return View("Sustainability/Documents", model);
        }

        [ResponseCache(CacheProfileName = "NoChache")]
        public IActionResult GetDocuments(DocumentsFilter filter, int companyId)
        {
            return ViewComponent(typeof(AdminDocumentsGridViewComponent), new { filter, companyId });
        }

        public IActionResult Metrics(int id, int? metricId)
        {
            var companyId = adminService.GetTargetOrFirstCompanyId(id);
            MetricsViewModel viewModel;
            string errorMessage;
            if (metricId != null)
            {
                viewModel = adminService.GetMetricsViewModel(companyId, metricId.Value);
                errorMessage = "This company doesn't have such metric.";
            }
            else
            {
                viewModel = adminService.GetMetricsViewModel(companyId);
                errorMessage = "This company has not reported environmental data.";
            }
            if (viewModel == null)
            {
                return View("Sustainability/Error", new ErrorModel()
                {
                    CompanyId = companyId,
                    Message = errorMessage
                });
            }
            return View("Sustainability/Metrics", new MetricsModel()
            {
                CompanyId = companyId,
                Metrics = viewModel
            });
        }
        #endregion

        #region Questions
        public IActionResult Questions()
        {
            return View();
        }

        [ResponseCache(CacheProfileName = "NoCache")]
        public IActionResult GetQuestions(Filter filter)
        {
            return ViewComponent(typeof(AdminQuestionsGridViewComponent), new { filter = filter });
        }

        public IActionResult EditQuestion(int? id)
        {
            var viewModel = adminService.GetQuestionEditViewModel(id ?? 0);
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult EditQuestion(QuestionEditViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }
            if (viewModel.Id > 0)
            {
                adminService.UpdateQuestion(viewModel);
            }
            else
            {
                adminService.AddQuestions(viewModel);
            }
            return RedirectToAction("Questions");
        }
        #endregion
    }
}