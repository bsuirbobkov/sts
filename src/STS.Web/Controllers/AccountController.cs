using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using STS.Application.Identity;
using STS.Application.Models.Account;
using STS.Application.Services.Interfaces;
using STS.CrossCutting.Enums;

namespace STS.Web.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IAccountService accountService;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IAccountService accountService)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.accountService = accountService;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignIn(SignInViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            var signInResult = await signInManager.PasswordSignInAsync(
                viewModel.UserName,
                viewModel.Password,
                viewModel.RememberMe,
                true);

            if (!signInResult.Succeeded)
            {
                ModelState.AddModelError("credentials", "Wrong Username");
                return View(viewModel);
            }

            var applicationUser = accountService.GetByUserName(viewModel.UserName);
            if (applicationUser.UserStatus != UserStatus.Active)
            {
                return View("Lockout");
            }

            if (applicationUser.IsInRole(UserRole.CompanyUser))
            {
                return RedirectToAction("Index", "Company");
            }

            if (applicationUser.IsInRole(UserRole.SuperAdmin))
            {
                return RedirectToAction("Index", "Admin");
            }

            return View(viewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> SignUp(string key)
        {
            if (!accountService.IsKeyValid(key))
            {
                return View("LinkNotValid");
            }
            if (accountService.IsPasswordExistsForKey(key))
            {
                await signInManager.SignOutAsync();
                return RedirectToAction("SignIn");
            }
            if (signInManager.IsSignedIn(User))
            {
                await signInManager.SignOutAsync();
                return RedirectToAction("SignUp", new { key });
            }
            var viewModel = accountService.GetSignUpViewModel(key);
            return View(viewModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SignUp(SignUpViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }

            if (!accountService.IsKeyValid(viewModel.SecurityKey))
            {
                return View("LinkNotValid");
            }

            accountService.ExpireKeyState(viewModel.SecurityKey);
            var applicationUser = accountService.GetByUserName(viewModel.UserName);
            var token = await userManager.GeneratePasswordResetTokenAsync(applicationUser);
            await userManager.ResetPasswordAsync(applicationUser, token, viewModel.Password);

            var isLocked = applicationUser.UserStatus != UserStatus.Active;
            await userManager.SetLockoutEnabledAsync(applicationUser, isLocked);

            return RedirectToAction("SignIn");
        }

        public async Task<IActionResult> LogOff()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("SignIn", "Account");
        }

        [HttpGet]
        [Authorize(Roles = "CompanyUser")]
        public IActionResult ResendEmail(int userId)
        {
            accountService.ResendEmail(userId, UserId);
            return RedirectToAction("Users", "Company");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = accountService.GetByEmail(model.Email);
            if (user != null)
            {
                accountService.RestorePassword(user);
            }
            ViewBag.SuccessText = "If the e-mail is registered in the system, a link to reset the password has been sent to the e-mail provided.";
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string key = null)
        {
            if (!accountService.IsKeyValid(key))
            {
                return View("LinkNotValid");
            }
            var passwordViewModel = accountService.GetResetPasswordViewModel(key);
            accountService.ExpireKeyState(key);

            return passwordViewModel == null ? (IActionResult)RedirectToAction(nameof(SignIn)) : View(passwordViewModel);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (!accountService.IsKeyValid(model.Code))
            {
                return View("LinkNotValid");
            }
            var applicationUser = accountService.GetByUserKeyAsync(model.Code);
            var token = await userManager.GeneratePasswordResetTokenAsync(applicationUser);
            var result = await userManager.ResetPasswordAsync(applicationUser, token, model.Password);
            accountService.ExpireKeyState(model.Code);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(SignIn));
            }
            return View("Error");
        }
    }
}
