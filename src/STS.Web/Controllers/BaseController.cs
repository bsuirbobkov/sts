﻿using System.IdentityModel.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using STS.CrossCutting.Constans;

namespace STS.Web.Controllers
{
    public class BaseController : Controller
    {
        protected int UserId
        {
            get
            {
                if (User == null)
                {
                    return 0;
                }
                int result;
                var id = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                int.TryParse(id, out result);
                return result;
            }
        }

        protected int CompanyId
        {
            get
            {
                if (User == null)
                {
                    return 0;
                }
                int result;
                var id = User.FindFirst(ClaimTypesConstant.CompanyId)?.Value;
                int.TryParse(id, out result);
                return result;
            }
        }

        protected string CompanyName => User.FindFirst(ClaimTypesConstant.CompanyName)?.Value;
        protected string UserName => User.FindFirst(ClaimTypesConstant.UserName)?.Value;

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (string.IsNullOrEmpty(LinkConstant.PathBase))
            {
                LinkConstant.FillRequest(Request);
            }
        }
    }
}