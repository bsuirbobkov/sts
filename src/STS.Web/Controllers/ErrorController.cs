using Microsoft.AspNetCore.Mvc;

namespace STS.Web.Controllers
{
    public class ErrorController : BaseController
    {
        public ActionResult Index()
        {
            //Response.StatusCode = 400;
            return View("Error");
        }

        public ViewResult PageNotFound()
        {
            //Response.StatusCode = 404;
            NotFound();
            return View();
        }

        public virtual ViewResult AccessDenied()
        {
            //Response.StatusCode = 403;
            return View();
        }
    }
}
