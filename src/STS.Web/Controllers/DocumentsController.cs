﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STS.Application.Services.Interfaces;
using STS.CrossCutting.Enums;

namespace STS.Web.Controllers
{
    [Authorize]
    public class DocumentsController : BaseController
    {
        private readonly IDocumentService documentService;

        public DocumentsController(IDocumentService documentService)
        {
            this.documentService = documentService;
        }

        public FileContentResult DownloadFile(int id)
        {
            var hasAccess = documentService.HasAccess(UserId, id);
            var isAdmin = User.IsInRole(nameof(UserRole.SuperAdmin));

            if (hasAccess || isAdmin)
            {
                var model = documentService.GetDocument(id);
                return File(model.Data, model.MimeType, model.FileName);
            }

            return null;
        }
    }
}