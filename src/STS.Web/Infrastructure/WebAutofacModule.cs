﻿using Autofac;
using STS.Application.Services.Interfaces;
using STS.Web.Services;

namespace STS.Web.Infrastructure
{
    public class WebAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SendEmailService>().As<ISendEmailService>().InstancePerDependency();
            base.Load(builder);
        }
    }
}