﻿module SustainabilityStore {
    import BaseStore = Stores.BaseStore;
    import SustanabilityModel = Sustainability.ISustanabilityModel;

    export class SustanabilityStore extends BaseStore {
        private _activeCategory: CategoryModel;
        private _categories: Array<CategoryModel>;
        private _documentTypes: Array<DocumentTypeModel>;
        private _metricTrends: Array<BaseEnumModel>;
        private _documentConsiderations: Array<BaseEnumModel>;
        private _metricConsiderations: Array<BaseEnumModel>;
        private _orders: Array<BaseEnumModel> = new Array<BaseEnumModel>({ id: 1, name: "Increase" }, { id: 0, name: "Decrease" });

        constructor(model: SustanabilityModel) {
            super();

            this._documentTypes = model.documentTypes;
            this._metricTrends = model.metricTrends;
            this._documentConsiderations = model.documentConsiderations;
            this._metricConsiderations = model.metricConsiderations;

            this._categories = model.categories;
            this.initTodoItems();

            this.setActiveCategory(model.categories[0].id);
        }

        private initTodoItems = () => {

            for (let categoryIndex = 0; categoryIndex < this._categories.length; categoryIndex++) {
                const category = new CategoryModel(this._categories[categoryIndex]);
                category.todos = this._categories[categoryIndex].todos;

                for (let todoIndex = 0; todoIndex < category.todos.length; todoIndex++) {
                    const todo = category.todos[todoIndex];
                    const type = TodoTypes[todo.todoType];
                    let documentTodo;

                    switch (type) {
                        case TodoTypes.DocumentTodo: {
                            documentTodo = new DocumentTodoModel(todo);
                            documentTodo.documents = (todo as DocumentTodoModel).documents;
                            for (let documentTodoItemIndex = 0;
                                documentTodoItemIndex < documentTodo.documents.length;
                                documentTodoItemIndex++) {

                                const todoItem = documentTodo.documents[documentTodoItemIndex];
                                documentTodo.documents[documentTodoItemIndex] = new DocumentTodoItemModel(todoItem);
                            }
                        } break;

                        case TodoTypes.SurveyTodo: {
                            documentTodo = new SurveyTodoModel(todo);
                            documentTodo.surveys = (todo as SurveyTodoModel).surveys;
                            for (let documentTodoItemIndex = 0;
                                documentTodoItemIndex < documentTodo.surveys.length;
                                documentTodoItemIndex++) {

                                const todoItem = documentTodo.surveys[documentTodoItemIndex];
                                documentTodo.surveys[documentTodoItemIndex] = new SurveyTodoItemModel(todoItem);
                            }
                        } break;

                        case TodoTypes.MetricTodo: {
                            documentTodo = new MetricTodoModel(todo);
                            documentTodo.metrics = (todo as MetricTodoModel).metrics;
                            for (let documentTodoItemIndex = 0;
                                documentTodoItemIndex < documentTodo.metrics.length;
                                documentTodoItemIndex++) {

                                const todoItem = documentTodo.metrics[documentTodoItemIndex];
                                documentTodo.metrics[documentTodoItemIndex] = new MetricTodoItemModel(todoItem);
                            }
                        } break;
                    }

                    category.todos[todoIndex] = documentTodo;
                }

                this._categories[categoryIndex] = category;
            }

        }

        get categoryValidationState(): boolean {
            const result = this._activeCategory.isValid();
            this.emitChange();
            return result;
        }
        get documentTypes(): Array<DocumentTypeModel> { return this._documentTypes; }
        get metricTrends(): Array<BaseEnumModel> { return this._metricTrends; }
        get orders(): Array<BaseEnumModel> { return this._orders; }
        get categories(): Array<CategoryModel> { return this._categories; }
        get documentConsiderations(): Array<BaseEnumModel> { return this._documentConsiderations; }
        get metricConsiderations(): Array<BaseEnumModel> { return this._metricConsiderations; }
        get activeCategory(): CategoryModel {
            return this._activeCategory;
        }

        private findTodoModel = <T extends BaseTodoModel>(type: TodoTypes): T => {
            let result: T = null;
            if (this._activeCategory.todos.length) {
                const todoModels = this._activeCategory.todos;
                const todoModel = todoModels.filter(item => item.todoType === TodoTypes[type]);
                if (todoModel.length) {
                    result = (todoModel[0] as T);
                }
            }
            return result;
        }

        setActiveCategory = (id: number) => {
            if (!this._activeCategory || this._activeCategory.isValid()) {
                this._categories.forEach(item => { item.active = item.id === id });
                this._activeCategory = this._categories.filter(item => item.id === id)[0];
            }
            this.emitChange();
        }

        updateTodoState = (id: number) => {
            if (this._activeCategory.todos.length) {
                const todos = this._activeCategory.todos;
                const todo = todos.filter(item => item.id === id);
                if (todo.length && todo[0].isValid()) {
                    todo[0].enabled = !todo[0].enabled;
                }
                this.emitChange();
            }
        }

        createTodo = (type: TodoTypes) => {
            if (this._activeCategory.todos.length) {
                const todos = this._activeCategory.todos;
                const todo = todos.filter(item => item.todoType === TodoTypes[type]);
                const todoModel: any = (todo[0] as DocumentTodoModel) || (todo[0] as MetricTodoModel) || (todo[0] as SurveyTodoModel);
                const todoItems: Array<BaseTodoItemModel> = todoModel.documents || todoModel.metrics || todoModel.surveys;
                let newTodoItem: BaseTodoItemModel;
                switch (type) {
                    case TodoTypes.DocumentTodo:
                        newTodoItem = new DocumentTodoItemModel();
                        break;
                    case TodoTypes.MetricTodo:
                        newTodoItem = new MetricTodoItemModel();
                        break;
                    case TodoTypes.SurveyTodo:
                        newTodoItem = new SurveyTodoItemModel();
                        break;
                    default:
                }
                todoItems.push(newTodoItem);
                this.emitChange();
            }
        }

        removeTodo = (id: number, type: TodoTypes) => {
            if (this._activeCategory.todos.length) {
                const todos = this._activeCategory.todos;
                const todo = todos.filter(item => item.todoType === TodoTypes[type]);
                const todoModel: any = (todo[0] as DocumentTodoModel) || (todo[0] as MetricTodoModel) || (todo[0] as SurveyTodoModel);
                const todoItems: Array<BaseTodoItemModel> = todoModel.documents || todoModel.metrics || todoModel.surveys;
                if (todo.length) {
                    const todoItem = todoItems.filter(item => item.id === id);
                    let index = todoItems.indexOf(todoItem[0]);
                    if (index > -1) {
                        todoItems.splice(index, 1);
                    }
                    this.emitChange();
                }
            }
        }

        updateDocumentTodoType = (todoId: number, documentTypeId: number) => {
            const todoModel = this.findTodoModel<DocumentTodoModel>(TodoTypes.DocumentTodo);
            if (!!todoModel) {
                let todoItem = todoModel.documents.filter(item => item.id === todoId)[0];
                this.setConsiderationIfNecessary(todoItem, documentTypeId);
                todoItem.documentTypeId = documentTypeId;
                todoItem.documentTypeValidator();
                todoItem.considerationTimeValidator();
                this.emitChange();
            }
        }

        private setConsiderationIfNecessary = (todoItemModel: DocumentTodoItemModel, documentTypeId: number): void => {
            if (!todoItemModel.considerationTime) {
                const documentTypes = this._documentTypes.filter(x => x.id === documentTypeId);

                if (documentTypes && documentTypes.length > 0) {
                    const valueToSet = documentTypes[0].useDateRange
                        ? DocumentConsideration.FromUploadingDate
                        : DocumentConsideration.FromEndingDate;

                    todoItemModel.considerationTime = valueToSet;
                }
            }
        }

        updateDocumentTodoLink = (todoId: number, hyperlink: string) => {
            const todoModel = this.findTodoModel<DocumentTodoModel>(TodoTypes.DocumentTodo);
            if (!!todoModel) {
                let todoItem = todoModel.documents.filter(item => item.id === todoId)[0];
                todoItem.hyperlink = hyperlink;
                todoItem.linkValidator();
                this.emitChange();
            }
        }

        updateDocumentTodoQuantity = (todoId: number, val: number) => {
            const todoModel = this.findTodoModel<DocumentTodoModel>(TodoTypes.DocumentTodo);
            if (!!todoModel) {
                let todoItem = todoModel.documents.filter(item => item.id === todoId)[0];
                todoItem.quantity = val;
                todoItem.quantityValidator();
                this.emitChange();
            }
        }

        updateDocumentTodoTime = (todoId: number, val: number) => {
            const todoModel = this.findTodoModel<DocumentTodoModel>(TodoTypes.DocumentTodo);
            if (!!todoModel) {
                let todoItem = todoModel.documents.filter(item => item.id === todoId)[0];
                todoItem.considerationTimeInMonths = val;
                todoItem.considerationTimeInMonthsValidator();
                this.emitChange();
            }
        }

        updateDocumentTodoConsideration = (todoId: number, considerationTime: number) => {
            const todoModel = this.findTodoModel<DocumentTodoModel>(TodoTypes.DocumentTodo);
            if (!!todoModel) {
                let todoItem = todoModel.documents.filter(item => item.id === todoId)[0];
                todoItem.considerationTime = considerationTime;
                todoItem.considerationTimeValidator();
                this.emitChange();
            }
        }

        updateMetricTodoTrend = (todoId: number, metricTrendId: number) => {
            const todoModel = this.findTodoModel<MetricTodoModel>(TodoTypes.MetricTodo);
            if (!!todoModel) {
                let todoItem = todoModel.metrics.filter(item => item.id === todoId)[0];
                todoItem.metricTrendId = metricTrendId;
                todoItem.metricTrendValidator();
                this.emitChange();
            }
        }

        updateMetricTodoOrder = (todoId: number, orderId: number) => {
            const todoModel = this.findTodoModel<MetricTodoModel>(TodoTypes.MetricTodo);
            if (!!todoModel) {
                let todoItem = todoModel.metrics.filter(item => item.id === todoId)[0];
                todoItem.increase = orderId > 0;
                this.emitChange();
            }
        }

        updateMetricTodoRange = (todoId: number, rangeInPercents: number) => {
            const todoModel = this.findTodoModel<MetricTodoModel>(TodoTypes.MetricTodo);
            if (!!todoModel) {
                let todoItem = todoModel.metrics.filter(item => item.id === todoId)[0];
                todoItem.rangeInPercents = rangeInPercents;
                todoItem.rangeValidator();
                this.emitChange();
            }
        }

        updateMetricTodoTime = (todoId: number, considerationTimeInMonths: number) => {
            const todoModel = this.findTodoModel<MetricTodoModel>(TodoTypes.MetricTodo);
            if (!!todoModel) {
                let todoItem = todoModel.metrics.filter(item => item.id === todoId)[0];
                todoItem.considerationTimeInMonths = considerationTimeInMonths;
                todoItem.considerationTimeInMonthsValidator();
                this.emitChange();
            }
        }

        updateMetricTodoConsideration = (todoId: number, considerationTime: number) => {
            const todoModel = this.findTodoModel<MetricTodoModel>(TodoTypes.MetricTodo);
            if (!!todoModel) {
                let todoItem = todoModel.metrics.filter(item => item.id === todoId)[0];
                todoItem.considerationTime = considerationTime;
                todoItem.considerationTimeValidator();
                this.emitChange();
            }
        }

        updateSurveyTotalScores = (todoId: number, totalScores: number) => {
            const todoModel = this.findTodoModel<SurveyTodoModel>(TodoTypes.SurveyTodo);
            if (!!todoModel) {
                let todoItem = todoModel.surveys.filter(item => item.id === todoId)[0];
                todoItem.totalScores = totalScores;
                todoItem.totalScoresValidator();
                this.emitChange();
            }
        }
    }
}