﻿module Sustainability {
    interface IDocumentItemProps {
        model: DocumentTodoItemModel;
        index: number;
    }

    interface IDocumentItemState {
        documentTypes: Array<DocumentTypeModel>;
        documentConsiderations: Array<BaseEnumModel>;
    }

    export class DocumentItem extends React.Component<IDocumentItemProps, IDocumentItemState> {
        
        constructor(props?: IDocumentItemProps) {
            super(props);
            this.state = {
                documentTypes: appStore.documentTypes,
                documentConsiderations: appStore.documentConsiderations
            };
        }

        render(): JSX.Element {
            return (
                <div className="row acheivement-item">
                    <div className="col-xs-12">
                        <div className="row">
                            <div className="col-xs-12 col-md-6 form-group focused">
                                <div className="row">
                                    <div className="col-xs-1 acheivement-number">{this.props.index}) </div>
                                    <Select items={this.state.documentTypes}
                                            value={this.props.model.documentTypeId}
                                            onSelect={this.onDocumentTypeChange}
                                            className="col-xs-12 col-md-11 focused"
                                            label="Sustainability Achievement"
                                            error={this.props.model.errors["documentTypeId"]}/>
                                </div>
                            </div>
                            <InputGroup
                                className="col-xs-12 col-md-4 form-group focused"
                                label="Hyperlink"
                                value={this.props.model.hyperlink}
                                onChange={this.onHyperlinkChange}
                                error={this.props.model.errors["hyperlink"]}/>
                            <InputGroup
                                className="col-xs-12 col-md-2 form-group focused"
                                label="Quantity"
                                value={this.props.model.quantity}
                                onChange={this.onQuantityChange}
                                error={this.props.model.errors["quantity"]}
                                onlyDigit={true}/>
                        </div>
                        <div className="row">
                            <div className="col-xs-12 col-sm-10 col-md-6 form-group focused">
                                <div className="row">
                                    <Select items={this.state.documentConsiderations}
                                            value={this.props.model.considerationTime}
                                            onSelect={this.onConsiderationChange}
                                            className="col-xs-12 col-md-11 col-md-offset-1"
                                            label="Time Consideration"
                                            error={this.props.model.errors["considerationTime"]}/>
                                </div>
                            </div>
                            <InputGroup
                                className="col-xs-12 col-sm-2 col-md-4 form-group focused"
                                label="<span>(mo.) </span>"
                                value={this.props.model.considerationTimeInMonths}
                                onChange={this.onConsiderationTimeChange}
                                error={this.props.model.errors["considerationTimeInMonths"]}
                                onlyDigit={true}/>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-1 acheivement-del">
                        <button onClick={(event) => $.fn
                        .confirmationDialog(event.target, this.onRemoveDocument) } className="icon icon-trash"></button>
                    </div>
                </div>
            );
        }

        private onRemoveDocument = (): void => {
            appStore.removeTodo(this.props.model.id, TodoTypes.DocumentTodo);
        }

        private onDocumentTypeChange = (id: number) => {
            appStore.updateDocumentTodoType(this.props.model.id, id);
        }

        private onConsiderationChange = (value: number) => {
            appStore.updateDocumentTodoConsideration(this.props.model.id, value);
        }

        private onHyperlinkChange = (event: React.SyntheticEvent) => {
            const value = (event.target as any).value;
            appStore.updateDocumentTodoLink(this.props.model.id, value);
        }
        private onQuantityChange = (event: React.SyntheticEvent) => {
            const parsValue = parseInt((event.target as any).value, 10);
            appStore.updateDocumentTodoQuantity(this.props.model.id, parsValue | 0);
        }
        private onConsiderationTimeChange = (event: React.SyntheticEvent) => {
            const parsValue = parseInt((event.target as any).value, 10);
            appStore.updateDocumentTodoTime(this.props.model.id, parsValue | 0);
        }
    }
}