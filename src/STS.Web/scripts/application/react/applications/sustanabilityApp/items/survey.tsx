﻿module Sustainability {
    interface ISurveItemProps {
        model: SurveyTodoItemModel;
    }

    interface ISurveyItemState {
        totalScores: number;
    }

    export class SurveyItem extends React.Component<ISurveItemProps, ISurveyItemState> {
        private static nonNumericRegex: RegExp = /[^0-9.]+/g;

        constructor(props: ISurveItemProps) {
            super(props);
            this.state = {
                totalScores: props.model.totalScores,
            };
        }

        componentWillReceiveProps(nextProps: ISurveItemProps, nextContext): void {
            this.setState(prevState => {
                prevState.totalScores = nextProps.model.totalScores;
                return prevState;
            });
        }

        private onTotalScoresChanged = (event: React.SyntheticEvent): void => {
            const newValue = this.getNumericValue(event);
            appStore.updateSurveyTotalScores(this.props.model.id, newValue);
        }

        private onRemoveSurvey = (): void => {
            appStore.removeTodo(this.props.model.id, TodoTypes.SurveyTodo);
        }

        private getNumericValue = (event: React.SyntheticEvent): number => {
            const target = (event.target as any);
            const value = target.value.replace(SurveyItem.nonNumericRegex, "") | 0;
            return value;
        }

        private getValidator = (name: string): JSX.Element => {
            return (this.props.model.errors && this.props.model.errors[name]
                ? <span className="error field-validation-error" data-valmsg-replace="true">
                    <span className="has-error">{this.props.model.errors[name]}</span>
                </span>
                : null);
        }

        render(): JSX.Element {
            return (
                <div className="acheivement-item row">
                    <div className="col-xs-12">
                        <div className="row">
                            <InputGroup
                                className="col-xs-12 col-md-6 form-group focused"
                                label="Total Scores Count To Achieve This Level"
                                value={this.props.model.totalScores}
                                onChange={this.onTotalScoresChanged}
                                error={this.props.model.errors["totalScoresCount"]}
                                onlyDigit={true}/>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-1 acheivement-del">
                        <button onClick={(event) => $.fn.confirmationDialog(event.target, () => this.onRemoveSurvey()) } className="icon icon-trash"></button>
                    </div>
                </div>
            );
        }


    }
}
