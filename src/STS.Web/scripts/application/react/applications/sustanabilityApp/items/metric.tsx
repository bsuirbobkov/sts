﻿module Sustainability {
    interface IMetricItemProps {
        model: MetricTodoItemModel;
        index: number;
    }

    interface IMetricItemState {
        metricTrends: Array<BaseEnumModel>;
        orders: Array<BaseEnumModel>;
        metricConsiderations: Array<BaseEnumModel>;
    }

    export class MetricItem extends React.Component<IMetricItemProps, IMetricItemState> {
        constructor(props?: IMetricItemProps) {
            super(props);
            this.state = {
                metricTrends: appStore.metricTrends,
                orders: appStore.orders,
                metricConsiderations: appStore.metricConsiderations
            };
        }

        render(): JSX.Element {
            return (
                <div className="row acheivement-item">
                    <div className="col-xs-12">
                        <div className="row">
                            <div className="col-xs-12 col-md-6 form-group focused">
                                <div className="row">
                                    <div className="col-xs-1 acheivement-number">
                                        {this.props.index})
                                    </div>
                                    <Select label="Metric Trend"
                                        className="col-xs-12 col-md-11 focused"
                                        items={this.state.metricTrends}
                                        onSelect={this.onTrendChanged}
                                        value={this.props.model.metricTrendId}
                                        error={this.props.model.errors["metricTrendId"]}/>
                                </div>
                            </div>
                            <Select label="Behavior"
                                className="col-xs-12 col-md-3 form-group focused"
                                value={this.props.model.increase ? 1 : 0}
                                items={this.state.orders}
                                onSelect={this.onOrderChanged}/>
                            <InputGroup
                                className="col-xs-12 col-md-3 form-group focused"
                                label="Threshold (%)"
                                value={this.props.model.rangeInPercents}
                                onChange={this.onRangeChanged}
                                error={this.props.model.errors["rangeInPercents"]}
                                onlyDigit={true}/>
                        </div>
                        <div className="row">
                            <div className="col-xs-12 col-sm-10 col-md-6 form-group focused">
                                <div className="row">
                                    <Select items={this.state.metricConsiderations}
                                        value={this.props.model.considerationTime}
                                        onSelect={this.onConsiderationChange}
                                        className="col-xs-12 col-md-11 col-md-offset-1 focused"
                                        error={this.props.model.errors["considerationTime"]}
                                        label="Time Consideration"/>
                                </div>
                            </div>
                            <InputGroup
                                className="col-xs-12 col-sm-2 col-md-3 form-group focused"
                                label="<span>(mo.) </span>"
                                value={this.props.model.considerationTimeInMonths}
                                onChange={this.onConsiderationTimeChanged}
                                error={this.props.model.errors["considerationTimeInMonths"]}
                                onlyDigit={true}/>
                        </div>
                    </div>
                    <div className="col-xs-12 col-sm-1 acheivement-del">
                        <button onClick={ (event) => $.fn
                            .confirmationDialog(event.target, this.onRemoveMetric) } className="icon icon-trash"></button>
                    </div>
                </div>
            );
        }

        private onRemoveMetric = (): void => {
            appStore.removeTodo(this.props.model.id, TodoTypes.MetricTodo);
        }

        private onConsiderationChange = (value: number) => {
            appStore.updateMetricTodoConsideration(this.props.model.id, value);
        }

        private onTrendChanged = (id: number) => {
            appStore.updateMetricTodoTrend(this.props.model.id, id);
        }

        private onOrderChanged = (id: number) => {
            appStore.updateMetricTodoOrder(this.props.model.id, id);
        }

        private onRangeChanged = (event: React.SyntheticEvent) => {
            const value = (event.target as any).value;
            const result = parseInt(value, 10) | 0;
            appStore.updateMetricTodoRange(this.props.model.id, result);
        }

        private onConsiderationTimeChanged = (event: React.SyntheticEvent) => {
            const value = (event.target as any).value;
            const result = parseInt(value, 10) | 0;
            appStore.updateMetricTodoTime(this.props.model.id, result);
        }

    }
}