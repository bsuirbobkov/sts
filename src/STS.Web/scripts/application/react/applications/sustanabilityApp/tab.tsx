﻿module Sustainability {
    interface ISustanabilityTabProps {
        category: CategoryModel;
        saveLink: string;
        redirectLink: string;
    }

    interface ITabState {
        isInitialized: boolean;
    }

    export class SustanabilityTab extends React.Component<ISustanabilityTabProps, ITabState> {
        constructor(props?: any) {
            super(props);
            this.state = {
                isInitialized: false
            };
        }

        componentDidMount(): void {
            this.setState(prevState => {
                prevState.isInitialized = true;
                return prevState;
            });
        }

        render(): JSX.Element {
            const className = `tab-pane ${(!this.state.isInitialized || this.props.category.active ? "active" : "")}`;

            let documentTodos: Array<BaseTodoModel> = new Array<BaseTodoModel>(),
                metricTodos: Array<BaseTodoModel> = new Array<BaseTodoModel>(),
                surveyTodos: Array<BaseTodoModel> = new Array<BaseTodoModel>();

            if (this.props.category.todos) {
                documentTodos = this.props.category.todos
                    .filter(todo => todo.todoType === TodoTypes[TodoTypes.DocumentTodo]);
                metricTodos = this.props.category.todos
                    .filter(todo => todo.todoType === TodoTypes[TodoTypes.MetricTodo]);
                surveyTodos = this.props.category.todos
                    .filter(todo => todo.todoType === TodoTypes[TodoTypes.SurveyTodo]);
            }

            return (
                <div className={className}>
                    <h2>{this.props.category.name}</h2>
                    {documentTodos.map(item => <DocumentContainer key={item.id} model={item as DocumentTodoModel}/>) }
                    {surveyTodos.map(item => <SurveyContainer key={item.id} model={item as SurveyTodoModel}/>) }
                    {metricTodos.map(item => <MetricContainer key={item.id} model={item as MetricTodoModel}/>) }
                    <div className="row edit-form-btns h-mt30">
                        <div className="col-xs-12 col-sm-2 col-sm-offset-4">
                            <button className="btn btn-cancel" onClick={this.onCancelClicked}>Cancel</button>
                        </div>
                        <div className="col-xs-12 col-sm-2">
                            <button className="btn" onClick={this.onSaveClicked}>Save</button>
                        </div>
                    </div>
                </div>
            );
        }

        private onSaveClicked = (event: React.SyntheticEvent): void => {
            event.preventDefault();
            if (appStore.categoryValidationState) {
                this.save();
            }
        }

        private onCancelClicked = (event: React.SyntheticEvent): void => {
            event.preventDefault();
            $.fn.confirmationDialog(null,
                () => this.redirect(),
                "Are you sure you want to leave this page? All the unsaved data will be lost.");
        }

        private save = (): void => {
            jQuery.ajax({
                url: this.props.saveLink,
                method: "POST",
                data: {
                    categories: appStore.categories
                },
                error: (result) => {
                    $.fn.confirmationDialog(null, null, `An error occurred while saving rules! ${result.responseJSON}`);
                },
                complete: () => {
                    $.fn.confirmationDialog(null, null, "Data saved successfully!");
                }
            });
        }

        private redirect = (): void => {
            window.location.href = this.props.redirectLink;
        }
    }
}