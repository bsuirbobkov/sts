﻿module Sustainability {
    interface ISustanabilityPageProps {
        saveLink: string;
        redirectLink: string;
    }

    interface ISustanabilityPageState {
        categories: Array<CategoryModel>
    }

    export class SustanabilityPage extends React.Component<ISustanabilityPageProps, ISustanabilityPageState> {
        constructor(props?: any) {
            super(props);
            this.state = this.getInitState();
        }

        componentDidMount(): void {
            appStore.addChangeListener(this.onStoreChange);
        }

        componentWillMount(): void {
            appStore.removeChangeListener(this.onStoreChange);
        }

        render() {
            return (
                <div className="edit-form-wr">
                    <h1>Manage Sustainability Levels</h1>
                    <SustanabilityTabHeaders categories={this.state.categories}/>
                    <div className="tab-content edit-form">
                        { this.state.categories.map(category => this.printTab(category)) }
                    </div>
                </div>
            );
        };

        private printTab = (category: CategoryModel): JSX.Element => {
            return (
                <SustanabilityTab key={category.id} category={category}
                  redirectLink={this.props.redirectLink} saveLink={this.props.saveLink}/>
            );
        }

        private getInitState = (): ISustanabilityPageState => {
            const categories = appStore.categories && appStore.categories.length
                ? appStore.categories
                : new Array<CategoryModel>();
            return { categories: categories };
        }

        private onStoreChange = (): void => {
            this.setState(prevState => {
                prevState.categories = appStore.categories;
                return prevState;
            });
        }
    }
}
