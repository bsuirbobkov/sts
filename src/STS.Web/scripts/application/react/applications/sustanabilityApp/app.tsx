﻿let appStore: SustainabilityStore.SustanabilityStore;
module Sustainability {

    export interface ISustanabilityModel {
        categories: Array<CategoryModel>;
        documentTypes: Array<DocumentTypeModel>;
        metricTrends: Array<BaseEnumModel>;
        documentConsiderations: Array<BaseEnumModel>;
        metricConsiderations: Array<BaseEnumModel>;
    }

    interface ISustanabilityWrapperProps {
        model: any;
        saveLink: string;
        redirectLink: string;
    }

    export class SustanabilityApplication extends React.Component<ISustanabilityWrapperProps, any> {

        constructor(props: ISustanabilityWrapperProps) {
            super(props);
            appStore = new SustainabilityStore.SustanabilityStore(props.model);
        }

        render() {
            return (<div><SustanabilityPage saveLink={this.props.saveLink} redirectLink={this.props.redirectLink}/></div>);
        }
    }   
}
