﻿class BaseEnumModel {
    id: number;
    name: string;
}

class BaseTodoModel extends BaseEnumModel {
    enabled: boolean;
    todoType: string;

    constructor(model?: any) {
        super();
        if (model) {
            this.id = model.id;
            this.name = model.name;
            this.enabled = model.enabled;
            this.todoType = model.todoType;
        }
    }

    isValid = (): boolean => { return true; }
}

class DocumentTypeModel extends BaseEnumModel {
    useDateRange: boolean
}

class CategoryModel extends BaseEnumModel {
    orderWeight: number;
    active: boolean;
    todos: Array<BaseTodoModel>;

    constructor(model: any) {
        super();
        if (model) {
            this.orderWeight = model.orderWeight;
            this.active = model.active;
            this.id = model.id;
            this.name = model.name;
        }
    }

    isValid = (): boolean => {
        let res = !(this.todos.filter(item => !item.isValid()).length);
        return res;
    }
}

class DocumentTodoModel extends BaseTodoModel {
    documents: Array<DocumentTodoItemModel>;

    constructor(model?: any) {
        super(model);
    }

    isValid = (): boolean => {
        let res = !(this.documents.filter(item => !item.isValid()).length);
        return res;
    }
}

class MetricTodoModel extends BaseTodoModel {
    metrics: Array<MetricTodoItemModel>;

    constructor(model?: any) {
        super(model);
    }

    isValid = (): boolean => {
        let res = !(this.metrics.filter(item => !item.isValid()).length);
        return res;
    }
}

class SurveyTodoModel extends BaseTodoModel {
    surveys: Array<SurveyTodoItemModel>;

    constructor(model?: any) {
        super(model);
    }

    isValid = (): boolean => {
        let res = !(this.surveys.filter(item => !item.isValid()).length);
        return res;
    }
}

class BaseTodoItemModel {
    private static newId: number = 1;

    id: number;
    considerationTimeInMonths: number;
    errors: any = {};
    
    constructor() {
        this.id = BaseTodoItemModel.newId;
        ++BaseTodoItemModel.newId;
    }

    get isEmpty(): boolean { return true; }
    isValid = (): boolean => { return true; }
}

class DocumentTodoItemModel extends BaseTodoItemModel {
    private urlRegex = /^(https|http|ftp):\/\/(www\.)?[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)$/g;

    considerationTime: DocumentConsideration;
    documentTypeId: number;
    hyperlink: string;
    quantity: number;

    get isEmpty(): boolean {
        return !(this.documentTypeId && this.hyperlink && this.considerationTimeInMonths && this.quantity && this.considerationTime);
    }

    constructor(state?: any) {
        super();
        if (state) {
            this.considerationTime = state.considerationTime;
            this.considerationTimeInMonths = state.considerationTimeInMonths;
            this.documentTypeId = state.documentTypeId;
            this.hyperlink = state.hyperlink;
            this.quantity = state.quantity;
        } else {
            this.considerationTime = 0;
            this.considerationTimeInMonths = 0;
            this.documentTypeId = 0;
            this.hyperlink = "";
            this.quantity = 0;
        }
    }

    isValid = (): boolean => {

        this.linkValidator();
        this.quantityValidator();
        this.documentTypeValidator();
        this.considerationTimeInMonthsValidator();
        this.considerationTimeValidator();

        return $.isEmptyObject(this.errors);
    }

    considerationTimeValidator = (): void => {
        if (this.considerationTime <= 0) {
            this.errors["considerationTime"] = " is required";
            return;
        } else {
            delete this.errors["considerationTime"];
        }
    }

    documentTypeValidator = (): void => {
        if (this.documentTypeId <= 0) {
            this.errors["documentTypeId"] = " is required";
            return;
        } else {
            delete this.errors["documentTypeId"];
        }
    }

    linkValidator = (): void => {
        if (!this.hyperlink.length) {
            this.errors["hyperlink"] = "Link is required";
            return;
        } else {
            delete this.errors["hyperlink"];
        }

        if (this.hyperlink.length > 500) {
            this.errors["hyperlink"] = "Link must be less then 500";
            return;
        } else {
            delete this.errors["hyperlink"];
        }

        if (!this.hyperlink.match(this.urlRegex)) {
            this.errors["hyperlink"] = "Link is not valid";
            return;
        } else {
            delete this.errors["hyperlink"];
        }
    }

    quantityValidator = (): void => {
        if (this.quantity < 1) {
            this.errors["quantity"] = "Quantity must be more then 0";
            return;
        } else {
            delete this.errors["quantity"];
        }

        if (this.quantity > 99) {
            this.errors["quantity"] = "Quantity must be less then 100";
            return;
        } else {
            delete this.errors["quantity"];
        }
    }

    considerationTimeInMonthsValidator = (): void => {
        if (this.considerationTimeInMonths < 1) {
            this.errors["considerationTimeInMonths"] = "Consideration Time must be more then 0";
            return;
        } else {
            delete this.errors["considerationTimeInMonths"];
        }
    }
}

class MetricTodoItemModel extends BaseTodoItemModel {

    considerationTime: number;
    increase: boolean;
    metricTrendId: number;
    rangeInPercents: number;
    get isEmpty(): boolean {
        return !(this.metricTrendId && this.rangeInPercents && this.considerationTimeInMonths && this.considerationTime);
    }

    constructor(state?: any) {
        super();
        if (state) {
            this.considerationTime = state.considerationTime;
            this.considerationTimeInMonths = state.considerationTimeInMonths;
            this.increase = state.increase;
            this.metricTrendId = state.metricTrendId;
            this.rangeInPercents = state.rangeInPercents;
        } else {
            this.considerationTime = 0;
            this.considerationTimeInMonths = 0;
            this.increase = true;
            this.metricTrendId = 0;
            this.rangeInPercents = 0;
        }
    }

    isValid = (): boolean => {

        this.rangeValidator();
        this.considerationTimeInMonthsValidator();
        this.considerationTimeValidator();
        this.metricTrendValidator();

        return $.isEmptyObject(this.errors);
    }

    considerationTimeValidator = (): void => {
        if (this.considerationTime <= 0) {
            this.errors["considerationTime"] = " is required";
            return;
        } else {
            delete this.errors["considerationTime"];
        }
    }

    rangeValidator = (): void => {
        if (this.rangeInPercents < 1) {
            this.errors["rangeInPercents"] = "Percents must be more then 0";
            return;
        } else {
            delete this.errors["rangeInPercents"];
        }

        if (this.rangeInPercents > 1000) {
            this.errors["rangeInPercents"] = "Percents should not be more than 1000";
            return;
        } else {
            delete this.errors["quantity"];
        }
    }

    metricTrendValidator = (): void => {
        if (this.metricTrendId <= 0) {
            this.errors["metricTrendId"] = " is required";
            return;
        } else {
            delete this.errors["metricTrendId"];
        }
    }

    considerationTimeInMonthsValidator = (): void => {
        if (this.considerationTimeInMonths < 1) {
            this.errors["considerationTimeInMonths"] = "Consideration Time must be more then 0";
            return;
        } else {
            delete this.errors["considerationTimeInMonths"];
        }
    }
}

class SurveyTodoItemModel extends BaseTodoItemModel {
    totalScores: number;
    get isEmpty(): boolean { return !this.totalScores;}
    
    constructor(state?: any) {
        super();
        if (state) {
            this.considerationTimeInMonths = state.considerationTimeInMonths;
            this.totalScores = state.totalScores;
        } else {
            this.considerationTimeInMonths = 0;
            this.totalScores = 0;
        }
    }

    isValid = (): boolean => {
        this.totalScoresValidator();
        return $.isEmptyObject(this.errors);
    }

    totalScoresValidator = (): void => {
        if (this.totalScores < 1) {
            this.errors["totalScoresCount"] = "Number of scores must be more then 0";
            return;
        } else {
            delete this.errors["totalScoresCount"];
        }

        if (this.totalScores > 100) {
            this.errors["totalScoresCount"] = "Number of scores should not be more than 100";
            return;
        } else {
            delete this.errors["totalScoresCount"];
        }
    }
}

enum TodoTypes {
    DocumentTodo = 1,
    SurveyTodo = 2,
    MetricTodo = 3
}

enum DocumentConsideration {
    FromUploadingDate = 1,
    FromEndingDate = 2
}

enum MetricConsideration {
    FromEnterDate = 1,
    FromEndingDate = 2
}