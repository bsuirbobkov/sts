﻿module Sustainability {
    interface IDocumentContainerProps {
        model: DocumentTodoModel;
    }

    export class DocumentContainer extends React.Component<IDocumentContainerProps, any> {
        constructor(props?: IDocumentContainerProps) {
            super(props);
        }

        render(): JSX.Element {
            const hasEmpty: boolean = this.props.model.documents ? !!this.props.model.documents.filter(item => item.isEmpty).length : false;
            return (
                <TodoContainer label="Required Achievements" isActive={this.props.model.enabled} onChange={this.changeTodoState} id={this.props.model.id}>
                    {this.props.model.documents ? this.props.model.documents.map((item, index) => <DocumentItem key={item.id} model={item} index={++index}/>) : null }
                    <div className="row">
                        <div className={`col-xs-12 ${hasEmpty ? "readonly" : ""}`}>
                            <a href="#" onClick={this.createTodo} className={`icon icon-add ${hasEmpty ? "icon-add-disabled" : "icon-add-red"}`}>Add Another Achievement</a>
                        </div>
                    </div>
                </TodoContainer>
            );
        }

        private changeTodoState = (): void => {
            appStore.updateTodoState(this.props.model.id);
        }

        private createTodo = (event: React.SyntheticEvent): void => {
            event.preventDefault();
            appStore.createTodo(TodoTypes.DocumentTodo);
        }
    }
}