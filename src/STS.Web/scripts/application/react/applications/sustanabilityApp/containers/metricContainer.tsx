﻿module Sustainability {
    interface IMetricConainerProps {
        model: MetricTodoModel;
    }

    export class MetricContainer extends React.Component<IMetricConainerProps, any> {
        render(): JSX.Element {
            const hasEmpty: boolean = this.props.model.metrics ? !!this.props.model.metrics.filter(item => item.isEmpty).length : false;
            return (
                <TodoContainer label="Required Metric Trend" isActive={this.props.model.enabled} onChange={this.changeTodoState} id={this.props.model.id}>
                    {this.props.model.metrics ? this.props.model.metrics.map((item, index) => <MetricItem model={item} key={item.id} index={++index}/>) : null}
                    <div className="row">
                        <div className={`col-xs-12 ${hasEmpty ? "readonly" : ""}`}>
                            <a href="#" onClick={this.createTodo} className={`icon icon-add ${hasEmpty ? "icon-add-disabled" : "icon-add-red"}`}>Compare Another Metric</a>
                        </div>
                    </div>
                </TodoContainer>
            );
        }

        private changeTodoState = (): void => {
            appStore.updateTodoState(this.props.model.id);
        }

        private createTodo = (event: React.SyntheticEvent): void => {
            event.preventDefault();
            appStore.createTodo(TodoTypes.MetricTodo);
        }
    }
}