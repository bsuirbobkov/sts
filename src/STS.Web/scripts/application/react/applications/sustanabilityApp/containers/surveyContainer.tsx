﻿module Sustainability {
    interface ISurveyContainerProps {
        model: SurveyTodoModel;
    }

    export class SurveyContainer extends React.Component<ISurveyContainerProps, any> {
        render(): JSX.Element {
            const hasItems = !!(this.props.model.surveys && this.props.model.surveys.length);
            return (
                <TodoContainer label="Required Survey Status" isActive={this.props.model.enabled} onChange={this.changeTodoState} id={this.props.model.id}>
                    {this.props.model.surveys ? this.props.model.surveys.map(survey => <SurveyItem key={survey.id} model={survey}/>) : null }
                    {hasItems
                        ? null
                        : <div className="row">
                            <div className="col-xs-12">
                                <a href="#" onClick={this.createTodo} className="icon icon-add icon-add-red">Add Another Survey</a>
                            </div>
                        </div>
                    }

                </TodoContainer>
            );
        }

        private changeTodoState = () => {
            appStore.updateTodoState(this.props.model.id);
        }

        private createTodo = (event: React.SyntheticEvent): void => {
            event.preventDefault();
            appStore.createTodo(TodoTypes.SurveyTodo);
        }
    }
}