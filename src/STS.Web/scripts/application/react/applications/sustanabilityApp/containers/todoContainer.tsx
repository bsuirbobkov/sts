﻿module Sustainability {
    interface ITodoContainerProps {
        isActive: boolean;
        label: string;
        onChange(): void;
        id?: number;
    }

    export class TodoContainer extends React.Component<ITodoContainerProps, any> {
        constructor(props?: ITodoContainerProps) {
            super(props);
        }
        render(): JSX.Element {
            const disabledClass = this.props.isActive ? "" : "inactive";
            return (<div className={`edit-form ${disabledClass}`}>
                <div className="row acheivement-dashed">
                    <div className="col-xs-12 text-center">
                        <div className="checkbox-toggle">
                            <input type="checkbox" className="checkbox" id={"todo-status-" + this.props.id} onChange={this.props.onChange} checked={this.props.isActive}/>
                            <label htmlFor={"todo-status-" + this.props.id}>{this.props.label}</label>
                        </div>
                    </div>
                </div>
                {this.props.isActive ?
                    this.props.children :
                    <fieldset disabled="disabled" readOnly className="readonly">
                        {this.props.children}
                    </fieldset>
                }
            </div>);
        }
    }
}