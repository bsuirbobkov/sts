﻿module Sustainability {
    interface ITabsProps {
        categories: Array<CategoryModel>;
    }

    export class SustanabilityTabHeaders extends React.Component<ITabsProps, any> {

        private onTabSelect = (event: React.MouseEvent, id: number): void => {
            if (!appStore.activeCategory.isValid()) {
                $.fn.confirmationDialog(null,
                    null,
                    "Invalid data. Please fix the highlighted validation errors to continue.");
            }
            appStore.setActiveCategory(id);
        }

        private renderTab(category: CategoryModel): JSX.Element {
            const className = category.active ? "active" : "";
            return (
                <li key={category.id} className={className}>
                    <a href="#" onClick={(event) => this.onTabSelect(event, category.id) }>{category.name}</a>
                </li>);
        }

        render(): JSX.Element {
            return (
                <div>
                    <ul className="nav nav-tabs">
                        { this.props.categories.map(category => this.renderTab(category)) }
                    </ul>
                </div>
            );
        }
    }
}