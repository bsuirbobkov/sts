﻿/// <reference path="uploadForm.tsx"/>
interface IDocumentState {
    ready: boolean;
}

interface IDocumentUploadModel {
    documentTypes: Array<IDocumentTypeModel>;
    fiscaleDate: string;
}

interface IDocumentWrapperModel {
    saveLink: string;
    redirectLink: string;
    model: IDocumentUploadModel; 
}

class DocumentUpload extends React.Component<IDocumentWrapperModel, any> {
    constructor(props?: any) {
        super(props);
        this.state = {
            ready: false
        };
    }

    componentDidMount(): void {
        setTimeout(() => {
            this.setState({ ready: true });
        }, 10);
    }

    render() {
        return (<div>{this.state.ready ? <UploadForm documentTypes={this.props.model.documentTypes} fiscaleDate={this.props.model.fiscaleDate} saveLink={this.props.saveLink} redirectLink={this.props.redirectLink}/> : "Loading..."}</div>);
    }
}
