﻿interface IDocumentUploadProps {
    documentTypes: Array<IDocumentTypeModel>;
    fiscaleDate: string;
    saveLink: string;
    redirectLink: string;
}

interface IDocumentUploadState {
    documentModel: DocumentModel;
    description: string;
    fromDate: string;
    toDate: string;
    documentType: IDocumentTypeModel;
    validationState: boolean;

    isTryToSave: boolean;
}

import ErrorConstants = Constants.ErrorConstants;

class UploadForm extends React.Component<IDocumentUploadProps, IDocumentUploadState> {

    private errors: ErrorDictionary;

    constructor(props?: any) {
        super(props);
        this.errors = new ErrorDictionary();
        this.state = {
            documentModel: new DocumentModel(null, null, null),
            description: "",
            fromDate: "",
            toDate: "",
            documentType: null,
            validationState: false,

            isTryToSave: false
        };
    }

    // Handlers
    private handleSaveDocument = (): void => {

        let result = new DocumentUploadModel();

        this.validateDocument();
        this.validateDocumentType();
        this.validateDateFrom();
        this.validateDateTo();

        if (this.errors.isEmpty() && !this.state.isTryToSave) {

            result.description = this.state.description;
            result.documentTypeId = this.state.documentType.id;
            result.documentModel = this.state.documentModel;
            result.fromDate = this.state.fromDate;
            result.toDate = this.state.toDate || null;

            this.sendDocumentModel(result);
        }
    }

    private handleCancel = (): void => {
        this.redirect();
    }

    private sendDocumentModel = (model: DocumentUploadModel) => {
        this.setState((prevState) => {
            prevState.isTryToSave = true;
            return prevState;
        });

        var formDataUtil = new FormDataUtil();
        var formData = formDataUtil.jsonToFormData(model);
        const timeout = 15000;

        jQuery.ajax({
            url: this.props.saveLink,
            contentType: false,
            processData: false,
            data: formData,
            type: 'POST',
            timeout: timeout,
            success: () => {
                setTimeout(() => {
                    this.setState((prevState) => {
                        prevState.isTryToSave = false;
                        return prevState;
                    });
                    this.redirect();
                }, 1000);

            },
            error: (errors: any) => {
                // TODO: need check other 
                this.setState((prevState) => {
                    this.errors.documentModel = "Check your document";
                    prevState.validationState = false;
                    prevState.isTryToSave = false;
                    return prevState;
                });
            }
        });
    }

    private redirect = (): void => {
        window.location.href = this.props.redirectLink;
    }

    private handleUploadFile = (event: Event): void => {
        event.preventDefault();

        const reader = new FileReader();
        const input: any = $(event.target)[0];
        const newFile = input.files[0];

        reader.onloadend = (reader) => {
            const base64 = (reader.target as FileReader).result;
            const fileName = newFile.name.substr(0, newFile.name.lastIndexOf('.'));
            const extension = newFile.name.substr(newFile.name.lastIndexOf('.'), newFile.name.length);

            const docModel = new DocumentModel(fileName, base64, extension);

            input.value = null;

            this.setState((prevState) => {
                prevState.documentModel = docModel;
                return prevState;
            });
            this.validateDocument();
        }

        if (newFile) {
            reader.readAsDataURL(newFile);
        }
    }

    private handleSelectType = (value: number): void => {
        this.setState((prevState) => {
            let newType = this.props.documentTypes.filter(item => item.id === value)[0];
            prevState.documentType = newType;
            return prevState;
        });
        this.validateDocumentType();

        if (this.state.fromDate && this.state.documentType) {
            this.validateDateFrom();
        }
    }

    private handleDescriptionChange = (e: React.SyntheticEvent) => {
        const newDescription: string = $(e.target).val();
        this.setState((prevState) => {
            prevState.description = newDescription;
            return prevState;
        });
    }

    private handleDateFrom = (dateString: string): void => {
        this.setState((prevState) => {
            prevState.fromDate = dateString;
            return prevState;
        });
        this.validateDateFrom();
    }

    private handleDateTo = (dateString: string): void => {
        this.setState((prevState) => {
            prevState.toDate = dateString;
            return prevState;
        });
        this.validateDateTo();
    }

    // End Handlers

    // Validators

    private validateDocument = (): void => {
        this.errors.documentModel = !(this.state.documentModel && this.state.documentModel.fileBase64Data) ? "Current Document is not valid" : null;
        this.updateValidationState();
    }

    private validateDocumentType = (): void => {
        this.errors.documentType = !this.state.documentType ? ErrorConstants.required : null;
        this.updateValidationState();
    }

    private validateDateFrom = (): void => {
        this.errors.fromDate = !this.state.fromDate ? ErrorConstants.required : null;

        if (this.state.documentType) {
            if (this.state.fromDate && this.state.toDate) {
                const fromDate = new Date(this.state.fromDate);
                const toDate = new Date(this.state.toDate);
                if (fromDate > toDate) {
                    this.errors.fromDate = `${ErrorConstants.lessThan} End Date`;
                    this.errors.toDate = `${ErrorConstants.moreThan} Start Date`;
                }
            }
        } else {
            this.errors.toDate = null;
        }
        this.updateValidationState();
    }

    private validateDateTo = (): void => {
        this.errors.toDate = !this.state.toDate ? ErrorConstants.required : null;

        if (this.state.documentType) {

            if (this.state.fromDate && this.state.toDate) {
                const fromDate = new Date(this.state.fromDate);
                const toDate = new Date(this.state.toDate);
                if (fromDate > toDate) {
                    this.errors.fromDate = `${ErrorConstants.lessThan} End Date`;
                    this.errors.toDate = `${ErrorConstants.moreThan} Start Date`;
                }
            }
        } else {
            this.errors.toDate = null;
        }
        this.updateValidationState();
    }

    private updateValidationState = (): boolean => {
        const isValid = this.errors.isEmpty();
        this.setState((prevState) => {
            prevState.validationState = isValid;
            return prevState;
        });
        return isValid;
    }
    // End Validators

    render() {
        return (
            <div className="edit-form-wr">
                <h1>Upload New Document</h1>
                <div className="edit-form">
                    <div className="edit-form-inner">
                        <div className="title">
                            <span>Upload New Document</span>
                        </div>
                        <div className="row edit-form-block">
                            <div className="col-sm-4 form-group focused">
                                <div className="upload-file btn">
                                    <div className="label-file"><span className="icon icon-plus"></span>{this.state.documentModel.fileName ? 'Change Document' : 'Select Document'}</div>
                                    <input type="file" className="input-file" onChange={this.handleUploadFile}/>
                                </div>
                                { this.errors.documentModel ?
                                    <span className="error field-validation-error" data-valmsg-replace="true">
                                        <span className="has-error">{this.errors.documentModel}</span></span> :
                                    null}
                            </div>

                            {this.state.documentModel.fileName ?
                                <div className="col-sm-8 form-group focused">
                                    <input type="text" className="form-control form-control-no-label" value={`${this.state.documentModel.fileName}${this.state.documentModel.expectedExtension}`} readOnly/>
                                </div>
                                : null
                            }
                        </div>
                        <div className="row edit-form-block">
                            <div className="col-sm-12 form-group focused">
                                <label>Document Descriptor</label>
                                <textarea className="form-control"
                                    onChange={this.handleDescriptionChange}
                                    value={this.state.description}
                                    type="text"
                                    rows="4"/>
                            </div>
                        </div>
                        <div className="row edit-form-block">
                            <Select label="Select Document type"
                                className="col-sm-6 form-group focused"
                                onSelect={this.handleSelectType}
                                value={this.state.documentType ? this.state.documentType.id : -1}
                                items={this.props.documentTypes}
                                error={this.errors.documentType}/>

                            <Datepicker label="Start Date"
                                onChange={this.handleDateFrom}
                                className="col-sm-3 form-group focused j-datepicker"
                                readOnly={false}
                                error={this.errors.fromDate}/>

                            <Datepicker label="End Date"
                                onChange={this.handleDateTo}
                                className="col-sm-3 form-group focused j-datepicker"
                                readOnly={false}
                                error={this.errors.toDate}/>
                        </div>
                    </div>
                </div>
                <div className="row edit-form-btns">
                    <div className="col-sm-6">
                        <button className="btn btn-cancel" onClick={this.handleCancel}>Cancel</button>
                    </div>
                    <div className="col-sm-6">
                        <button className="btn" onClick={this.handleSaveDocument}>Upload Document</button>
                    </div>
                </div>
            </div>);
    }

    componentDidMount(): void {
        $(".form-control").focus(function () {
            if (!$(this).parent().hasClass("has-error")) {
                $(this).parent().addClass("focused");
            }
        });
    }
}
