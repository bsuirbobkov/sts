﻿class FormDataUtil {
    public jsonToFormData(object: any): FormData {
        const formData = new FormData();
        this.objectToFormData(object, null, formData);
        return formData;
    }

    private getInstanceType(instance: any): string {
        return Object.prototype.toString.call(instance).slice(8, -1);
    }

    private objectToFormData(object: any, path: string, formData: FormData): void {
        for (let key in object) {
            if (!object.hasOwnProperty(key)) {
                continue;
            }

            const type = this.getInstanceType(object[key]);
            switch (type) {
                case "Array":
                    for (let index = 0; index < object[key].length; index++) {
                        const subItem = path ? path : key;
                        const itemKey = `${subItem}[${index}]`;
                        this.objectToFormData(object[key][index], itemKey, formData);
                    }
                    break;

                case "Object":
                    this.objectToFormData(object[key], key, formData);
                    break;

                default:
                    this.appendJsonToForm(object, key, path, formData);
            }
        }
    }

    private appendJsonToForm(object: any, key: string, path: string, formData: FormData): void {
        const formKey = path
            ? `${path}[${key}]`
            : key;

        const value = object[key];
        if (value) {
            formData.append(formKey, object[key]);
        }
    }
}

