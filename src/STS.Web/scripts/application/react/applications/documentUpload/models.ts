﻿interface IBaseViewModel {
    id: number;
    name: string;
}

interface IDocumentTypeModel extends IBaseViewModel { }

class ErrorDictionary {
    documentType: string;
    fromDate: string;
    toDate: string;
    categories: string;
    documentModel: string;

    isEmpty = (): boolean => {
        return !(this.documentType ||
            this.fromDate ||
            this.toDate ||
            this.categories ||
            this.documentModel);
    }
}

class DocumentModel {
    constructor(public fileName: string, public fileBase64Data: string, public expectedExtension: string) { }
}

class DocumentUploadModel {
    documentModel: DocumentModel;
    description: string;
    documentTypeId: number;
    scopeId: number;
    fromDate: string;
    toDate: string;
}