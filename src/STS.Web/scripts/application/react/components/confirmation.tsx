﻿interface IPosition {
    top: number;
    left: number;
}

interface IConfirmationPopupState {
    position: IPosition;
    arrowPosition?: string;
}

interface IConfirmationDialogProps {
    questionText?: string;
    confirmButtonText?: string;
    cancelButtonText?: string;
    activeParentClassName?: string;
    onConfirm?(): void;
    onAbort?(): void;
    parent?: any;
}
class ConfirmationDialog extends React.Component<IConfirmationDialogProps, IConfirmationPopupState> {

    private static arrowWidth: number = 6;

    static defaultProps: IConfirmationDialogProps = {
        questionText: "Are you sure you want to delete this item?",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        activeParentClassName: "icon-trash-active"
    };

    constructor(props?: IConfirmationDialogProps) {
        super(props);
        this.state = {
            position: {
                left: 0,
                top: 0
            }
        };
    }

    componentDidMount(): void {
        this.updateParentNode();
        this.calculatePosition();

        window.addEventListener("resize", this.calculatePosition, false);
    }

    componentWillUnmount(): void {
        this.updateParentNode();
        window.removeEventListener("resize", this.calculatePosition);
    }

    render(): JSX.Element {
        const dialogStyle = this.props.parent
            ? {
                marginTop: '0px',
                position: 'absolute',
                left: this.state.position.left + 'px',
                top: this.state.position.top + 'px'
            }
            : {
                marginTop: this.state.position.top + "px",
                marginLeft: this.state.position.left + "px"
            };
        const wrapperStyle = this.props.parent
            ? null
            : {
                position: "fixed",
                background: "rgba(0, 0, 0, 0.3)"
            };
        const modalWrapperClassName = `modal fade in modal-relative modal-relative-${this.state.arrowPosition ? `${this.state.arrowPosition}` : "none"}`;
        return (
            <div className={modalWrapperClassName} tabIndex="-1" role="dialog" aria-hidden="true" style={wrapperStyle}
                onClick={this.onOutsideClick}>
                <div className="modal-dialog" role="document" ref="modal-dialog" style={dialogStyle}>
                    <div className="modal-content">
                        <div className="modal-body">
                            {this.props.children ? this.props.children : this.props.questionText}
                        </div>
                        {this.props.onConfirm != null ?
                            <div className="row modal-footer">
                                <div className="col-xs-6">
                                    <button type="button" className="btn btn-cancel btn-cancel-noicon" onClick={this.onAbort}>{this.props.cancelButtonText}</button>
                                </div>
                                <div className="col-xs-6">
                                    <button type="button" className="btn btn-primary" onClick={this.onConfirm }>{this.props.confirmButtonText}</button>
                                </div>
                            </div> :
                            <div className="row modal-footer">
                                <div className="col-xs-12">
                                    <button type="button" className="btn btn-primary" onClick={this.onAbort}>Ok</button>
                                </div>
                            </div>
                        }

                    </div>
                </div>
            </div>
        );
    }

    private onConfirm = (event: React.MouseEvent): void => {
        event.stopPropagation();
        this.props.onConfirm();
        this.hideConfirmationDialog();
    }

    private onAbort = (event: React.MouseEvent): void => {
        event.stopPropagation();
        this.hideConfirmationDialog();
    }

    private onOutsideClick = (): void => {
        this.hideConfirmationDialog();
    }

    private hideConfirmationDialog = (): void => {
        const parentNode = ReactDOM.findDOMNode(this).parentNode as Element;
        ReactDOM.unmountComponentAtNode(parentNode);
        parentNode.remove();
    }

    private calculatePosition = (): void => {
        const parent = this.props.parent;
        const modal: any = this.refs["modal-dialog"];
        const dialogWidth = modal.offsetWidth;
        const dialogHeight = modal.offsetHeight;

        if (!this.props.parent) {
            this.setState(prevState => {
                prevState.position.top = ((document.documentElement.clientHeight || document.body.clientHeight || 0) / 2 - dialogHeight);
                prevState.position.left = ((document.documentElement.clientWidth || document.body.clientWidth || 0) - dialogWidth) / 2;
                return prevState;
            });
            return;
        };

        const parentPosition = this.getScreenCordinates(parent);
        const top = parentPosition.top - dialogHeight / 2 + parent.clientHeight / 2;

        let arrowPosition = "left";
        let left = parentPosition.left - dialogWidth - ConfirmationDialog.arrowWidth;
        if (left < 0) {
            left = parentPosition.left + (parent as any).offsetWidth + ConfirmationDialog.arrowWidth;
            arrowPosition = "right";
        }
        this.setState(prevState => {
            prevState.arrowPosition = arrowPosition;
            prevState.position.left = left;
            prevState.position.top = top;
            return prevState;
        });
    }

    private updateParentNode = (): void => {
        const parent = this.props.parent;
        if (parent) {
            const activeClassName = this.props.activeParentClassName;
            const index = parent.className.indexOf(activeClassName);
            if (index > -1) {
                parent.className = parent.className.replace(activeClassName, "");
            } else parent.className += ` ${activeClassName}`;
        }
    }

    private getScreenCordinates = (element: any): IPosition => {
        const position: IPosition = { left: element.offsetLeft, top: element.offsetTop };
        do {
            position.left += element.offsetParent.offsetLeft;
            position.top += element.offsetParent.offsetTop;
            element = element.offsetParent;
        } while (element === document.getElementsByTagName("body")[0]);
        return position;
    }

}

$.fn.confirmationDialog = (element: any, onConfirm: any, questionText: string): void => {
    const wrapper = document.createElement("div");
    document.body.appendChild(wrapper);
    ReactDOM.render(<ConfirmationDialog parent={element} onConfirm={onConfirm} questionText={questionText}/>, wrapper as Element);
}
