﻿interface IMaskInputProps {
    value?: any;
    className?: string;
    label: string;
    onChange(value: string): void;
    error?: string;
    alias?: string;
    options?: any;
}

class MaskInput extends React.Component<IMaskInputProps, any> {
    constructor(props?: any) {
        super(props);
    }

    componentDidMount(): void {
        const maskInputElement = $(this.refs["mask-input"]).inputmask(this.props.alias, this.props.options);
        maskInputElement.on("change", (event: JQueryEventObject) => {
            const value = jQuery(event.target).val();
            this.props.onChange(value);
        });
    }

    componentDidUpdate(prevProps: ISelectProps, prevState: ISelectState, prevContext): void {
        jQuery(this.refs["mask-input"]).val(prevProps.value);
    }

    render() {
        return (
            <div>
                <div className={this.props.className}>
                    <label dangerouslySetInnerHTML={{ __html: this.props.label }}></label>
                    <input ref="mask-input" type="text" className="form-control text-center" value={this.props.value}/>
                </div>
            </div>
        );
    }
}
