﻿interface ISelectProps {
    value?: number;
    className?: string;
    items: Array<any>;
    label: string;
    onSelect?(value: number): void;
    error?: string;
}

interface ISelectState {
    value?: number;
}

class Select extends React.Component<ISelectProps, ISelectState> {
    constructor(props?: any) {
        super(props);
        this.state = {
            value: this.props.value
        };
    }

    componentDidMount(): void {
        const select = (this.refs["select"] as Element);
        const $select = $(select);
        const $chosen = $(this.refs["chosen-select"]).chosen({ disable_search: true });
        $chosen.on("change", (event, data) => {
            this.props.onSelect(parseInt(data.selected, 10));
        });

        select.addEventListener("blur", () => {
            if (this.state.value) {
                this.props.onSelect(this.state.value);
            }
        }, true);

        $select.focus(() => {
            if (!$select.hasClass("has-error")) {
                $select.addClass("focused");
            }
        });
    }

    componentWillUnmount(): void {
        (this.refs["select"] as Element).removeEventListener("blur", () => {
            this.props.onSelect(this.state.value);
        }, true);
    }

    componentDidUpdate(prevProps: ISelectProps, prevState: ISelectState, prevContext): void {
        const $chosen = $(this.refs["chosen-select"]);
        $chosen.val(this.state.value);
        $chosen.trigger("chosen:updated");
    }

    componentWillReceiveProps(nextProps: ISelectProps, nextContext): void {
        this.setState(prevState => {
            prevState.value = nextProps.value;
            return prevState;
        });
    }

    render() {
        const classNames = this.props.className + (this.props.error ? ' has-error' : '');
        return (
            <div className={classNames} ref="select">
                <label>{this.props.label}</label>
                <div>
                    <select ref="chosen-select" className="form-control" data-placeholder="Select one item" defaultValue={this.state.value} readOnly>
                        <option value=""></option>
                        { this.props.items.map((item) => <option key={item.id} value={item.id}>{item.name}</option>) }
                    </select></div>
                { this.props.error ?
                    <span className="error field-validation-error" data-valmsg-replace="true">
                        <span className="has-error">{`${this.props.label}${this.props.error}`}</span></span> :
                    null}
            </div>);
    }
}
