﻿interface IInputGroupProps {
    onlyDigit?: boolean;
    className?: string;
    value: any;
    label: string;
    onChange(event: React.SyntheticEvent): void;
    error?: string;
}

class InputGroup extends React.Component<IInputGroupProps, any> {

    private static nonNumericRegex: RegExp = /[^0-9.]+/g;

    constructor(props?: any) {
        super(props);
    }

    render() {
        const className = this.props.className + (this.props.error ? ' has-error' : '');
        return (
            <div className={className}>
                <label dangerouslySetInnerHTML={{ __html: this.props.label }}></label>
                <input ref="mask-input" type="text" className="form-control text-center"
                    defaultValue={this.props.value}
                    onInput={this.onInput}
                    onBlur={(event) => this.props.onChange(event)}
                   />
                { this.props.error
                    ? <span className="error field-validation-error" data-valmsg-replace="true">
                        <span className="has-error">{this.props.error}</span>
                    </span>
                    : null }
            </div>
        );
    }

    private onInput = (event: React.SyntheticEvent): void => {
        if (this.props.onlyDigit) {
            let target = (event.target as any);
            let value = target.value.replace(InputGroup.nonNumericRegex, "") | 0;
            target.value = value;
        }
    }
}
