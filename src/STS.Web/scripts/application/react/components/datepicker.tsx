﻿interface IDatepickerProps {
    value?: any;
    label?: string;
    className?: string;
    readOnly?: boolean;
    onChange(date: string): void;
    error?: string;
}

class Datepicker extends React.Component<IDatepickerProps, any> {
    constructor(props?: any) {
        super(props);
        this.state = {
            value: this.props.value
        }
    }

    componentDidMount(): void {
        const options = {
            todayHighlight: true,
            maxViewMode: "years",
            orientation: "auto right",
            format: "mm/dd/yyyy",
            autoclose: true
        };
        const $input = $(this.refs["datepicker"]);
        const $datepicker = $input.datepicker(options);

        $input.focus(() => {
            if (!$(this).parent().hasClass("has-error")) {
                $(this).parent().addClass("focused");
            }
        });
        ($input as any).mask("99/99/9999", { placeholder: "mm/dd/yyyy" });
        $datepicker.on("changeDate", (eventObject: DatepickerEventObject) => this.handleChange(eventObject));
        $datepicker.on("hide", (eventObject: DatepickerEventObject) => { this.props.onChange(!this.state.value ? eventObject.format("mm/dd/yyyy") : this.state.value)});
    }

    private handleChange = (eventObject: DatepickerEventObject) => {
        const $input = $(this.refs["datepicker"]);
        eventObject.preventDefault();
        if ($(eventObject.target).val().match(/\d{2}\/\d{2}\/\d{4}/)) {
            const newValue = eventObject.format("mm/dd/yyyy");
            this.setState(prevState => {
                prevState.value = newValue;
                return prevState;
            });
            $input.focus();
            this.props.onChange(newValue);
        }
    }

    render() {
        let classNames = this.props.className + (this.props.error ? ' has-error' : '');
        return (
            <div className={classNames}>
                <label htmlFor="">{this.props.label}</label>
                { !this.props.readOnly ?
                    <input type="text" ref="datepicker" value={this.state.value} className="form-control"/> :
                    <input type="text" ref="datepicker" defaultValue={this.state.value} readOnly className="form-control"/>
                }
                { this.props.error ?
                    <span className="error field-validation-error" data-valmsg-replace="true">
                        <span className="has-error">{`${this.props.label}${this.props.error}`}</span></span> :
                    null}
            </div>);
    }


}
