﻿module Constants {
    export class ErrorConstants {
        static required: string = " is required";
        static notValid: string = " is not valid";
        static moreThan: string = " must be more than";
        static lessThan: string = " must be less than";
        static maxLength: string = " should not be more than ";
    }
}