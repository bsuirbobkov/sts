'use strict';

module Stores {

    let CHANGE_EVENT = "changeStore";

    import EventEmitter = Events.EventEmitter;

    export class BaseStore extends EventEmitter {
        protected updateEvent: Event;

        constructor() {
            super();

            const event = document.createEvent("Event");
            event.initEvent(CHANGE_EVENT, true, true);
            this.updateEvent = event;
        }

        addChangeListener = (listener: EventListener | EventListenerObject) => {
            this.addEventListener(CHANGE_EVENT, listener);
        }

        removeChangeListener = (listener: EventListener | EventListenerObject) => {
            this.removeEventListener(CHANGE_EVENT, listener);
        }

        emitChange = (): void => {
            this.dispatchEvent(this.updateEvent);
        }
    }
}
