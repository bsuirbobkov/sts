﻿module Events {
    export class EventEmitter implements EventTarget {

        private listeners: {} = new Array<any>();

        addEventListener = (type: string, listener: EventListener | EventListenerObject, useCapture?: boolean): void => {
            if (!(type in this.listeners)) {
                this.listeners[type] = [];
            }
            this.listeners[type].push(listener);
        };

        dispatchEvent = (event: any): boolean => {
            if (!(event.type in this.listeners)) {
                return;
            }
            var stack = this.listeners[event.type];
            event.currentTarget = this as EventEmitter;
            event.target = this as EventEmitter;

            for (var i = 0, l = stack.length; i < l; i++) {
                stack[i].call(this, event);
            }
        };

        removeEventListener = (type: string, listener: EventListener | EventListenerObject, useCapture?: boolean): void => {
            if (!(type in this.listeners)) {
                return;
            }
            var stack = this.listeners[type];
            for (var i = 0, l = stack.length; i < l; i++) {
                if (stack[i] === listener) {
                    stack.splice(i, 1);
                    return this.removeEventListener(type, listener);
                }
            }
        };
    }
}