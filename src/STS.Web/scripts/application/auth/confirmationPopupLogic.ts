﻿module Auth.ConfirmationPopupLogic {
    var passwordForm: JQuery = $('#set-password-form');
    var btnShowTerm: JQuery = $('#btn-show-terms');
    var btnAcceptTerm: JQuery = $('#btn-accept-terms');
    var btnDontAcceptTerm: JQuery = $('#btn-dont-accept-terms');
    var btnAcceptExplanation: JQuery = $('#btn-accept-explanation');
    var confirmationWindow: JQuery = $('#dialog-confirm');
    var explanationWindow: JQuery = $('#dialog-explanation');

    var options: ModalOptions = {
        show: true
    };

    btnAcceptExplanation.on('click', (evnt: JQueryEventObject) => {
        evnt.preventDefault();
        explanationWindow.modal('hide');
    });

    btnAcceptTerm.on('click', (evnt: JQueryEventObject) => {
        evnt.preventDefault();
        confirmationWindow.modal('hide');
        passwordForm.submit();
    });

    btnDontAcceptTerm.on('click', (evnt: JQueryEventObject) => {
        evnt.preventDefault();
        confirmationWindow.modal('hide');
        explanationWindow.modal(options);
    });

    btnShowTerm.on('click', (evnt: JQueryEventObject) => {
        evnt.preventDefault();
        if (!passwordForm.valid()) return;
        confirmationWindow.modal(options);
    });
}