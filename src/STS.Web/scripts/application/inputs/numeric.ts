﻿module Inputs {
    export class NumericInput {
        constructor(targetId: string, prefix: string, groupSeparator: string) {
            const target = jQuery(targetId);
            if (isNaN(target.val()) || parseInt(target.val()) === 0) {
                target.val("");
            }

            const options: any = {
                groupSeparator: groupSeparator,
                autoGroup: !!groupSeparator,
                digits: 0,
                prefix: prefix,
                placeholder: "",
                rightAlign: false,
                removeMaskOnSubmit: true,
                allowPlus: false,
                allowMinus: false
            }

            target.inputmask("numeric", options);
        }
    }
}

jQuery.fn.NumericInput = (targetId: string, prefix: string, groupSeparator: string): Inputs.NumericInput => {
    return new Inputs.NumericInput(targetId, prefix, groupSeparator);
}