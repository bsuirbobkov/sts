﻿module Inputs {
    export class CurrencyInput {
        constructor(targetId: string) {
            const target = jQuery(targetId);
            if (parseFloat(target.val()) === 0) {
                target.val("");
            } else {
                target.val(target.val().replace(",", "."));
            }

            const options: any = {
                removeMaskOnSubmit: true,
                placeholder: "",
                digitsOptional: true,
                rightAlign: false,
                onUnMask: (maskedValue, unmaskedValue) => {
                    const result = unmaskedValue.replace(/,/g, "");
                    return result;
                }
            }
            target.inputmask("currency", options);
        }
    }
}

jQuery.fn.CurrencyInput = (targetId: string): Inputs.CurrencyInput => {
    return new Inputs.CurrencyInput(targetId);
}