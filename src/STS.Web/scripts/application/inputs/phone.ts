﻿module Inputs {
    "use strict";
   
    export class PhoneInput {
        constructor(private phoneNumberContainers: Array<JQuery>) {
            this.bindControllStates();
        }

        private bindControllStates(): void {
            if (this.phoneNumberContainers && this.phoneNumberContainers.length) {
                for (let j = 0; j < this.phoneNumberContainers.length; j++) {
                    $(this.phoneNumberContainers[j]).inputmask("(999) 999-9999");
                }
            }
        }
    }
}

jQuery.fn.PhoneInput = (phoneNumberContainers: Array<JQuery>):
    Inputs.PhoneInput => {
    if (!phoneNumberContainers) {
        throw new Error("Containers is missed");
    }
    return new Inputs.PhoneInput(phoneNumberContainers);
};
