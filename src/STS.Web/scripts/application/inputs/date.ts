﻿module Inputs {
    "use strict";

    export class DateInput {
        private target: JQuery;
        private rules: Array<any>;
        private inputMaskOptions: InputMask.JQueryInputMaskOptions = {};

        private datepickerOptions: DatepickerOptions = {
            format: "mm/dd/yyyy",
            todayHighlight: true,
            maxViewMode: "years",
            orientation: "bottom right"
        };

        constructor(targetId: string, datePickerOptions?: any) {
            this.target = jQuery(targetId);
            this.target.parent().addClass("j-datepicker");
            this.target.inputmask("mm/dd/yyyy", this.inputMaskOptions);
            this.target.rules("add", { stringDate: true });
            this.rules = this.target.rules();

            if (!!datePickerOptions) {
                this.datepickerOptions = datePickerOptions;
                this.datepickerOptions.format = "mm/dd/yyyy";
            }
            this.target.on("click", (event) => this.initializeDatepicker(event));
            
        }

        private initializeDatepicker(event: JQueryEventObject): void {
            this.target.off("click", this.initializeDatepicker);

            const datepicker = this.target.datepicker(this.datepickerOptions);
            datepicker.on("hide", () => {
                this.target.rules("add", this.rules);
            });
            datepicker.on("show", () => {
                this.target.rules("remove");
            });
            this.target.focus();
        }
    }
}

jQuery.fn.DateInput = (targetId: string, datePickerOptions?: any): Inputs.DateInput => {
    return new Inputs.DateInput(targetId, datePickerOptions);
}