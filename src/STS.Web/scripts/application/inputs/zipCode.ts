﻿module Inputs {
    export class ZipCodeInput {
        constructor(targetId: string) {
            const target = jQuery(targetId);
            if (isNaN(target.val()) || parseInt(target.val()) === 0) {
                target.val("");
            }

            const options: any = {
               
                placeholder: "",
                rightAlign: false,
                removeMaskOnSubmit: true,
                allowPlus: false,
                allowMinus: false,
                digitsOptional: true,
                greedy: false,
                autoUnmask: true,
                onUnMask: (maskedValue, unmaskedValue) => {
                    const result = unmaskedValue.replace(/-/g, "");
                    return result;
                }
            }

            target.inputmask("99999[-9999]", options);
        }
    }
}

jQuery.fn.ZipCodeInput = (targetId: string): Inputs.ZipCodeInput => {
    return new Inputs.ZipCodeInput(targetId);
}