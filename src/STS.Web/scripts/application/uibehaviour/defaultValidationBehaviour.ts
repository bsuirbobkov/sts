﻿module UiBehavior {

    var defaultOptions: JQueryValidation.ValidationOptions = {
        validClass: "has-success-custom",
        errorClass: "has-error",
        highlight(element, errorClass, validClass) {
            $(element).closest(".form-group")
                .removeClass(validClass)
                .addClass("has-error");
        },
        unhighlight(element, errorClass, validClass) {
            $(element).closest(".form-group")
                .removeClass("has-error")
                .addClass(validClass);

        },
        onfocusout: (element: any) => {
            if (element.type === "password") return;
            $(element).val($.trim($(element).val()));
        }
    };

    $.validator.setDefaults(defaultOptions);
    $.validator.addMethod("stringDate", (value, element, params) => {
        return !/Invalid|NaN/.test(new Date(value).toString());
    }, $.validator.format("The Date field in this format is not valid."));

    $.validator.unobtrusive.options = {
        errorClass: defaultOptions.errorClass,
        validClass: defaultOptions.validClass
    };
}
