﻿module UiBehavior {
    "use strict";
    var formControl: JQuery = $(".form-control");

    var addFocused = element => {
        $(element).parent().addClass("focused");
    };
    var addError = element => {
        $(element).parent().addClass("has-error");
    };
    var checkValue = () => {
        formControl.each((index, element) => {
            var jElement = $(element);
            if (jElement.val() || jElement.css("backgroundColor") !== "rgb(255, 255, 255)") {
                addFocused(element);
            }
        });
    };
    checkValue();
    setTimeout(checkValue, 10);
    setTimeout(checkValue, 100);

    $("input.input-validation-error").ready(() => {
        addError($("input.input-validation-error"));
    });

    formControl.bind("focusout", function () {
        addFocused(this);
        const validator = $(this).closest("form").validate();
        validator.element(this);
    });

    formControl.on("paste", function () {
        addFocused(this);
        $(this).keypress();
    });

    formControl.focus(function () {
        if (!$(this).parent().hasClass("has-error")) {
            addFocused(this);
        }
    });


    formControl.keypress(function () {
        addFocused(this);
    });

    // todo: extract into another class and/or file
    $(".nav-left-menu").click(() => {
        $(".nav-left").toggleClass("open");
        $("body").toggleClass("nav-expanded");
    });

    let dropdowns = $(".chosen-select");
    if (dropdowns.length) {
        dropdowns.fixedChosen();
    }

    var container: any = $(".j-custom-scroll");

    function setContentSize(height: any) {
        container.height(height);
    }

    function calculateSize() {
        const win = $(window);
        const winHeight = win.height();
        const headerHeight = $(".header").outerHeight();
        const footerHeight = $(".footer").outerHeight();
        let tabContainer = 0;

        const tabPane = container.parent(".tab-pane");
        if (tabPane && tabPane.length > 0) {
            const tabContainerTop = $(".tab-content").offset().top;
            tabContainer = tabContainerTop - headerHeight + 92;
        }

        return winHeight - headerHeight - footerHeight - tabContainer;
    }

    setContentSize(calculateSize());

    container.mCustomScrollbar({
        axis: "y"
    });
}