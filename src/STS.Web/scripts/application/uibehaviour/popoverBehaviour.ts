﻿module UiBehavior {
    export class PopoverBehaviour {
        setupElementBehaviour(targetElement: JQuery, settings?: any): void {
            const defaultSettings: any = {
                html: true,
                trigger: "manual",
                title: `More Info <a class="close" data-dismiss="alert">&times;</a>`
            }

            const targetSettings = settings
                ? settings
                : defaultSettings;

            targetElement
                .click(this.onPopupIconClick)
                .popover(targetSettings);
        }

        setupBodyBehaviour(): void {
            $("body").on("click", this.onBodyClickedCallback);
        }

        private onPopupIconClick = (event: JQueryEventObject): void => {
            $('[data-toggle="popover"]').popover("hide");
            $(event.target).popover("toggle");
            $(".close").click(() => {
                $(".close").parent().parent().popover("hide");
            });
        }

        private onBodyClickedCallback = (event: JQueryEventObject): void => {
            if ($(event.target).data("toggle") !== "popover"
                && $(event.target).parents(".popover.in").length === 0) {
                $('[data-toggle="popover"]').popover("hide");
            }
        }
    }
}