﻿module Report{
    "use strict";

    export class ChartContainer {
        constructor(private chartContainer: JQuery) {
            this.bindHighcharts();
        }

        private bindHighcharts(): void{
        this.chartContainer.highcharts({
            title: {
                text: "Report",
                x: -20 //center
            },
            subtitle: {
                text: "Empty",
                x: -20
            },
            yAxis: {
                min: 1,
                max: 50
            },
            xAxis: {
                min: 1,
                max: 50
            },
            series: [
                {
                    name: null,
                    data: null
                }
            ],
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            }
            });
        }
    }
}

jQuery.fn.ChartContainer = (chartContainer: JQuery):
    Report.ChartContainer => {
    if (!chartContainer) {
        throw new Error("Container is missed");
    }
    return new Report.ChartContainer(chartContainer);
};