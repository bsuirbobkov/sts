﻿module Report {
    export const enum ReportTypes {
        SupplierDistributionBySustainabilityLevelSpendAdjusted = 1,
        SupplierDistributionBySustainabilityLevelTotal = 2,
        SupplierScope1Scope2Emissions = 3,
        MonthlySupplierParticipationTrendSpendAdjusted = 4
    } 
}