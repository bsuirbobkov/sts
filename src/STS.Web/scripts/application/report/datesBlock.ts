﻿module Report {
    "use strict";

    export class DatesBlock{
        constructor(private select: JQuery, private block: JQuery) {
            this.bindEvent();
        }

        private selectHandler = () => {
            if (Reports.doesHaveDate(this.select.val())) {
                this.block.show();
            } else {
                this.block.hide();
            }
        }

        private bindEvent() {
            this.select.change(() => {
                this.selectHandler();
            });

            $(document).ready(() => {
                this.selectHandler();
            });
        }
    }
}

jQuery.fn.DatesBlock = (select: JQuery, block: JQuery):
    Report.DatesBlock => {
    if (!select || !block) {
        throw new Error("Element is missed");
    }
    return new Report.DatesBlock(select, block);
};