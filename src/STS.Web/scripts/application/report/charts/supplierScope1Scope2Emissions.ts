﻿module Report {
    "use strict";

    export class SupplierScope1Scope2Emissions {
        constructor(private chartContainer: JQuery, private data) {
            this.bindHighcharts();
        }

        private bindHighcharts(): void {
            const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];

            let categories = [];
            let suppliers = [];
            for (let month of this.data.series) {
                let date = new Date(month.monthYear);
                categories.push(monthNames[date.getMonth()] + " " + date.getFullYear());
                suppliers.push(month.supplierCount);
            }

            this.chartContainer.highcharts({
                chart: {
                    zoomType: "xy"
                },
                title: {
                    text: "Supplier Scope 1 and Scope 2 GHG Emissions"
                },
                xAxis: [{
                    categories: categories,
                    crosshair: true
                }],
                yAxis: [{
                        labels: {
                            format: "{value:,.0f} tons",
                            
                            style: {
                                color: Highcharts.getOptions().colors[2]
                            }
                        },
                        title: {
                            text: "Scope 1 and Scope 2 GHG emissions",
                            style: {
                                color: Highcharts.getOptions().colors[2]
                            }
                        }
                    },
                    { 
                        labels: {
                            format: "{value}",
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        title: {
                            text: "Number of suppliers",
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true
                    }
                ],
                plotOptions: {
                    spline: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: "Emissions",
                    type: "column",
                    yAxis: 0,
                    data: this.data.series,
                    tooltip: {
                        pointFormat: "Scope 1: <b>{point.scope1:,.0f} Tons</b> <br>" +
                                     "Scope 2: <b>{point.scope2:,.0f} Tons</b> <br>" +
                                     "Combined: <b>{point.y:,.0f} Tons</b> <br>"
                    }
                } as any,
                {
                    name: "Suppliers",
                    type: "spline",
                    yAxis: 1,
                    data: suppliers
                }
                ]
            }
            );
        }
    }
}