﻿module Report {
    "use strict";

    export class SupplierDistributionBySustainabilityLevelTotal {
        constructor(private chartContainer: JQuery, private data) {
            this.bindHighcharts();
        }

        private bindHighcharts(): void {
            this.chartContainer.highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: "pie"
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: "Supplier Distribution by Sustainability Level",
                    x: -20
                },
                subtitle: {
                    text: "Total",
                    x: -20
                },
                tooltip: {
                    pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b> <br>Number of suppliers: <b>{point.number}</b>"
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: "pointer",
                        dataLabels: {
                            enabled: true,
                            format: "<b>{point.name}</b>: {point.percentage:.1f}% ({point.number})",
                            style: {
                                color: "black"
                            }
                        }
                    }
                },
                series: [
                    {
                        name: "Percent",
                        data: this.data.series
                    }
                ]
            });
        }
    }
}