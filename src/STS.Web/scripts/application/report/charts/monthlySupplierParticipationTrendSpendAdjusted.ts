﻿module Report {
    "use strict";

    export class MonthlySupplierParticipationTrendSpendAdjusted {
        constructor(private chartContainer: JQuery, private data) {
            this.bindHighcharts();
        }

        private bindHighcharts(): void {
            const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];

            let categories = [];

            let notCompletedSurvey = [];
            let completedSurvey = [];
            let haveSubscription = [];
            let suppliers = [];
            for (let month of this.data.series) {
                let date = new Date(month.monthYear);
                categories.push(monthNames[date.getMonth()] + " " + date.getFullYear());

                notCompletedSurvey.push(month.notCompletedSurvey);
                completedSurvey.push(month.completedSurvey);
                haveSubscription.push(month.haveSubscription);
                suppliers.push(month.suppliers);
            }

            this.chartContainer.highcharts({
                chart: {
                    zoomType: "xy"
                },
                title: {
                    text: "Monthly Supplier Participation Trend"
                },
                subtitle: {
                    text: "Spend Ajusted",
                    x: -20
                },
                xAxis: {
                    categories: categories
                },
                yAxis: [{
                        max: 100,
                        alignTicks: false,
                        endOnTick: false,
                        title: {
                            text: "Total procurement expenditures",
                            style: {
                                color: Highcharts.getOptions().colors[2]
                            }
                        },
                        labels: {
                            format: "{value}%",
                            style: {
                                color: Highcharts.getOptions().colors[2]
                            }
                        }
                    },
                    {
                        title: {
                            text: "Number of suppliers associated with the company",
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: "{value}",
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true
                    }
                ],
                credits: {
                    enabled: false
                },
                plotOptions: {
                    spline: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                series: [{
                        name: "Not Completed Survey",
                        type: "column",
                        tooltip: {
                            valueSuffix: "%"
                        },
                        stacking: "normal",
                        yAxis: 0,
                        data: notCompletedSurvey,
                    } as any,
                    {
                        name: "Completed Survey",
                        type: "column",
                        tooltip: {
                            valueSuffix: "%"
                        },
                        stacking: "normal",
                        yAxis: 0,
                        data: completedSurvey,
                    } as any,
                    {
                        name: "Have Subscription",
                        type: "column",
                        tooltip: {
                            valueSuffix: "%"
                        },
                        stacking: "normal",
                        yAxis: 0,
                        data: haveSubscription,
                    } as any,
                    {
                        name: "Suppliers",
                        type: "spline",
                        yAxis: 1,
                        data: suppliers
                    }
                ]
            }
            );
        }
    }
}