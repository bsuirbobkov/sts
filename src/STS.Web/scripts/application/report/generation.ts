﻿module Report {
    "use strict";

    export class Generation {
        constructor(private generateButton: JQuery, private chartContainer: JQuery) {
            this.setupHighcharts();
            this.bindDefault();
            this.bindGeneration();
        }

        private reports = {
            1: (result) => {
                return new SupplierDistributionBySustainabilityLevelSpendAdjusted(this.chartContainer, result);
            },
            2: (result) => {
                return new SupplierDistributionBySustainabilityLevelTotal(this.chartContainer, result);
            },
            3: (result) => {
                return new SupplierScope1Scope2Emissions(this.chartContainer, result);
            },
            4: (result) => {
                return new MonthlySupplierParticipationTrendSpendAdjusted(this.chartContainer, result);
            }
        };

        private bindGeneration(): void {
            this.generateButton.click(() => {
                var frm = $(this.generateButton).closest("form");
                var data = frm.serialize();
                var url = frm.attr("action");
                $.post(url, data)
                    .done(result => {
                        this.chartContainer.empty();
                        var chart = this.reports[result.reportType](result);
                    })
                    .fail(result => {
                        this.addValidationMessages(JSON.parse(result.responseText));
                    });
            });
        }

        private bindDefault(): void {
            var chart = new ChartContainer(this.chartContainer);
        }

        private setupHighcharts(): void {
            Highcharts.getOptions().colors.splice(1, 1);
            Highcharts.setOptions({
                lang: {
                    thousandsSep: ","
                }
            });
        }

        private addValidationMessages(response) {
            if (response.startDateMessage) {
                $("[data-valmsg-for=StartDate]")
                    .html(response.startDateMessage)
                    .addClass("field-validation-error");
            }
            if (response.endDateMessage) {
                $("[data-valmsg-for=EndDate]")
                    .html(response.endDateMessage)
                    .addClass("field-validation-error");
            }
        }
    }
}

jQuery.fn.Generation = (generateButton: JQuery, chartContainer: JQuery):
    Report.Generation => {
    if (!generateButton || !chartContainer) {
        throw new Error("Element is missed");
    }
    return new Report.Generation(generateButton, chartContainer);
};