﻿module Report {
    "use strict";

    export class Reports {
        private static haveDate = [ReportTypes.MonthlySupplierParticipationTrendSpendAdjusted,
                            ReportTypes.SupplierScope1Scope2Emissions];
        private static dontHaveDate = [ReportTypes.SupplierDistributionBySustainabilityLevelTotal,
                                ReportTypes.SupplierDistributionBySustainabilityLevelSpendAdjusted];
        static doesHaveDate(id: number) : boolean {
            return this.haveDate.indexOf(+id) !== -1;
        }
    }
}