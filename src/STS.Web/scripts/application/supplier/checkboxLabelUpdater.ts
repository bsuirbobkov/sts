﻿module SupplierEdit {
    "use strict";
    export class CheckboxLabelUpdaterElement {
        constructor(private checkBox: JQuery, private label: JQuery, private trueText: string, private falseText: string) {
            this.onCheckBoxChange();
            this.checkBox.on("click",
                () => this.onCheckBoxChange()
            );
        }

        private onCheckBoxChange(): void {
            const isChecked = this.checkBox.is(":checked");
            this.label.html(isChecked? this.trueText : this.falseText);
        }
    }
}

jQuery.fn.CheckboxLabelUpdater = (elementId: string, trueText: string, falseText :string):
    SupplierEdit.CheckboxLabelUpdaterElement => {
    const element = $(`#${elementId}`);
        const label = $(`label[for=${elementId}]`);
    if (!element || !label) {
        throw new Error("Containers is missed");
    }
    return new SupplierEdit.CheckboxLabelUpdaterElement(element, label, trueText, falseText);
};