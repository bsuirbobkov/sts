﻿module SupplierEdit {
    "use strict";
    export class CompanyEditorForm {
        constructor(private choosenContainers: Array<JQuery>, private passwordContainers: Array<JQuery>) {
            this.bindChoosenStates();
            this.bindPasswordStates();
        }

        private bindChoosenStates(): void {
            if (this.choosenContainers && this.choosenContainers.length) {
                for (let i = 0; i < this.choosenContainers.length; i++) {
                    $(this.choosenContainers[i]).fixedChosen();
                }
            }
        }

        private bindPasswordStates(): void {
            if (this.passwordContainers && this.passwordContainers.length) {
                const form = $(this.passwordContainers[0]).closest("form");
                for (let i = 0; i < this.passwordContainers.length; i++) {
                    const input = $(this.passwordContainers[i]);
                    input.attr('type', 'password');
                    form.on("submit", () => {
                        if (form.valid()) {
                            input.attr('type', 'text');
                        }
                    });
                }
            }
        }
    }
}

jQuery.fn.CompanyEditorForm = (choosenContainers: Array<JQuery>, passwordContainers: Array<JQuery>):
    SupplierEdit.CompanyEditorForm => {
    if (!choosenContainers && !passwordContainers) {
        throw new Error("Containers is missed");
    }
    return new SupplierEdit.CompanyEditorForm(choosenContainers, passwordContainers);
};
