﻿module SupplierEdit {
    "use strict";

    export class DetachSupplierEditorForm {
        constructor(private targetButton: JQuery,
            private modalBox: JQuery, private targetLink: string, private redirectLink: string) {
            this.bindHandlers();
        }

        private bindHandlers(): void {
            this.onDetachClick();
            this.onDetachConfirmClick();
        }

        private isButtonActive(): boolean {
            return !this.targetButton.hasClass("disabled");
        }

        private onDetachClick(): void {
            if (!this.isButtonActive()) {
                return;
            }

            const dialogOptions: ModalOptions = {
                show: true
            };

            this.targetButton.on("click", () => {
                this.modalBox.modal(dialogOptions);
            });
        }

        private onDetachConfirmClick(): void {
            this.modalBox.find("#detach-confirm").on("click", () => {
                jQuery.post(this.targetLink).done(() => {
                    let wnd = window as any;
                    wnd.location = this.redirectLink as any;
                });
            });
        }
    }
}

jQuery.fn.DetachSupplierEditorForm = (targetButton: JQuery, modalBox: JQuery, targetLink: string, redirectLink: string):
    SupplierEdit.DetachSupplierEditorForm => {
    if (!targetButton) {
        throw new Error("Target button is missed");
    }

    if (!modalBox) {
        throw new Error("Modal box button is missed");
    } 

    if (!targetLink) {
        throw new Error("Target url is not specified");
    }

    if (!redirectLink) {
        throw new Error("Redirect link is not specified");
    }

    return new SupplierEdit.DetachSupplierEditorForm(targetButton, modalBox, targetLink, redirectLink);
};