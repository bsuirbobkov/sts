﻿module SupplierEdit {
    "use strict";
    export class SupplierEditorForm {
        constructor(private checkBox: JQuery, private inverseBehaviour: boolean) {
            this.onCheckBoxChange();
            this.checkBox.on("click",
                () => this.onCheckBoxChange()
            );
        }

        private onCheckBoxChange(): void {
            const isChecked = this.checkBox.is(":checked");
            const targetElement = $(this.checkBox.data("target"));
            if (isChecked === !this.inverseBehaviour) {
                targetElement.attr('disabled', 'disabled');
                targetElement.addClass('collapse');
            } else {
                targetElement.removeAttr('disabled');
                targetElement.removeClass('collapse');
            }
        }
    }
}

jQuery.fn.SupplierEditorForm = (checkBox: JQuery, inverseBehaviour: boolean = null):
    SupplierEdit.SupplierEditorForm => {
    if (!checkBox) {
        throw new Error("Containers is missed");
    }
    return new SupplierEdit.SupplierEditorForm(checkBox, inverseBehaviour);
};