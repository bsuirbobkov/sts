﻿var allValue = "ALL";
var searchCodeInput = ".code-search";
var codeGettingUrl = null;
var codeSearchingUrl = null;
var codeDetailsUrl = null;
var dableCode = {};
var dableCodeRevers = {};
var size = 5;

dableCode["31"] = [32, 33];
dableCode["44"] = [45];
dableCode["48"] = [49];

dableCodeRevers[32] = 31;
dableCodeRevers[33] = 31;
dableCodeRevers[45] = 44;
dableCodeRevers[49] = 48;


var codeBox = null;
var upperLevelCodes = null;
var autocompleteOpened = false;

class CodeItem {
    code: string;
    title: string;
    constructor(code: string, title: string) {
        this.code = code;
        this.title = title;
    }
}

function getCurrentCode(elem) {
    return $(elem).val();
};

function validSetCode(code, elem, callback: any = null) {
    getCode(code, (d,c) => {
        if (d) {
            $(elem).parent().parent().children(".code-name").html(d.title);
        }
        disableFields();
    });
}

function disableFields() {
    var inputs = $(searchCodeInput).toArray();
    for (let i = 1; i <= size; i++) {
        if (!checkValuesPresence(inputs, i)) {
            $(inputs[i]).attr("disabled", "disabled");
        } else {
            $(inputs[i]).removeAttr("disabled");
        }
    }
}

function checkValuesPresence(inputs, index) {
    for (let i = index - 1; i <= size; i++) {
        if (inputs[i].value) {
            return true;
        }
    }
    return false;
}

function getCodes(code, callback) {
    let id = "";
    if (code)
        id = code;

    $.ajax({
        dataType: "json",
        cache: false,
        url: codeGettingUrl,
        data: { code: id },
        success(data) {
            var codes = {};
            $.each(data, (index, item) => {
                codes[item.code] = new CodeItem(item.code, item.title);
                if (dableCode[item.code]) {
                    $(dableCode[item.code]).each((i, c) => {
                        codes[c as any] = new CodeItem(item.code, item.title);
                    });
                }
            });
            if (callback) {
                callback(codes);
            }
        }
    });
};


function getCode(code, callback) {
    let id = "";
    if (code)
        id = code;
    $.ajax({
        dataType: "json",
        cache: false,
        url: codeDetailsUrl,
        data: { code: id },
        success(data) {
            if (callback && (data || code === "")) {
                callback(data, code);
            }
        }
    });
};


$.initCodeSelection = (url, searchUrl, detailsUrl) => {
    codeGettingUrl = url;
    codeSearchingUrl = searchUrl;
    codeDetailsUrl = detailsUrl;
};

function setupCodeSearch() {
    codeBox = $(searchCodeInput);
    codeBox.addClass("default");
    $(codeBox).keydown(function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            validSetCode($(this).val(), this);
        }
    }).focusin(function () {
        const element = $(this);
        element.data("previous", element.val());
    }).focusout(function () {
        const element = $(this);
        if (element.val() !== element.data("previous")) {
            validSetCode($(this).val(), this);
        }
    }).autocomplete({
        source(input, callback) {
            $.ajax({
                dataType: "json",
                url: codeSearchingUrl,
                cache: false,
                data: { term: input.term },
                success(data) {
                    var result = [];
                    $.each(data, function () {
                        result.push({ label: this.code + " | " + this.title, value: this.code });
                    });
                    callback(result);
                }
            });
        },
        select(event, ui) {
            validSetCode(ui.item.value, this);
        },
        open() {
            autocompleteOpened = true;
        },
        close() {
            autocompleteOpened = false;
        }
    });
};

$.fn.codeInput = function () {
    var codeInputs = this;
    for (let inp of codeInputs) {
        validSetCode(getCurrentCode(inp), inp);
        $(inp).change(() => {
            if (!$(inp).val()) {
                $(inp).parent().parent().children(".code-name").html("");
            }
        });
    }
};

$(document).ready(() => {
    setupCodeSearch();
});
