$.fn.extend({
    fixedChosen: function () {
        return this.each(function () {
            $(this).chosen();
            $(this).data("chosen").__proto__.result_do_highlight = function (el) {
                if (el.length) {
                    this.result_clear_highlight();
                    this.result_highlight = el;
                    this.result_highlight.addClass("highlighted");
                }
            };
        });
    }
});