﻿module Grid {
    import PopoverBehaviour = UiBehavior.PopoverBehaviour;

    export class DocumentsAjaxGrid<TFilter extends IFilter> extends AjaxGrid<TFilter> {
        private behaviour: PopoverBehaviour;

        constructor(container: JQuery, filter: TFilter, uploadCallback?: Function) {
            super(container, filter, uploadCallback);

            if (!uploadCallback) {
                this.uploadCallback = this.initDescriptions;
                this.initDescriptions();
            }
        }

        private initDescriptions(): void {
            if (!this.behaviour) {
                this.behaviour = new PopoverBehaviour();
                this.behaviour.setupElementBehaviour(jQuery('[data-toggle="popover"]'));
                this.behaviour.setupBodyBehaviour();
            } else {
                this.behaviour.setupElementBehaviour(jQuery('[data-toggle="popover"]'));
            }
        }
    }
}

jQuery.fn.DocumentsAjaxGrid = (container: JQuery, filter: Grid.IFilter, uploadCallback?: Function): Grid.DocumentsAjaxGrid<Grid.IFilter> => {
    if (!container) {
        throw new Error("Container is missed");
    }

    if (!filter) {
        throw new Error("Filter is missed");
    }

    return new Grid.DocumentsAjaxGrid(jQuery(container), filter, uploadCallback);
}
