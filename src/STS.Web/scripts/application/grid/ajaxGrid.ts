﻿module Grid {
    export interface IFilter {
        sortColumns: string;
        sortAscending: boolean;
        currentPage: number;
        rowsPerPage: number;
    }

    export class AjaxGrid<TFilter extends IFilter> {
        protected container: JQuery;
        protected filter: TFilter;
        protected sourceLink: string;
        protected isInited: boolean = false;
        protected uploadCallback: Function;

        constructor(container: JQuery, filter: TFilter, uploadCallback?: Function) {
            this.throwIfArgumentsInvalid(container, filter);
            this.sourceLink = container.data("source-link");
            this.container = container;
            this.filter = filter;
            this.bindAll();
            this.isInited = true;
            this.uploadCallback = uploadCallback;
        }

        protected throwIfArgumentsInvalid(container: JQuery, filter: TFilter) {
            if (!container || container.length === 0) {
                throw new Error("Container is missed");
            }

            if (!filter) {
                throw new Error("Filter is missed");
            }
        }

        protected bindAll() {
            this.bindPager();
            this.bindPageSizer();
            this.bindSorter();
        }

        protected bindPager(): void {
            this.container.find("ul > li > a").on("click", (event: JQueryEventObject) => {
                var page = jQuery(event.target).data("page") as number;
                if (page) {
                    this.filter.currentPage = page;
                    this.reload();
                }
            });
        }

        protected bindPageSizer(): void {
            const dropdown = this.isInited
                ? this.container.find("select").chosen()
                : this.container.find("select");

            dropdown.on("change", (event: JQueryEventObject) => {
                var size = jQuery(event.target).val() as number;
                if (size) {
                    this.filter.rowsPerPage = size;
                    this.filter.currentPage = 1;
                    this.reload();
                }
            });
        }

        protected bindSorter(): void {
            const chooseDirection = (order: string): boolean => {
                if (order.toLocaleUpperCase() === "TRUE") {
                    return false;
                } else {
                    return true;
                }
            };

            this.container.find("[data-sort=True]").on("click", (event: JQueryEventObject) => {
                var column = jQuery(event.currentTarget);

                this.filter.sortColumns = column.data("name") as string;
                this.filter.sortAscending = chooseDirection(column.data("sort-ascending") as string);

                this.reload();
            });
        }

        protected reload(): void {
            jQuery.ajax({
                url: this.sourceLink,
                data: this.getRequestData(),
                cache: false,
                success: (html: string) => {
                    this.container.html(html);
                    this.bindAll();
                    if (this.uploadCallback) {
                        this.uploadCallback();
                    }
                }
            });
        }

        protected getRequestData(): any {
            return this.filter;
        }
    }
}

jQuery.fn.AjaxGrid = (container: JQuery, filter: Grid.IFilter, uploadCallback?: Function): Grid.AjaxGrid<Grid.IFilter> => {
    if (!container) {
        throw new Error("Container is missed");
    }

    if (!filter) {
        throw new Error("Filter is missed");
    }

    return new Grid.AjaxGrid(jQuery(container), filter, uploadCallback);
}
