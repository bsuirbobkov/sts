module STS.Survey {
    function isAtOwnDomain(surveyIframeId: string, document: Document): boolean {
            try {
                const frame = document.getElementById(surveyIframeId) as HTMLFrameElement;
                const frameContentSource = frame.contentWindow.location.href;
                return true;
            } catch (exception) {
                return false;
            };
        }

    export function onChanged(surveyIframeId: string, redirectLink: string, document: Document) {
        if (isAtOwnDomain(surveyIframeId, document)) {
            document.location.href = redirectLink;
        }
    }
}