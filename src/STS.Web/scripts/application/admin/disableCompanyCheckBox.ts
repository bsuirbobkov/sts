﻿module Admin {
    "use strict";
    export class DisableCompanyCheckBox {
        constructor(private checkBox: JQuery, private label: JQuery) {
            this.init();
            this.checkBox.on("click",
                () => this.onCheckBoxChange()
            );
        }

        private onCheckBoxChange(): void {
            const isChecked = this.checkBox.is(":checked");
            const disable = !isChecked;
            const url = `DisableCompany/?companyId=${this.checkBox.attr("company-id")}&disable=${disable}`;

            jQuery.ajax({
                url: url,
                cache: false,
                success: () => {
                    this.label.html(isChecked ? "Enabled" : "Disabled");
                },
                error: () => {
                    this.checkBox.prop("checked", !isChecked);
                    this.label.html(!isChecked ? "Enabled" : "Disabled");
                }
            });    
        }

        private init(): void {
            const isChecked = this.checkBox.is(":checked");
            this.label.html(isChecked ? "Enabled" : "Disabled");
        }
    }
}

jQuery.fn.DisableCompanyCheckBox = (elementId: JQuery):
    Admin.DisableCompanyCheckBox => {
    const element = $(`#${elementId}`);
    const label = $(`label[for=${elementId}]`);
    if (!element || !label) {
        throw new Error("Containers is missed");
    }
    return new Admin.DisableCompanyCheckBox(element, label);
};