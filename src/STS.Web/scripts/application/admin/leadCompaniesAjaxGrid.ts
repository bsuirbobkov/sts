﻿module Admin {
    import IFilter = Grid.IFilter;

    export class LeadCompaniesAjaxGrid extends Grid.AjaxGrid<IFilter> {
        constructor(container: JQuery, filter: IFilter) {
            super(container, filter);
        }

        private bindCheckBoxes() {
            for (let x of $(".disablingButton[company-id]").toArray()) {
                $.fn.DisableCompanyCheckBox(x.id);
            };
        }

        protected bindAll() {
            super.bindAll();
            this.bindCheckBoxes();
        }
    }
}

jQuery.fn.LeadCompaniesAjaxGrid = (container: JQuery, filter: Admin.ISuppliersFilter): Admin.LeadCompaniesAjaxGrid => {
    if (!container) {
        throw new Error("Container is missed");
    }

    if (!filter) {
        throw new Error("Filter is missed");
    }

    return new Admin.LeadCompaniesAjaxGrid(jQuery(container), filter);
}
