﻿module Admin {
    export interface ISuppliersFilter extends Grid.IFilter {
        leadCompanyId: number;
    }

    export class SuppliersFilterableAjaxGrid extends Grid.AjaxGrid<ISuppliersFilter> {
        private leadCompanyFilter: JQuery;

        constructor(container: JQuery, private filterContainer: JQuery, filter: ISuppliersFilter) {
            super(container, filter);
            this.leadCompanyFilter = this.filterContainer.find("select").fixedChosen();
            this.leadCompanyFilter.on("change", this.onLeadCompanyChanged);
        }

        protected getRequestData(): any {
            const requestData = {
                "filter.currentPage": this.filter.currentPage,
                "filter.rowsPerPage": this.filter.rowsPerPage,
                "filter.sortAscending": this.filter.sortAscending,
                "filter.sortColumns": this.filter.sortColumns,
                "filter.leadCompanyId": this.filter.leadCompanyId
            };

            return requestData;
        }

        private onLeadCompanyChanged = (): void => {
            const value = this.leadCompanyFilter.val();
            this.filter.leadCompanyId = parseInt(value);
            this.reload();
        }

        private bindCheckBoxes() {
            for (let x of $(".disablingButton[company-id]").toArray()) {
                $.fn.DisableCompanyCheckBox(x.id);
            };
        }

        protected bindAll() {
            super.bindAll();
            this.bindCheckBoxes();
        }
    }
}

jQuery.fn.SuppliersFilterableAjaxGrid = (container: JQuery, filterContainer: JQuery, filter: Admin.ISuppliersFilter): Admin.SuppliersFilterableAjaxGrid => {
    if (!container) {
        throw new Error("Container is missed");
    }

    if (!filterContainer) {
        throw new Error("Filter Container is missed");
    }

    if (!filter) {
        throw new Error("Filter is missed");
    }

    return new Admin.SuppliersFilterableAjaxGrid(jQuery(container), filterContainer, filter);
}
