﻿import report = Report.MonthlySupplierParticipationTrendSpendAdjusted;
import reportTypes = Report.ReportTypes;

module Lead {
    "use strict";

    export class Dashbord {
        static setReport(container: JQuery) {
            this.setupHighcharts();
            const data = this.getData(reportTypes.MonthlySupplierParticipationTrendSpendAdjusted);
            $.post("LeadCompany/Reports", data)
                .done(result => {
                    let chart = new report(container, result);
                });
        }

        static getData(reportType: number) {
            const end = new Date();
            const start = new Date(end.getFullYear(), end.getMonth() - 24);
            return {
                endDate: this.getDateString(end),
                startDate: this.getDateString(start),
                reportType: reportType
            };
        }

        static getDateString(date: Date): string {
            return (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
        }

        static setupHighcharts(): void {
            Highcharts.getOptions().colors.splice(1, 1);
            Highcharts.setOptions({
                lang: {
                    thousandsSep: ","
                }
            });
        }
    }
}

jQuery.fn.SetDashbordReport = (container: JQuery) => {
    if (!container) {
        throw new Error("Container is missed");
    }
    Lead.Dashbord.setReport(container);
};