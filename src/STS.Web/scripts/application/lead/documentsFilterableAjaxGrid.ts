﻿module Lead {
    export interface IDocumentsFilter extends Grid.IFilter {
        documentTypeId: number;
    }

    export class DocumentsFilterableAjaxGrid extends Grid.DocumentsAjaxGrid<IDocumentsFilter> {
        private documentTypeFilter: JQuery;

        constructor(container: JQuery, private filterContainer: JQuery, filter: IDocumentsFilter, private companyId: number) {
            super(container, filter);
            this.documentTypeFilter = this.filterContainer.find("select").chosen();
            this.documentTypeFilter.on("change", this.onDocumentTypeChanged);
        }

        protected getRequestData(): any {
            const requestData = {
                "filter.currentPage": this.filter.currentPage,
                "filter.rowsPerPage": this.filter.rowsPerPage,
                "filter.sortAscending": this.filter.sortAscending,
                "filter.sortColumns": this.filter.sortColumns,
                "filter.documentTypeId": this.filter.documentTypeId,
                companyId: this.companyId
            };

            return requestData;
        }

        private onDocumentTypeChanged = (): void => {
            const value = this.documentTypeFilter.val();
            this.filter.documentTypeId = parseInt(value);
            this.reload();
        }
    }
}

jQuery.fn.DocumentsFilterableAjaxGrid = (container: JQuery, filterContainer: JQuery, filter: Lead.IDocumentsFilter, supplierId: number): Lead.DocumentsFilterableAjaxGrid => {
    if (!container) {
        throw new Error("Container is missed");
    }

    if (!filterContainer) {
        throw new Error("Filter Container is missed");
    }

    if (!filter) {
        throw new Error("Filter is missed");
    }

    return new Lead.DocumentsFilterableAjaxGrid(jQuery(container), filterContainer, filter, supplierId);
}
