/// <binding BeforeBuild='clean:js' AfterBuild='compile, sass, copylibraries' Clean='clean:js' ProjectOpened='default' />
var gulp = require('gulp'),
    tsc = require('gulp-typescript'),
    sourcemaps = require('gulp-sourcemaps'),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    merge = require('merge2'),
    uglify = require("gulp-uglify"),
    tsProject = tsc.createProject('tsconfig.json'),
    sass = require("gulp-sass");

var paths = {
    webroot: "./wwwroot/",
    clientroot: "./scripts/"
};

var project = require('./project.json');

// webroot paths
paths.tsOutputPath = paths.webroot + '/js';
paths.libraryOutputPath = paths.webroot + '/lib';
paths.js = paths.webroot + "js/**/*.js";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.concatJsDest = paths.webroot + "js/site.min.js";

// client-side paths
paths.allTypeScript = paths.clientroot + '/**/*.{ts,tsx}';
paths.library = paths.clientroot + 'lib/**';
paths.typings = 'typings/';
paths.libraryTypeScriptDefinitions = 'typings/**/*.ts';
paths.styles = './styles/**/*.*';

gulp.task('compile', function () {
    var sourceTsFiles = [paths.allTypeScript,                //path to typescript files
                         paths.libraryTypeScriptDefinitions]; //reference to library .d.ts files


    var tsResult = gulp.src(sourceTsFiles)
                       .pipe(sourcemaps.init())
                       .pipe(tsc(tsProject));


    return merge([ // Merge the two output streams, so this task is finished when the IO of both operations are done.
        tsResult.dts.pipe(gulp.dest(paths.tsOutputPath)),
        tsResult.js.pipe(sourcemaps.write('.')).pipe(gulp.dest(paths.tsOutputPath))
    ]);
});

gulp.task("sass", function () {
    gulp.src("styles/*.css")
        .pipe(gulp.dest(paths.webroot + '/css'));
    return gulp.src('styles/main.scss')
        .pipe(sass())
        .pipe(gulp.dest(paths.webroot + '/css'));
});

gulp.task("clean:js", function (cb) {
    rimraf(paths.concatJsDest, cb);
});
/*

gulp.task("min:js", function () {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});*/

gulp.task('copylibraries', function () {
    return gulp.src(paths.library)
        .pipe(gulp.dest(paths.libraryOutputPath));
});

gulp.task('default', ['copylibraries', 'compile', 'sass'], function () {
    gulp.watch(paths.allTypeScript, ['compile']);
    gulp.watch(paths.library, ['copylibraries']);
    gulp.watch(paths.styles, ['sass']);
});

gulp.task('runall', ['copylibraries', 'compile', 'sass'], function () {
});
