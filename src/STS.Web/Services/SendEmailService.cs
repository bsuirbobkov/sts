﻿using System.Collections.Generic;
using RazorEngine;
using RazorEngine.Templating;
using STS.Application.Models.EmailTemplates;
using STS.Application.Services.Interfaces;
using STS.CrossCutting.Constans;

namespace STS.Web.Services
{
    public class SendEmailService: ISendEmailService
    {
        private static DynamicViewBag GetDynamicViewBag()
        {
            var viewBag = new DynamicViewBag();
            var dictionary = new Dictionary<string, object>
            {
                {
                    "baseUrl",
                    LinkConstant.PathBase
                }
            };
            viewBag.AddDictionary(dictionary);
            return viewBag;
        }

        public string RenderSignUpTemplate(SignUpModel model)
        {
            return Engine.Razor.RunCompile(WebConstant.SignUpTemplate, model.GetType(), model, GetDynamicViewBag());
        }

        public string RenderPasswordRestoreTemplate(LinkModel model)
        {
            return Engine.Razor.RunCompile(WebConstant.PasswordRestoreTemplate, model.GetType(), model, GetDynamicViewBag());
        }

        public string RenderChangeLevelCompanyTemplate(ChangeLevelModel model)
        {
            return Engine.Razor.RunCompile(WebConstant.ChangeLevelCompanyTemplate, model.GetType(), model, GetDynamicViewBag());
        }

        public string RenderChangeAdminLevelTemplate(ChangeLevelModel model)
        {
            return Engine.Razor.RunCompile(WebConstant.ChangeLevelAdminTemplate, model.GetType(), model, GetDynamicViewBag());
        }
    }
}