﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Autofac;
using Hangfire;
using Hangfire.Dashboard;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using STS.Application.Identity;
using STS.Application.MapperProfiles;
using STS.Container;
using STS.Web.ActionFilter;
using STS.Web.Infrastructure;
using STS.Web.Binders;

namespace STS.Web
{
    public class Startup
    {
        private readonly CultureInfo defaultRequestCulture = CultureInfo.GetCultureInfo("en-US");
        public IContainer ApplicationContainer { get; private set; }
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
            AutoMapperConfiguration.Configure();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(config =>
            {
                config.ModelBinderProviders.Insert(0, new TodoModelBinderProvider());
            });

            services.AddOptions();

            services.Configure<MvcOptions>(options =>
            {
                var noCache = new CacheProfile
                {
                    Duration = 0,
                    Location = ResponseCacheLocation.None,
                    NoStore = true,
                    VaryByHeader = ""
                };
                options.CacheProfiles.Add(new KeyValuePair<string, CacheProfile>("NoChache", noCache));
            });

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequiredLength = 6;
                })
                .AddRoleStore<ApplicationRoleStore>()
                .AddUserStore<ApplicationUserStore>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Cookies.ApplicationCookie.LoginPath =
                    new PathString(Configuration.GetSection("AuthenticationSettings")["LoginUrl"]);
            });

            services.AddHangfire(config =>
            {
                var hangfire = HangfireAutofacConfiguration.Register(services, Configuration, x => x.RegisterModule<WebAutofacModule>());
                config.UseSqlServerStorage(Configuration.GetConnectionString("Hangfire"));
                var lifetimeScope = hangfire.Build();
                config.UseActivator(new AutofacJobActivator(lifetimeScope));
            });

            ConfigurationConfig.Register(services, Configuration);

            var builder = AutofacConfiguration.Register(services, Configuration, b => b.RegisterModule<WebAutofacModule>());
            ApplicationContainer = builder.Build();

            var serviceProvider = ApplicationContainer.Resolve<IServiceProvider>();
            RazorEngineConfig.Configure(serviceProvider.GetService<IHostingEnvironment>());

            return serviceProvider;
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            CultureInfo.DefaultThreadCurrentCulture = defaultRequestCulture;
            CultureInfo.DefaultThreadCurrentUICulture = defaultRequestCulture;

            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                var dashboardOptions = new DashboardOptions
                {
                    Authorization = new List<IDashboardAuthorizationFilter>
                    {
                        new AllowedAllAuthenticatedFilter()
                    }
                };
                app.UseHangfireDashboard("/hangfire/dashboard", dashboardOptions);
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseIdentity();

            app.UseStaticFiles();
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                LoginPath = new PathString("/Account/SignIn"),
                LogoutPath = new PathString("/Account/LogOff"),
                AccessDeniedPath = new PathString("/Error/AccessDenied"),
                Events = new CookieAuthenticationEvents
                {
                    OnValidatePrincipal = context => Task.FromResult(0) as Task
                },
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseHangfire(Configuration);
        }
    }
}
