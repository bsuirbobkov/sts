﻿using System;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using STS.Application.Services.Interfaces;

namespace STS.Web
{
    public static class HangfireMiddlewareConfig
    {
        private const int AttemptsCount = 3;
        private static readonly TimeSpan Interval = TimeSpan.FromMinutes(5);
        private static readonly int WorkerCount = Environment.ProcessorCount * 2;

        private const string DeterminateCategoryLevel = "DeterminateSustainabilityLevel";
        private const string HangfireServerName = "STS_Hangfire_Server";

        public static void UseHangfire(this IApplicationBuilder app, IConfiguration configuration)
        {
            var filters = GlobalJobFilters.Filters;
            filters.Add(new AutomaticRetryAttribute { Attempts = AttemptsCount });

            var options = new BackgroundJobServerOptions
            {
                SchedulePollingInterval = Interval,
                WorkerCount = WorkerCount,
                ServerName = HangfireServerName,
                FilterProvider = filters
            };

            app.UseHangfireServer(options);
            InitDefaultJobs(configuration);
        }

        private static void InitDefaultJobs(IConfiguration configuration)
        {
            var cron = configuration.GetSection("HangfireSettings")["CronTime"];
            RecurringJob.AddOrUpdate<ISchedulerService>(DeterminateCategoryLevel,
                x => x.DeterminateSustainabilityLevel(), cron);
        }
    }
}
