﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using RazorEngine;
using RazorEngine.Templating;
using STS.CrossCutting.Constans;

namespace STS.Web
{
    public class RazorEngineConfig
    {

        public static void Configure(IHostingEnvironment hostingEnvironment)
        {
            Func<string, string> getTemplate = x => File.ReadAllText(hostingEnvironment.ContentRootPath + $@"/Views/EmailTemplates/{x}.cshtml");
            Action<string> addTemplate = x => Engine.Razor.AddTemplate(x, getTemplate(x));

            addTemplate(WebConstant.EmailTemplate);
            addTemplate(WebConstant.SignUpTemplate);

            addTemplate(WebConstant.PasswordRestoreTemplate);
            addTemplate(WebConstant.ChangeLevelCompanyTemplate);
            addTemplate(WebConstant.ChangeLevelAdminTemplate);
        }
    }
}