﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using STS.CrossCutting.Models.Configuration;

namespace STS.Web
{
    public class ConfigurationConfig
    {
        public static void Register(IServiceCollection services, IConfigurationRoot configurationRoot)
        {
            services.Configure<AppSettings>(configurationRoot.GetSection(nameof(AppSettings)));
            services.Configure<AuthenticationSettings>(configurationRoot.GetSection(nameof(AuthenticationSettings)));
            services.Configure<EmailSettings>(configurationRoot.GetSection(nameof(EmailSettings)));
            services.Configure<RelativePathSettings>(configurationRoot.GetSection(nameof(RelativePathSettings)));
            services.Configure<FileManagementSettings>(configurationRoot.GetSection(nameof(FileManagementSettings)));

            services.AddSingleton<IConfiguration>(configurationRoot);
        }
    }
}