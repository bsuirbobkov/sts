﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace STS.Web.Binders
{
    public class TodoModelBinder : IModelBinder
    {
        private readonly IModelMetadataProvider provider;
        private readonly Dictionary<string, Type> types;
        private readonly Dictionary<string, IModelBinder> binders;

        public TodoModelBinder(
            IModelMetadataProvider provider,
            IDictionary<string, Type> types,
            IDictionary<string, IModelBinder> binders)
        {
            this.provider = provider;
            this.types = types.ToDictionary(item => item.Key, item => item.Value,
                StringComparer.InvariantCultureIgnoreCase);

            this.binders = binders.ToDictionary(item => item.Key, item => item.Value,
                StringComparer.InvariantCultureIgnoreCase);
        }

        public async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            // actual: categories[0].Todos[0]
            // expected: categories[0][Todos][0][todoType]
            var actualKey = bindingContext.ModelName;
            var expectedKey = actualKey.Replace(".Todos", "[todos]");
            expectedKey = expectedKey + "[todoType]";

            var typeResult = bindingContext.ValueProvider.GetValue(expectedKey);

            if (typeResult == ValueProviderResult.None)
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return;
            }

            var todoName = typeResult.FirstValue;
            var type = types[todoName];
            if (type == null)
            {
                bindingContext.Result = ModelBindingResult.Failed();
                return;
            }

            await BindTodoModelAsync(bindingContext, todoName, type);
        }

        private async Task BindTodoModelAsync(ModelBindingContext bindingContext, string todoName, Type type)
        {
            var binder = binders[todoName];
            var metadata = provider.GetMetadataForType(type);
            var model = Activator.CreateInstance(type);

            using (bindingContext.EnterNestedScope(metadata, bindingContext.FieldName, bindingContext.ModelName, model))
            {
                await binder.BindModelAsync(bindingContext);
            }

            bindingContext.Result = ModelBindingResult.Success(model);
        }
    }
}