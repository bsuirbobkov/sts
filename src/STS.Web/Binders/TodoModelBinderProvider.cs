﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using STS.Application.Models.Todo;
using STS.CrossCutting.Attributes;

namespace STS.Web.Binders
{
    public class TodoModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (context.Metadata.ModelType == typeof(BaseTodoModel))
            {
                var attributes = GetTodoModelAttributes();
                var types = GetTodoModelTypes(attributes);
                var binders = GetTodoModelBinders(context, attributes);
                return new TodoModelBinder(context.MetadataProvider, types, binders);
            }

            return null;
        }

        private static IList<TodoTypeAttribute> GetTodoModelAttributes()
        {
            var baseTodoType = typeof(BaseTodoModel);
            var result = baseTodoType.Assembly.GetTypes()
                .Where(type => IsDirivedFrom(baseTodoType, type))
                .Select(GetTodoTypeAttribute)
                .ToList();

            return result;
        }

        private static bool IsDirivedFrom(Type parentType, Type childType)
        {
            return parentType.IsAssignableFrom(childType) && parentType != childType;
        }

        private static TodoTypeAttribute GetTodoTypeAttribute(Type type)
        {
            var attribute = type.GetCustomAttributes(typeof(TodoTypeAttribute), false).FirstOrDefault();
            return attribute as TodoTypeAttribute;
        }

        private static IDictionary<string, Type> GetTodoModelTypes(
            IEnumerable<TodoTypeAttribute> attributes)
        {
            var result = attributes.ToDictionary(
                    item => item.TodoName,
                    item => item.TodoType);

            return result;
        }

        private static IDictionary<string, IModelBinder> GetTodoModelBinders(
            ModelBinderProviderContext context,
            IEnumerable<TodoTypeAttribute> attributes)
        {
            var result = new Dictionary<string, IModelBinder>();
            foreach (var attribute in attributes)
            {
                var metadata = context.MetadataProvider.GetMetadataForType(attribute.TodoType);
                var binder = context.CreateBinder(metadata);
                result.Add(attribute.TodoName, binder);
            }

            return result;
        }
    }
}