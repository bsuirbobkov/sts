﻿using Microsoft.AspNetCore.Mvc;
using STS.Application.Models.ManageSustainability;
using STS.Application.Services.Interfaces;

namespace STS.Web.Components
{
    public class AdminFilterViewComponent : ViewComponent
    {
        private readonly IAdminService adminService;

        public AdminFilterViewComponent(IAdminService adminService)
        {
            this.adminService = adminService;
        }

        public IViewComponentResult Invoke(int companyId)
        {
            var viewModel = new SustainabilityFilterModel
            {
                CompanyId = companyId,
                Suppliers = adminService.GetCompanies()
            };
            return View(viewModel);
        }
    }
}