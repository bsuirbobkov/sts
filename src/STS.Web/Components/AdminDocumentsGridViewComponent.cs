﻿using Microsoft.AspNetCore.Mvc;
using STS.Application.Models.ManageSustainability;
using STS.Application.Services.Interfaces;

namespace STS.Web.Components
{
    public class AdminDocumentsGridViewComponent : ViewComponent
    {
        private readonly ICompanyService companyService;

        public AdminDocumentsGridViewComponent(ICompanyService companyService)
        {
            this.companyService = companyService;
        }

        public IViewComponentResult Invoke(DocumentsFilter filter, int companyId)
        {
            var model = companyService.FilterDocuments(filter, companyId);
            return View(model);
        }
    }
}