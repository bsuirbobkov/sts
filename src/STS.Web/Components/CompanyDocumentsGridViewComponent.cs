﻿using Microsoft.AspNetCore.Mvc;
using STS.Application.Filters;
using STS.Application.Services.Interfaces;

namespace STS.Web.Components
{
    public class CompanyDocumentsGridViewComponent : ViewComponent
    {
        private readonly ICompanyService companyService;

        public CompanyDocumentsGridViewComponent(ICompanyService companyService)
        {
            this.companyService = companyService;
        }

        public IViewComponentResult Invoke(IFilter filter, int companyId)
        {
            var filterResult = companyService.FilterDocuments(filter, companyId);
            return View(filterResult);
        }
    }
}