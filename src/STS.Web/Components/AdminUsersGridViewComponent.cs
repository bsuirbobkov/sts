﻿using Microsoft.AspNetCore.Mvc;
using STS.Application.Filters;
using STS.Application.Services.Interfaces;

namespace STS.Web.Components
{
    public class AdminUsersGridViewComponent : ViewComponent
    {
        private readonly IAdminService adminService;

        public AdminUsersGridViewComponent(IAdminService adminService)
        {
            this.adminService = adminService;
        }

        public IViewComponentResult Invoke(Filter filter)
        {
            var filterResult = adminService.FilterUsers(filter);
            return View(filterResult);
        }
    }
}