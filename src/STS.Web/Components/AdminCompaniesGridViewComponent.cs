﻿using Microsoft.AspNetCore.Mvc;
using STS.Application.Filters;
using STS.Application.Services.Interfaces;

namespace STS.Web.Components
{
    public class AdminCompaniesGridViewComponent : ViewComponent
    {
        private readonly IAdminService adminService;

        public AdminCompaniesGridViewComponent(IAdminService adminService)
        {
            this.adminService = adminService;
        }

        public IViewComponentResult Invoke(IFilter filter)
        {
            var filterResult = adminService.FilterCompanies(filter);
            return View(filterResult);
        }
    }
}
