﻿using Microsoft.AspNetCore.Mvc;
using STS.Application.Filters;
using STS.Application.Services.Interfaces;

namespace STS.Web.Components
{
    public class AdminQuestionsGridViewComponent : ViewComponent
    {
        private readonly IAdminService adminService;

        public AdminQuestionsGridViewComponent(IAdminService adminService)
        {
            this.adminService = adminService;
        }

        public IViewComponentResult Invoke(IFilter filter)
        {
            var filterResult = adminService.FilterQuestions(filter);
            return View(filterResult);
        }
    }
}