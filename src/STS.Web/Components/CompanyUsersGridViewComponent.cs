﻿using Microsoft.AspNetCore.Mvc;
using STS.Application.Filters;
using STS.Application.Services.Interfaces;

namespace STS.Web.Components
{
    public class CompanyUsersGridViewComponent : ViewComponent
    {
        private readonly ICompanyService companyService;

        public CompanyUsersGridViewComponent(ICompanyService companyService)
        {
            this.companyService = companyService;
        }

        public IViewComponentResult Invoke(IFilter filter, int companyId)
        {
            var filterResult = companyService.FilterUsers(filter, companyId);
            return View(filterResult);
        }
    }
}