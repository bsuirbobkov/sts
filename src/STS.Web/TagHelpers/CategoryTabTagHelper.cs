﻿using System;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using STS.Application.Models.Sustainability;
using STS.Web.Models;

namespace STS.Web.TagHelpers
{
    [HtmlTargetElement("category-tab",
        Attributes = "controller, action, item, category-id")]
    public class CategoryTabTagHelper : TagHelper
    {
        [HtmlAttributeName("controller")]
        public string Controller { get; set; }

        [HtmlAttributeName("action")]
        public string Action { get; set; }

        [HtmlAttributeName("item")]
        public CategoryItemViewModel Item { get; set; }

        [HtmlAttributeName("category-id")]
        public int? CategoryId { get; set; }

        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public IUrlHelperFactory UrlHelper { get; set; }

        public CategoryTabTagHelper(IUrlHelperFactory urlHelper)
        {
            UrlHelper = urlHelper;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var viewModel = GetUnderlineContent();

            output.TagName = "a";
            output.Attributes.Add("href", viewModel.Href);
            output.Attributes.Add("class", $"c-levels-item {viewModel.LinkClass} {viewModel.LinkActiveClass}");

            var icon = GetIcon();
            var title = GetValue(viewModel);

            output.Content.AppendHtml(icon);
            output.Content.AppendHtml(title);
            output.Content.Append(viewModel.SubTitle);
        }

        private string GetLink()
        {
            var urlHelper = UrlHelper.GetUrlHelper(ViewContext);
            var linkHref = urlHelper.Action(new UrlActionContext
            {
                Action = Action,
                Controller = Controller,
                Values = new { categoryId = Item.Id }
            });

            return linkHref;
        }

        private TagBuilder GetIcon()
        {
            var icon = new TagBuilder("b");
            icon.Attributes.Add("class", "c-levels-item-check");
            var spacer = new TagBuilder("span");
            icon.InnerHtml.AppendHtml(spacer);
            return icon;
        }

        private TagBuilder GetValue(CategoryItemTagHelperViewModel viewModel)
        {
            var title = new TagBuilder("span");
            title.InnerHtml.Append(viewModel.Title);
            return title;
        }

        private CategoryItemTagHelperViewModel GetUnderlineContent()
        {
            const string noActionHref = "javascript:void(0)";
            var linkActiveClass = Item.Id == CategoryId
                ? "next"
                : string.Empty;

            var result = new CategoryItemTagHelperViewModel
            {
                Title = Item.Name,
                LinkActiveClass = linkActiveClass,
                Href = GetLink(),
                LinkClass = string.Empty
            };

            switch (Item.Level)
            {
                case SustainabilityLevel.Previous:
                    result.SubTitle = "(Achieved)";
                    result.Href = noActionHref;
                    break;

                case SustainabilityLevel.Current:
                    result.SubTitle = "(Current)";
                    result.LinkClass = "c-levels-item-red";
                    result.LinkActiveClass = "active";
                    break;

                case SustainabilityLevel.Next:
                    result.SubTitle = "(Next level)";
                    result.LinkClass = "c-levels-item-disable";
                    break;

                case SustainabilityLevel.Future:
                    result.SubTitle = "(Future level)";
                    result.LinkClass = "c-levels-item-disable";
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            return result;
        }
    }
}