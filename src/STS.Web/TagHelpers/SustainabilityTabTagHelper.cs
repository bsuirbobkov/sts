﻿using System;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace STS.Web.TagHelpers
{
    [HtmlTargetElement("menu-tab", Attributes = "controller, action, text, supplier-id")]
    public class SustainabilityTabTagHelper : TagHelper
    {
        [HtmlAttributeName("controller")]
        public string Controller { get; set; }

        [HtmlAttributeName("action")]
        public string Action { get; set; }

        [HtmlAttributeName("text")]
        public string LinkText { get; set; }

        [HtmlAttributeName("supplier-id")]
        public int? SupplierId { get; set; }

        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public IUrlHelperFactory UrlHelper { get; set; }

        public SustainabilityTabTagHelper(IUrlHelperFactory urlHelper)
        {
            UrlHelper = urlHelper;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var urlHelper = UrlHelper.GetUrlHelper(ViewContext);
            var linkHref = urlHelper.Action(new UrlActionContext
            {
                Action = Action,
                Controller = Controller,
                Values = new { id = SupplierId }
            });

            var link = new TagBuilder("a");
            link.MergeAttribute("href", linkHref);
            link.InnerHtml.Append(LinkText);

            output.TagName = "li";
            SetActiveTab(output);
            output.Content.AppendHtml(link);
        }

        private void SetActiveTab(TagHelperOutput output)
        {
            var routeData = ViewContext.RouteData.Values;
            var targetController = routeData["controller"].ToString();
            var targetAction = routeData["action"].ToString();

            Func<string, string, bool> compareIgnoreCase = (x, y) =>
                    x.Equals(y, StringComparison.InvariantCultureIgnoreCase);

            if (compareIgnoreCase(Controller, targetController) && compareIgnoreCase(Action, targetAction))
            {
                output.Attributes.Add("class", "active");
            }
        }
    }
}