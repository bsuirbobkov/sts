﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace STS.Web.TagHelpers
{
    /// <summary>
    /// <see cref="T:Microsoft.AspNetCore.Razor.TagHelpers.ITagHelper" /> implementation targeting &lt;label&gt; elements with an <c>asp-for</c> attribute.
    /// </summary>
    [HtmlTargetElement("label", Attributes = "asp-for")]
    public class LabelTagHelper : TagHelper
    {
        private const string ForAttributeName = "asp-for";
        /// <inheritdoc />
        public override int Order
        {
            get
            {
                return -1000;
            }
        }

        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        protected IHtmlGenerator Generator { get; }

        /// <summary>
        /// An expression to be evaluated against the current model.
        /// </summary>
        [HtmlAttributeName("asp-for")]
        public ModelExpression For { get; set; }

        /// <summary>
        /// Creates a new <see cref="T:Microsoft.AspNetCore.Mvc.TagHelpers.LabelTagHelper" />.
        /// </summary>
        /// <param name="generator">The <see cref="T:Microsoft.AspNetCore.Mvc.ViewFeatures.IHtmlGenerator" />.</param>
        public LabelTagHelper(IHtmlGenerator generator)
        {
            this.Generator = generator;
        }

        /// <inheritdoc />
        /// <remarks>Does nothing if <see cref="P:Microsoft.AspNetCore.Mvc.TagHelpers.LabelTagHelper.For" /> is <c>null</c>.</remarks>
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            if (output == null)
                throw new ArgumentNullException("output");

            TagBuilder tagBuilder = this.Generator.GenerateLabel(this.ViewContext, this.For.ModelExplorer, this.For.Name, (string)null, (object)null);
            var metadata = this.For.ModelExplorer.Metadata as DefaultModelMetadata;

            if (metadata != null)
            {
                if (metadata.Attributes.Attributes.Any(attr => attr is RequiredAttribute))
                {
                    tagBuilder.InnerHtml.Append(" *");
                }
            }
            
            if (tagBuilder == null)
                return;
            output.MergeAttributes(tagBuilder);
            if (output.IsContentModified)
                return;
            TagHelperContent childContentAsync = await output.GetChildContentAsync();
            if (childContentAsync.IsEmptyOrWhiteSpace)
                output.Content.SetHtmlContent((IHtmlContent)tagBuilder.InnerHtml);
            else
                output.Content.SetHtmlContent((IHtmlContent)childContentAsync);
        }
    }
}