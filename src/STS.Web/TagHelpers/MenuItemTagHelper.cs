﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using STS.Web.Models;

namespace STS.Web.TagHelpers
{
    [HtmlTargetElement("menu-item", Attributes = "controller-name, action-name")]
    public class MenuItemTagHelper : TagHelper
    {
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string MenuText { get; set; }
        public IEnumerable<ActionTagHelperModel> ChildActions { get; set; }

        [ViewContext]
        public ViewContext ViewContext { get; set; }

        public IUrlHelperFactory UrlHelper { get; set; }

        public MenuItemTagHelper(IUrlHelperFactory urlHelper)
        {
            UrlHelper = urlHelper;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var urlHelper = UrlHelper.GetUrlHelper(ViewContext);

            var menuUrl = urlHelper.Action(new UrlActionContext { Action = ActionName, Controller = ControllerName });

            output.TagName = "li";
            var linkClass = output.Attributes["class"].Value;

            output.Attributes.Remove(output.Attributes["class"]);

            var a = new TagBuilder("a");
            a.MergeAttribute("href", $"{menuUrl}");
            a.MergeAttribute("class", $"{linkClass}");

            var span = new TagBuilder("span");
            span.MergeAttribute("class", "nav-left-label" );
            span.InnerHtml.Append(MenuText);

            a.InnerHtml.AppendHtml(span);

            var routeData = ViewContext.RouteData.Values;
            var currentController = routeData["controller"];
            var currentAction = routeData["action"];

            if (string.Equals(ActionName, currentAction as string, StringComparison.OrdinalIgnoreCase)
                && string.Equals(ControllerName, currentController as string, StringComparison.OrdinalIgnoreCase))
            {
                a.Attributes["class"] += " active";
            }
            else if (ChildActions?.Any(action => 
                        string.Equals(action.Action, currentAction as string, StringComparison.OrdinalIgnoreCase) &&
                        string.Equals(action.Controller, currentController as string, StringComparison.OrdinalIgnoreCase))
                        ?? false)
           {
               a.Attributes["class"] += " active";
           }
            

            output.Content.AppendHtml(a);
        }
    }
}